#ifndef OPERATOR_HH
#define OPERATOR_HH
//- Dune includes
#include <dune/common/fmatrix.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/stencil.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>


// //Had a lot of trouble with shuffle
// //Added linear activation beside tanh
// #include <iostream>
// #include<vector>
// #include <list>
// #include <cstdlib>
// // #include <math.h>
//
// #define PI 3.141592653589793238463
// #define N
//
// // #define epsilon 0.05
// #define epoch 500
//
// using namespace std;
// extern "C" FILE *popen(const char *command, const char *mode);
//
// //double sigmoid(double x) { return 1.0f / (1.0f + exp(-x)); }
// //double dsigmoid(double x) { return x * (1.0f - x); }
// double tanh(double x) { return (exp(x)-exp(-x))/(exp(x)+exp(-x)) ;}
// double dtanh(double x) {return 1.0f - x*x ;}
//
// double lin(double x) { return x;}
// double dlin(double x) { return 1.0f;}
//
// double init_weight() { return (2.*rand()/RAND_MAX -1); }
// double MAXX = -9999999999999999; //maximum value of input example
// //double init_weight() { return ((double)rand(solvenn))/((double)RAND_MAX); }
//
// static const int numInputs = 1;
// static const int numHiddenNodes = 5;
// static const int numOutputs = 1;
//
// const double lr = 0.05f;
//
// double hiddenLayer[numHiddenNodes];
// double outputLayer[numOutputs];
//
// double hiddenLayerBias[numHiddenNodes];
// double outputLayerBias[numOutputs];
//
// double hiddenWeights[numInputs][numHiddenNodes];
// double outputWeights[numHiddenNodes][numOutputs];
//
// static const int numTrainingSets = 30;
// double training_inputs[numTrainingSets][numInputs];
// double training_outputs[numTrainingSets][numOutputs];
//
// void shuffle(int *array, size_t n)
// {
//     if (n > 1) //If no. of training examples > 1
//     {
//         size_t i;
//         for (i = 0; i < n - 1; i++)
//         {
//             size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
//             int t = array[j];
//             array[j] = array[i];
//             array[i] = t;
//         }
//     }
// }
//
// void predict(double test_sample[])
// {
//     for (int j=0; j<numHiddenNodes; j++)
//     {
//         double activation=hiddenLayerBias[j];
//         for (int k=0; k<numInputs; k++)
//         {
//             activation+=test_sample[k]*hiddenWeights[k][j];
//         }
//         hiddenLayer[j] = tanh(activation);
//     }
//
//     for (int j=0; j<numOutputs; j++)
//     {
//         double activation=outputLayerBias[j];
//         for (int k=0; k<numHiddenNodes; k++)
//         {
//             activation+=hiddenLayer[k]*outputWeights[k][j];
//         }
//         outputLayer[j] = lin(activation);
//     }
//     //std::cout<<outputLayer[0]<<"\n";
//     //return outputLayer[0];
//     //std::cout << "Input:" << training_inputs[i][0] << " " << training_inputs[i][1] << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[i][0] << "\n";
// }


// void prepdnn()
// {
//     ///TRAINING DATA GENERATION
//     // cout<< "Initial Data: ";
//     for (int i = 0; i < numTrainingSets; i++)
//     {
// 		double p = (1.0*(double)i/numTrainingSets);
// 		training_inputs[i][0] = (p);
// 		// training_outputs[i][0] = p*cos(p);
//     auto powS = pow(p,3.5);
//     auto pow1mS = pow(1.0-p,1.5);
//     auto kr = 1.0*powS/(powS+pow1mS*3);
//     training_outputs[i][0] = kr;
// 		/***************************Try Avoiding Edits In This part*******************************/
//         ///FINDING NORMALIZING FACTOR
//         for(int m=0; m<numInputs; ++m)
//             if(MAXX < training_inputs[i][m])
//                 MAXX = training_inputs[i][m];
//         for(int m=0; m<numOutputs; ++m)
//             if(MAXX < training_outputs[i][m])
//                 MAXX = training_outputs[i][m];
//
// 	}
// 	///NORMALIZING
// 	for (int i = 0; i < numTrainingSets; i++)
// 	{
//         for(int m=0; m<numInputs; ++m)
//             training_inputs[i][m] /= 1.0f*MAXX;
//
//         for(int m=0; m<numOutputs; ++m)
//             training_outputs[i][m] /= 1.0f*MAXX;
//         //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
//         //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
// 	}
// 	// cout<<endl;
//     ///WEIGHT & BIAS INITIALIZATION
//     for (int i=0; i<numInputs; i++) {
//         for (int j=0; j<numHiddenNodes; j++) {
//             hiddenWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numHiddenNodes; i++) {
//         hiddenLayerBias[i] = init_weight();
//         for (int j=0; j<numOutputs; j++) {
//             outputWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numOutputs; i++) {
//         //outputLayerBias[i] = init_weight();
//         outputLayerBias[i] = 0;
//     }
//
//     ///FOR INDEX SHUFFLING
//     int trainingSetOrder[numTrainingSets];
//     for(int j=0; j<numTrainingSets; ++j)
//         trainingSetOrder[j] = j;
//
//
//     ///TRAINING
//     //std::cout<<"start train\n";
//     vector<double> performance, epo; ///STORE MSE, EPOCH
//     for (int n=0; n < epoch; n++)
//     {
//         double MSE = 0;
//         shuffle(trainingSetOrder,numTrainingSets);
//         // std::cout<<"\nepoch :"<<n;
//         for (int x=0; x<numTrainingSets; x++)
//         {
//             int i = trainingSetOrder[x];
//             //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
//             //int x=i;
//             //std::cout<<"Training Set :"<<x<<"\n";
//             /// Forward pass
//             for (int j=0; j<numHiddenNodes; j++)
//             {
//                 double activation=hiddenLayerBias[j];
//                 //std::cout<<"Training Set :"<<x<<"\n";
//                  for (int k=0; k<numInputs; k++) {
//                     activation+=training_inputs[i][k]*hiddenWeights[k][j];
//                 }
//                 hiddenLayer[j] = tanh(activation);
//             }
//
//             for (int j=0; j<numOutputs; j++) {
//                 double activation=outputLayerBias[j];
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     activation+=hiddenLayer[k]*outputWeights[k][j];
//                 }
//                 outputLayer[j] = lin(activation);
//             }
//
//             //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
//             for(int k=0; k<numOutputs; ++k)
//                 MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);
//
//            /// Backprop
//            ///   For V
//             double deltaOutput[numOutputs];
//             for (int j=0; j<numOutputs; j++) {
//                 double errorOutput = (training_outputs[i][j]-outputLayer[j]);
//                 deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
//             }
//
//             ///   For W
//             double deltaHidden[numHiddenNodes];
//             for (int j=0; j<numHiddenNodes; j++) {
//                 double errorHidden = 0.0f;
//                 for(int k=0; k<numOutputs; k++) {
//                     errorHidden+=deltaOutput[k]*outputWeights[j][k];
//                 }
//                 deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
//             }
//
//             ///Updation
//             ///   For V and b
//             for (int j=0; j<numOutputs; j++) {
//                 //b
//                 outputLayerBias[j] += deltaOutput[j]*lr;
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
//                 }
//             }
//
//             ///   For W and c
//             for (int j=0; j<numHiddenNodes; j++) {
//                 //c
//                 hiddenLayerBias[j] += deltaHidden[j]*lr;
//                 //W
//                 for(int k=0; k<numInputs; k++) {
//                   hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
//                 }
//             }
//         }
//         //Averaging the MSE
//         MSE /= 1.0f*numTrainingSets;
//         //cout<< "  MSE: "<< MSE<<endl;
//         ///Steps to PLOT PERFORMANCE PER EPOCH
//         performance.push_back(MSE*100);
//         epo.push_back(n);
//     }
//
//     // // Print weights
//     // std::cout << "Final Hidden Weights\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << "[ ";
//     //     for(int k=0; k<numInputs; k++) {
//     //         std::cout << hiddenWeights[k][j] << " ";
//     //     }
//     //     std::cout << "] ";
//     // }
//     // std::cout << "]\n";
//     //
//     // std::cout << "Final Hidden Biases\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << hiddenLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//     // std::cout << "Final Output Weights";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << "[ ";
//     //     for (int k=0; k<numHiddenNodes; k++) {
//     //         std::cout << outputWeights[k][j] << " ";
//     //     }
//     //     std::cout << "]\n";
//     // }
//     // std::cout << "Final Output Biases\n[ ";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << outputLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//   //
//   //   //Plot the results
// 	// vector<float> x;
// 	// vector<float> y1, y2;
//   //   //double test_input[1000][numInputs];
//   //   int numTestSets = numTrainingSets;
// 	// for (float i = 0; i < numTestSets; i=i+0.25)
// 	// {
//   //       double p = (1.0*(double)i/numTestSets);
// 	// 	x.push_back(p);
// 	// 	// y1.push_back(p*cos(p));
//   //   auto powS = pow(p,3.5);
//   //   auto pow1mS = pow(1.0-p,1.5);
//   //   auto kr = 1.0*powS/(powS+pow1mS*3);
//   //   y1.push_back(kr);
//   //
// 	// 	double test_input[1];
// 	// 	test_input[0] = p/MAXX;
//   //       predict(test_input);
// 	// 	y2.push_back(outputLayer[0]*MAXX);
// 	// }
//   //
// 	// FILE * gp = popen("gnuplot", "w");
// 	// fprintf(gp, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp, "set grid \n");
// 	// fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
// 	// fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
//   //
// 	// ///Exact f(x) = sin(x) -> Green Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y1[k]);
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y2[k]);
//   //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
//   //
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// fflush(gp);
//   //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
//   //   FILE * gp1 = popen("gnuplot", "w");
// 	// fprintf(gp1, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp1, "set grid \n");
// 	// fprintf(gp1, "set title '%s' \n", "Performance");
// 	// fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp1, "plot '-' w p ls 1 \n");
//   //
//   //   for (int k = 0; k < epo.size(); k++) {
// 	// 	fprintf(gp1, "%f %f \n", epo[k], performance[k]);
// 	// }
// 	// fprintf(gp1, "e\n");
//   //
// 	// fflush(gp1);
//   //
// 	// system("pause");
// 	// //_pclose(gp);
//   // cin.get();
//
//     // return 0;
// }
//
//
// void launchdnn()
// {
//     // /TRAINING DATA GENERATION
//     // cout<< "Initial Data: ";
//     for (int i = 0; i < numTrainingSets; i++)
//     {
// 		double p = (1.0*(double)i/numTrainingSets);
// 		training_inputs[i][0] = (p);
// 		// training_outputs[i][0] = p*cos(p);
//     auto powS = pow(p,3.5);
//     auto pow1mS = pow(1.0-p,1.5);
//     auto kr = 1.0*powS/(powS+pow1mS*3);
//     training_outputs[i][0] = kr;
// 		/***************************Try Avoiding Edits In This part*******************************/
//         ///FINDING NORMALIZING FACTOR
//         for(int m=0; m<numInputs; ++m)
//             if(MAXX < training_inputs[i][m])
//                 MAXX = training_inputs[i][m];
//         for(int m=0; m<numOutputs; ++m)
//             if(MAXX < training_outputs[i][m])
//                 MAXX = training_outputs[i][m];
//
// 	}
// 	///NORMALIZING
// 	for (int i = 0; i < numTrainingSets; i++)
// 	{
//         for(int m=0; m<numInputs; ++m)
//             training_inputs[i][m] /= 1.0f*MAXX;
//
//         for(int m=0; m<numOutputs; ++m)
//             training_outputs[i][m] /= 1.0f*MAXX;
//         //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
//         //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
// 	}
// 	// cout<<endl;
//     ///WEIGHT & BIAS INITIALIZATION
//     for (int i=0; i<numInputs; i++) {
//         for (int j=0; j<numHiddenNodes; j++) {
//             hiddenWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numHiddenNodes; i++) {
//         hiddenLayerBias[i] = init_weight();
//         for (int j=0; j<numOutputs; j++) {
//             outputWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numOutputs; i++) {
//         //outputLayerBias[i] = init_weight();
//         outputLayerBias[i] = 0;
//     }
//
//     ///FOR INDEX SHUFFLING
//     int trainingSetOrder[numTrainingSets];
//     for(int j=0; j<numTrainingSets; ++j)
//         trainingSetOrder[j] = j;
//
//
//     ///TRAINING
//     //std::cout<<"start train\n";
//     vector<double> performance, epo; ///STORE MSE, EPOCH
//     for (int n=0; n < epoch; n++)
//     {
//         double MSE = 0;
//         shuffle(trainingSetOrder,numTrainingSets);
//         // std::cout<<"\nepoch :"<<n;
//         for (int x=0; x<numTrainingSets; x++)
//         {
//             int i = trainingSetOrder[x];
//             //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
//             //int x=i;
//             //std::cout<<"Training Set :"<<x<<"\n";
//             /// Forward pass
//             for (int j=0; j<numHiddenNodes; j++)
//             {
//                 double activation=hiddenLayerBias[j];
//                 //std::cout<<"Training Set :"<<x<<"\n";
//                  for (int k=0; k<numInputs; k++) {
//                     activation+=training_inputs[i][k]*hiddenWeights[k][j];
//                 }
//                 hiddenLayer[j] = tanh(activation);
//             }
//
//             for (int j=0; j<numOutputs; j++) {
//                 double activation=outputLayerBias[j];
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     activation+=hiddenLayer[k]*outputWeights[k][j];
//                 }
//                 outputLayer[j] = lin(activation);
//             }
//
//             //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
//             for(int k=0; k<numOutputs; ++k)
//                 MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);
//
//            /// Backprop
//            ///   For V
//             double deltaOutput[numOutputs];
//             for (int j=0; j<numOutputs; j++) {
//                 double errorOutput = (training_outputs[i][j]-outputLayer[j]);
//                 deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
//             }
//
//             ///   For W
//             double deltaHidden[numHiddenNodes];
//             for (int j=0; j<numHiddenNodes; j++) {
//                 double errorHidden = 0.0f;
//                 for(int k=0; k<numOutputs; k++) {
//                     errorHidden+=deltaOutput[k]*outputWeights[j][k];
//                 }
//                 deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
//             }
//
//             ///Updation
//             ///   For V and b
//             for (int j=0; j<numOutputs; j++) {
//                 //b
//                 outputLayerBias[j] += deltaOutput[j]*lr;
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
//                 }
//             }
//
//             ///   For W and c
//             for (int j=0; j<numHiddenNodes; j++) {
//                 //c
//                 hiddenLayerBias[j] += deltaHidden[j]*lr;
//                 //W
//                 for(int k=0; k<numInputs; k++) {
//                   hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
//                 }
//             }
//         }
//         //Averaging the MSE
//         MSE /= 1.0f*numTrainingSets;
//         //cout<< "  MSE: "<< MSE<<endl;
//         ///Steps to PLOT PERFORMANCE PER EPOCH
//         performance.push_back(MSE*100);
//         epo.push_back(n);
//     }
//
//     // // Print weights
//     // std::cout << "Final Hidden Weights\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << "[ ";
//     //     for(int k=0; k<numInputs; k++) {
//     //         std::cout << hiddenWeights[k][j] << " ";
//     //     }
//     //     std::cout << "] ";
//     // }
//     // std::cout << "]\n";
//     //
//     // std::cout << "Final Hidden Biases\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << hiddenLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//     // std::cout << "Final Output Weights";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << "[ ";
//     //     for (int k=0; k<numHiddenNodes; k++) {
//     //         std::cout << outputWeights[k][j] << " ";
//     //     }
//     //     std::cout << "]\n";
//     // }
//     // std::cout << "Final Output Biases\n[ ";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << outputLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//
//     //Plot the results
// 	vector<float> x;
// 	vector<float> y1, y2;
//     //double test_input[1000][numInputs];
//     int numTestSets = numTrainingSets;
// 	for (float i = 0; i < numTestSets; i=i+0.25)
// 	{
//         double p = (1.0*(double)i/numTestSets);
// 		x.push_back(p);
// 		// y1.push_back(p*cos(p));
//     auto powS = pow(p,3.5);
//     auto pow1mS = pow(1.0-p,1.5);
//     auto kr = 1.0*powS/(powS+pow1mS*3);
//     y1.push_back(kr);
//
// 		double test_input[1];
// 		test_input[0] = p/MAXX;
//         predict(test_input);
// 		y2.push_back(outputLayer[0]*MAXX);
// 	}
//   //
// 	// FILE * gp = popen("gnuplot", "w");
// 	// fprintf(gp, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp, "set grid \n");
// 	// fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
// 	// fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
//   //
// 	// ///Exact f(x) = sin(x) -> Green Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y1[k]);
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y2[k]);
//   //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
//   //
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// fflush(gp);
//   //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
//   //   FILE * gp1 = popen("gnuplot", "w");
// 	// fprintf(gp1, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp1, "set grid \n");
// 	// fprintf(gp1, "set title '%s' \n", "Performance");
// 	// fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp1, "plot '-' w p ls 1 \n");
//   //
//   //   for (int k = 0; k < epo.size(); k++) {
// 	// 	fprintf(gp1, "%f %f \n", epo[k], performance[k]);
// 	// }
// 	// fprintf(gp1, "e\n");
//   //
// 	// fflush(gp1);
//   //
// 	// system("pause");
// 	// //_pclose(gp);
//   // cin.get();
//
//     // return 0;
// }


// FlowOperator
// ----------------
template<class DiscreteFunction, class Model >
struct FlowOperator
  : public virtual Dune::Fem::Operator< DiscreteFunction >
{
  typedef DiscreteFunction DiscreteFunctionType;
  typedef Model            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType::LocalFunctionType         LocalFunctionType;
  typedef typename LocalFunctionType::RangeType                    RangeType;
  typedef typename LocalFunctionType::JacobianRangeType            JacobianRangeType;

  typedef typename DiscreteFunctionSpaceType::IteratorType         IteratorType;
  typedef typename IteratorType::Entity                            EntityType;
  typedef typename EntityType::Geometry                            GeometryType;

  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;

  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;

  // typedef typename EntityType::EntityPointer  EntityPointerType;

  typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection IntersectionType;
  typedef typename IntersectionType::Geometry             IntersectionGeometryType;

  typedef Dune::Fem::ElementQuadrature< GridPartType, 1 > FaceQuadratureType;

  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > QuadratureType;

  static const int dimDomain = LocalFunctionType::dimDomain;
  static const int dimRange = LocalFunctionType::dimRange;

public:
  //! constructor
  FlowOperator( const ModelType &model,
          const DiscreteFunctionType &solution )
    : model_( model )
    {
      // launchdnn(epo0, perf0);

    }
   //    void launchdnn( vector<double> epch, vector<double>perf ) const
   //    {
   //        ///TRAINING DATA GENERATION
   //        // cout<< "Initial Data: ";
   //        for (int i = 0; i < numTrainingSets; i++)
   //        {
   //        double p = (1.0*(double)i/numTrainingSets);
   //        training_inputs[i][0] = (p);
   //
   //        double tetha_in( 0 );
   //        double effsat;
   //        double Krw;
   //        //
   //        // inhomegeneity_tetha( entity, x, tetha_in );
   //        // effective_sat( entity, x, p, effsat );
   //        //
   //        if (1>p)
   //          Krw = (std::pow((1-p),(2.+3*2.0)/2.0));
   //        else
   //          Krw =0;
   //
   //         auto testwmob = Krw/1.0e-3;
   //
   //
   //        // // training_outputs[i][0] = p*cos(p);
   //        // auto powS = pow(p,3.5);
   //        // auto pow1mS = pow(1.0-p,1.5);
   //        // auto kr = 1.0*powS/(powS+pow1mS*3);
   //        training_outputs[i][0] = Krw;
   //        /***************************Try Avoiding Edits In This part*******************************/
   //            ///FINDING NORMALIZING FACTOR
   //            for(int m=0; m<numInputs; ++m)
   //                if(MAXX < training_inputs[i][m])
   //                    MAXX = training_inputs[i][m];
   //            for(int m=0; m<numOutputs; ++m)
   //                if(MAXX < training_outputs[i][m])
   //                    MAXX = training_outputs[i][m];
   //
   //      }
   //        ///NORMALIZING
   //        for (int i = 0; i < numTrainingSets; i++)
   //        {
   //              for(int m=0; m<numInputs; ++m)
   //                  training_inputs[i][m] /= 1.0f*MAXX;
   //
   //              for(int m=0; m<numOutputs; ++m)
   //                  training_outputs[i][m] /= 1.0f*MAXX;
   //              //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
   //              //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
   //        }
   //        // cout<<endl;
   //        ///WEIGHT & BIAS INITIALIZATION
   //        for (int i=0; i<numInputs; i++) {
   //            for (int j=0; j<numHiddenNodes; j++) {
   //                hiddenWeights[i][j] = init_weight();
   //            }
   //        }
   //        for (int i=0; i<numHiddenNodes; i++) {
   //            hiddenLayerBias[i] = init_weight();
   //            for (int j=0; j<numOutputs; j++) {
   //                outputWeights[i][j] = init_weight();
   //            }
   //        }
   //        for (int i=0; i<numOutputs; i++) {
   //            //outputLayerBias[i] = init_weight();
   //            outputLayerBias[i] = 0;
   //        }
   //
   //        ///FOR INDEX SHUFFLING
   //        int trainingSetOrder[numTrainingSets];
   //        for(int j=0; j<numTrainingSets; ++j)
   //            trainingSetOrder[j] = j;
   //
   //
   //        ///TRAINING
   //        //std::cout<<"start train\n";
   //        vector<double> performance, epo; ///STORE MSE, EPOCH
   //        // vector<double> performance; ///STORE MSE, EPOCH
   //
   //        for (int n=0; n < epoch; n++)
   //        {
   //            double MSE = 0;
   //            shuffle(trainingSetOrder,numTrainingSets);
   //            // std::cout<<"\nepoch :"<<n;
   //            for (int x=0; x<numTrainingSets; x++)
   //            {
   //                int i = trainingSetOrder[x];
   //                //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
   //                //int x=i;
   //                //std::cout<<"Training Set :"<<x<<"\n";
   //                /// Forward pass
   //                for (int j=0; j<numHiddenNodes; j++)
   //                {
   //                    double activation=hiddenLayerBias[j];
   //                    //std::cout<<"Training Set :"<<x<<"\n";
   //                     for (int k=0; k<numInputs; k++) {
   //                        activation+=training_inputs[i][k]*hiddenWeights[k][j];
   //                    }
   //                    hiddenLayer[j] = tanh(activation);
   //                }
   //
   //                for (int j=0; j<numOutputs; j++) {
   //                    double activation=outputLayerBias[j];
   //                    for (int k=0; k<numHiddenNodes; k++)
   //                    {
   //                        activation+=hiddenLayer[k]*outputWeights[k][j];
   //                    }
   //                    outputLayer[j] = lin(activation);
   //                }
   //
   //                //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
   //                for(int k=0; k<numOutputs; ++k)
   //                    MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);
   //
   //               /// Backprop
   //               ///   For V
   //                double deltaOutput[numOutputs];
   //                for (int j=0; j<numOutputs; j++) {
   //                    double errorOutput = (training_outputs[i][j]-outputLayer[j]);
   //                    deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
   //                }
   //
   //                ///   For W
   //                double deltaHidden[numHiddenNodes];
   //                for (int j=0; j<numHiddenNodes; j++) {
   //                    double errorHidden = 0.0f;
   //                    for(int k=0; k<numOutputs; k++) {
   //                        errorHidden+=deltaOutput[k]*outputWeights[j][k];
   //                    }
   //                    deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
   //                }
   //
   //                ///Updation
   //                ///   For V and b
   //                for (int j=0; j<numOutputs; j++) {
   //                    //b
   //                    outputLayerBias[j] += deltaOutput[j]*lr;
   //                    for (int k=0; k<numHiddenNodes; k++)
   //                    {
   //                        outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
   //                    }
   //                }
   //
   //                ///   For W and c
   //                for (int j=0; j<numHiddenNodes; j++) {
   //                    //c
   //                    hiddenLayerBias[j] += deltaHidden[j]*lr;
   //                    //W
   //                    for(int k=0; k<numInputs; k++) {
   //                      hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
   //                    }
   //                }
   //            }
   //            //Averaging the MSE
   //            MSE /= 1.0f*numTrainingSets;
   //            //cout<< "  MSE: "<< MSE<<endl;
   //            ///Steps to PLOT PERFORMANCE PER EPOCH
   //            performance.push_back(MSE*100);
   //            epo.push_back(n);
   //        }
   //
   //        epch =epo;
   //        perf = performance;
   //        // return epo;
   //        //
   //        // //  // Print weights
   //        //  // std::cout << "Final Hidden Weights\n[ ";
   //        //  for (int j=0; j<numHiddenNodes; j++) {
   //        //      // std::cout << "[ ";
   //        //      for(int k=0; k<numInputs; k++) {
   //        //          // std::cout << hiddenWeights[k][j] << " ";
   //        //      }
   //        //      // std::cout << "] ";
   //        //  }
   //        //  // std::cout << "]\n";
   //        //
   //        //  // std::cout << "Final Hidden Biases\n[ ";
   //        //  for (int j=0; j<numHiddenNodes; j++) {
   //        //      // std::cout << hiddenLayerBias[j] << " ";
   //        //  }
   //         // std::cout << "]\n";
   //         // std::cout << "Final Output Weights";
   //         // for (int j=0; j<numOutputs; j++) {
   //         //     // std::cout << "[ ";
   //         //     for (int k=0; k<numHiddenNodes; k++) {
   //         //         std::cout << outputWeights[k][j] << " ";
   //         //     }
   //         //     std::cout << "]\n";
   //         // }
   //         // std::cout << "Final Output Biases\n[ ";
   //         // for (int j=0; j<numOutputs; j++) {
   //         //     std::cout << outputLayerBias[j] << " ";
   //         // }
   //         // std::cout << "]\n";
   //
   //          //  //Plot the results
   //          // vector<float> x;
   //          // vector<float> y1, y2;
   //          //  double test_input[1000][numInputs];
   //          //  int numTestSets = numTrainingSets;
   //          // for (float i = 0; i < numTestSets; i=i+0.25)
   //          // {
   //          //      double p = (1.0*(double)i/numTestSets);
   //          //  x.push_back(p);
   //          //  // // y1.push_back(p*cos(p));
   //          //  // auto powS = pow(p,3.5);
   //          //  // auto pow1mS = pow(1.0-p,1.5);
   //          //  // auto kr = 1.0*powS/(powS+pow1mS*3);
   //          //  double Kr;
   //          //
   //          //  if (1>p)
   //          //    Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
   //          //  else
   //          //    Kr =0;
   //          //
   //          //  // auto testwmob = Krw/1.0e-3;
   //          //  y1.push_back(Kr);
   //          //
   //          //  double test_input[1];
   //          //  test_input[0] = p/MAXX;
   //          //      predict(test_input);
   //          //  y2.push_back(outputLayer[0]*MAXX);
   //          //
   //          // }
   //          // std::cout <<"yyyyy Output "<< y2.size()<< " ";
   //          // //
   //          // FILE * gp = popen("gnuplot", "w");
   //          // fprintf(gp, "set terminal wxt size 600,400 \n");
   //          // fprintf(gp, "set grid \n");
   //          // fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
   //          // fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
   //          // fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
   //          // fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
   //          //
   //          // ///Exact f(x) = sin(x) -> Green Graph
   //          // for (int k = 0; k < x.size(); k++) {
   //          //   fprintf(gp, "%f %f \n", x[k], y1[k]);
   //          // }
   //          // fprintf(gp, "e\n");
   //          // //
   //          // // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
   //          // for (int k = 0; k < x.size(); k++) {
   //          //   fprintf(gp, "%f %f \n", x[k], y2[k]);
   //          //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
   //          //
   //          // }
   //          // fprintf(gp, "e\n");
   //          //
   //          // fflush(gp);
   //          //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
   //          //   FILE * gp1 = popen("gnuplot", "w");
   //          // fprintf(gp1, "set terminal wxt size 600,400 \n");
   //          // fprintf(gp1, "set grid \n");
   //          // fprintf(gp1, "set title '%s' \n", "Performance");
   //          // fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
   //          // fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
   //          // fprintf(gp1, "plot '-' w p ls 1 \n");
   //          //
   //          //   for (int k = 0; k < epo.size(); k++) {
   //          //   fprintf(gp1, "%f %f \n", epo[k], performance[k]);
   //          // }
   //          // fprintf(gp1, "e\n");
   //          //
   //          // fflush(gp1);
   //          // //
   //          // system("pause");
   //          // //_pclose(gp);
   //          // cin.get();
   //          //  resultdnn = y2;
   //          //  inputset = x;
   //          //    // return 0;
   //          //  // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
   //          //  // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
   //         }
   //
   //
   //
   // void dnnprediction(vector<double> epo, vector<double>performance,std::vector<float> inputset, std::vector<float> resultdnn )const
   // {
   //
   //               vector<float> x;
   //               vector<float> y1, y2;
   //                double test_input[1000][numInputs];
   //                int numTestSets = numTrainingSets;
   //               for (float i = 0; i < numTestSets; i=i+0.25)
   //               {
   //                    double p = (1.0*(double)i/numTestSets);
   //                x.push_back(p);
   //                // // y1.push_back(p*cos(p));
   //                // auto powS = pow(p,3.5);
   //                // auto pow1mS = pow(1.0-p,1.5);
   //                // auto kr = 1.0*powS/(powS+pow1mS*3);
   //                double Kr;
   //
   //                if (1>p)
   //                  Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
   //                else
   //                  Kr =0;
   //
   //                // auto testwmob = Krw/1.0e-3;
   //                y1.push_back(Kr);
   //
   //                double test_input[1];
   //                test_input[0] = p/MAXX;
   //                    predict(test_input);
   //                y2.push_back(outputLayer[0]*MAXX);
   //
   //               }
   //               // std::cout <<"yyyyy Output "<< y2.size()<< " ";
   //               // //
   //               // FILE * gp = popen("gnuplot", "w");
   //               // fprintf(gp, "set terminal wxt size 600,400 \n");
   //               // fprintf(gp, "set grid \n");
   //               // fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
   //               // fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
   //               // fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
   //               // fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
   //               //
   //               // ///Exact f(x) = sin(x) -> Green Graph
   //               // for (int k = 0; k < x.size(); k++) {
   //               //   fprintf(gp, "%f %f \n", x[k], y1[k]);
   //               // }
   //               // fprintf(gp, "e\n");
   //               // //
   //               // // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
   //               // for (int k = 0; k < x.size(); k++) {
   //               //   fprintf(gp, "%f %f \n", x[k], y2[k]);
   //               //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
   //               //
   //               // }
   //               // fprintf(gp, "e\n");
   //               //
   //               // fflush(gp);
   //               //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
   //               //   FILE * gp1 = popen("gnuplot", "w");
   //               // fprintf(gp1, "set terminal wxt size 600,400 \n");
   //               // fprintf(gp1, "set grid \n");
   //               // fprintf(gp1, "set title '%s' \n", "Performance");
   //               // fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
   //               // fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
   //               // fprintf(gp1, "plot '-' w p ls 1 \n");
   //               //
   //               //   for (int k = 0; k < epo.size(); k++) {
   //               //   fprintf(gp1, "%f %f \n", epo[k], performance[k]);
   //               // }
   //               // fprintf(gp1, "e\n");
   //               //
   //               // fflush(gp1);
   //               // //
   //               // system("pause");
   //               // //_pclose(gp);
   //               // cin.get();
   //                resultdnn = y2;
   //                inputset = x;
   //                // std::cout << "resultdnn10";
   //                // //
   //                  // std::cout << y2[10] << " ";
   //                  // std::cout << "]\n";
   //                  // return 0;
   //                // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
   //                // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
   //              }


  // prepare the solution vector
  template <class Function>
  void prepare( const Function &func,
          DiscreteFunctionType &u )
    {
      // launchdnn(epo0, perf0);

       // vector<double> perf0(10), epo0(10);
       // launchdnn(epo0, perf0);

       // for (auto i: perf0)
       //     std::cout << i << ' ';
       // std::vector<char> path;
// ...
    // for (double i: perf0)
    //   std::cout << i << ' ';
    //   //
      // launchdnn(epo0, perf0);
      // epo=epo0;
      // perf=perf0;
      // launchdnn(epo0, perf0);
      // std::cout << perf0 << " ";
      // std::cout << "]\n";
// launchdnn(epo, perf);
    }
  //! application operator
  virtual void operator() ( const DiscreteFunctionType &u,
          DiscreteFunctionType &w ) const;

protected:
  const ModelType &model () const { return model_; }

  double penalty() const { return model_.penalty(); }

  void volumetricPart( const EntityType& entity,
           const int& quadOrder,
           const GeometryType &geometry,
           const LocalFunctionType& uLocal,
           LocalFunctionType& wLocalEn ) const;

  void internal_Bnd_terms( const EntityType& entity,
         const int& quadOrder,
         const GeometryType &geometry,
         const IntersectionType &intersection,
         const DiscreteFunctionSpaceType &dfSpace,
         const LocalFunctionType& uLocal,
         const DiscreteFunctionType& u,
         LocalFunctionType& wLocalEn) const;

public:
  //eps NIPG SIPG  (epsil=0 BOmethod, epsil=1 SIPGmethod, epsil=-1 NIPGmethod)
  const int epsil = Dune::Fem::Parameter::getValue< double >( "phaseflow.DGeps", 1 );
  const bool with_upw = Dune::Fem::Parameter::getValue< bool >( "phaseflow.with_upw", false );
  // vector<double> perf0, epo0;

private:
  ModelType model_;
};

// DifferentiableFlowOperator
// ------------------------------

template< class JacobianOperator, class Model >
struct DifferentiableFlowOperator
  : public FlowOperator< typename JacobianOperator::DomainFunctionType, Model >,
    public Dune::Fem::DifferentiableOperator< JacobianOperator >
{
  typedef FlowOperator< typename JacobianOperator::DomainFunctionType, Model > BaseType;

  typedef JacobianOperator JacobianOperatorType;

  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef typename BaseType::ModelType            ModelType;

protected:
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType::LocalFunctionType         LocalFunctionType;
  typedef typename LocalFunctionType::RangeType                    RangeType;
  typedef typename LocalFunctionType::JacobianRangeType            JacobianRangeType;

  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  typedef typename IteratorType::Entity       EntityType;
  typedef typename EntityType::Geometry       GeometryType;

  typedef typename DiscreteFunctionSpaceType :: DomainType DomainType;

  typedef typename DiscreteFunctionSpaceType::GridPartType  GridPartType;

  typedef typename BaseType::QuadratureType QuadratureType;

  typedef typename LocalFunctionType::RangeFieldType RangeFieldType;

  // typedef typename EntityType::EntityPointer  EntityPointerType;

  typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
  typedef typename IntersectionIteratorType::Intersection IntersectionType;
  typedef typename IntersectionType::Geometry             IntersectionGeometryType;

  typedef Dune::Fem::ElementQuadrature< GridPartType, 1 > FaceQuadratureType;

  static const int dimDomain = LocalFunctionType::dimDomain;
  static const int dimRange = LocalFunctionType::dimRange;

  typedef typename JacobianOperator::LocalMatrixType LocalMatrixType;
  typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

  typedef typename DiscreteFunctionSpaceType :: DomainFieldType DomainFieldType;
  typedef Dune::FieldVector< DomainType , dimRange > DeoModType;
  typedef Dune::FieldMatrix< DomainFieldType, dimDomain , dimDomain > MatrixType;
  typedef Dune::DGFEntityKey<int> KeyType;
  // global coordinates
  typedef typename GeometryType::GlobalCoordinate GlobalCoordinateType;
  // local coordinates
  typedef typename GeometryType::LocalCoordinate LocalCoordinateType;
  //

public:

  DifferentiableFlowOperator( const ModelType &model, const DiscreteFunctionType &solution,DiscreteFunctionSpaceType &space)
    : BaseType( model, solution )/*, stencil_(space,space)*/
    {}

  //! method to setup the jacobian of the operator for storage in a matrix
  void jacobian( const DiscreteFunctionType &u, JacobianOperatorType &jOp ) const;

protected:

  void element_Local_Contribution( const EntityType& entity,
                                   const GeometryType &geometry,
                                   const DiscreteFunctionSpaceType &dfSpace,
                                   const BasisFunctionSetType &baseSet,
                                   const LocalFunctionType& uLocal,
                                   LocalMatrixType& jLocal ) const;

  void interface_Local_Contribution( const EntityType& entity,
                                     const GeometryType &geometry,
                                     const DiscreteFunctionSpaceType &dfSpace,
                                     const BasisFunctionSetType &baseSet,
                                     const IntersectionType& intersection,
                                     const LocalFunctionType& uLocal,
                                     const DiscreteFunctionType& u,
                                     const JacobianOperatorType &jOp,
                                     LocalMatrixType& jLocal ) const;

protected:
  using BaseType::model;
  using BaseType::penalty;
  using BaseType::epsil;
  using BaseType::with_upw;

};

template< class DiscreteFunction, class Model >
void FlowOperator< DiscreteFunction, Model >
:: volumetricPart( const EntityType& entity,
                   const int& quadOrder,
                   const GeometryType &geometry,
                   const LocalFunctionType& uLocal,
                   LocalFunctionType& wLocalEn ) const
{
  QuadratureType quadrature( entity, quadOrder );
  const size_t numQuadraturePoints = quadrature.nop();
  for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
  {
    const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
    const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );


    // launchdnn();
    // launchdnn(epo, perf);
      // std::vector<float> resultdnntest;

   // launchdnn(epo0, perf0);
   // epo=epo0;
   // perf=perf0;

    // dnnprediction(epo0,perf0, inputset, resultdnn);

                     //
                     // vector<float> xx;
                     // vector<float> y1, y2;
                     //  double test_input[1000][numInputs];
                     //  int numTestSets = numTrainingSets;
                     // for (float i = 0; i < numTestSets; i=i+0.25)
                     // {
                     //      double p = (1.0*(double)i/numTestSets);
                     //  xx.push_back(p);
                     //  // // y1.push_back(p*cos(p));
                     //  // auto powS = pow(p,3.5);
                     //  // auto pow1mS = pow(1.0-p,1.5);
                     //  // auto kr = 1.0*powS/(powS+pow1mS*3);
                     //  double Kr;
                     //
                     //  if (1>p)
                     //    Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
                     //  else
                     //    Kr =0;
                     //
                     //  // auto testwmob = Krw/1.0e-3;
                     //  y1.push_back(Kr);
                     //
                     //  double test_input[1];
                     //  test_input[0] = p/MAXX;
                     //      predict(test_input);
                     //  y2.push_back(outputLayer[0]*MAXX);
                     //
                     // }
                     // // std::cout <<"yyyyy Output "<< y2.size()<< " ";
                     // // //
                     // // FILE * gp = popen("gnuplot", "w");
                     // // fprintf(gp, "set terminal wxt size 600,400 \n");
                     // // fprintf(gp, "set grid \n");
                     // // fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
                     // // fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
                     // // fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
                     // // fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
                     // //
                     // // ///Exact f(x) = sin(x) -> Green Graph
                     // // for (int k = 0; k < xx.size(); k++) {
                     // //   fprintf(gp, "%f %f \n", xx[k], y1[k]);
                     // // }
                     // // fprintf(gp, "e\n");
                     // // //
                     // // // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
                     // // for (int k = 0; k < xx.size(); k++) {
                     // //   fprintf(gp, "%f %f \n", xx[k], y2[k]);
                     // //   std::cout <<"xx coord"<< xx[k]<<" Output "<< y2[k] << " ";
                     // //
                     // // }
                     // // fprintf(gp, "e\n");
                     // //
                     // // fflush(gp);
                     // //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
                     // //   FILE * gp1 = popen("gnuplot", "w");
                     // // fprintf(gp1, "set terminal wxt size 600,400 \n");
                     // // fprintf(gp1, "set grid \n");
                     // // fprintf(gp1, "set title '%s' \n", "Performance");
                     // // fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
                     // // fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
                     // // fprintf(gp1, "plot '-' w p ls 1 \n");
                     // //
                     // //   for (int k = 0; k < epo.size(); k++) {
                     // //   fprintf(gp1, "%f %f \n", epo[k], perf[k]);
                     // // }
                     // // fprintf(gp1, "e\n");
                     // //
                     // // fflush(gp1);
                     // // //
                     // // system("pause");
                     // // //_pclose(gp);
                     // // cin.get();
                     // //  // resultdnn = y2;
                     // //  // inputset = xx;
                     //  std::cout << "resultdnn10";
                     //  //
                     //    // std::cout<<xx[119] << y2[119*vu] << " ";
                     //    std::cout<<xx.size()<< y2.size() << " ";
                     //
                     //    std::cout << "]\n";
                     // //    // return 0;
                     // //  // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
                     // //  // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";


    // std::cout << epo0<< " ";
    // std::cout << "]\n";
    // std::cout << resultdnn[0]<< " ";

    RangeType vu( RangeType( 0 ) );
    uLocal.evaluate( quadrature[ pt ], vu );
    JacobianRangeType du,dun;
    uLocal.jacobian( quadrature[ pt ], du );

    // std::cout<<xx[119] << y2[119*vu[1]]<< " ";

    RangeType avu( RangeType( 0 ) );
    model().source( entity, quadrature[ pt ], vu, avu );
    avu *= 1.0*weight;

    JacobianRangeType adu( 0 ), gpdu( 0 ), wpdu( 0 ), cpdu( 0 );

    model().gravity_Flux( entity, quadrature[ pt ], vu, du, gpdu );
    model().wetting_Pressure_DiffusiveFlux( entity, quadrature[ pt ], vu, du, wpdu );
    model().capillary_Pressure_DiffusiveFlux(  entity, quadrature[ pt ], vu, du, cpdu );

    adu = wpdu;
    adu += gpdu;
    adu += cpdu;
    adu *= weight;
    // add to local function
    wLocalEn.axpy( quadrature[ pt ], avu, adu );
  }
}


template< class DiscreteFunction, class Model >
void FlowOperator< DiscreteFunction, Model >
:: internal_Bnd_terms( const EntityType& entity,
                       const int& quadOrder,
                       const GeometryType &geometry,
                       const IntersectionType &intersection,
                       const DiscreteFunctionSpaceType &dfSpace,
                       const LocalFunctionType& uLocal,
                       const DiscreteFunctionType& u,
                       LocalFunctionType& wLocalEn ) const
{
  const double area = entity.geometry().volume();

  if ( intersection.neighbor() )
  {
    // const EntityPointerType pOutside = intersection.outside(); // pointer to outside element.
    // const EntityType &outside = pOutside;
    const EntityType &outside = intersection.outside();

    typedef typename IntersectionType::Geometry  IntersectionGeometryType;
    const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

    const double intersectionArea = intersectionGeometry.volume();

    LocalFunctionType uOutLocal = u.localFunction( outside ); // local u on outisde element

    FaceQuadratureType quadInside( dfSpace.gridPart(), intersection, quadOrder, FaceQuadratureType::INSIDE );
    FaceQuadratureType quadOutside( dfSpace.gridPart(), intersection, quadOrder, FaceQuadratureType::OUTSIDE );
    const size_t numQuadraturePoints = quadInside.nop();
    //! [Compute skeleton terms: iterate over intersections]
    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      //! [Compute skeleton terms: obtain required values on the intersection]
      // get coordinate of quadrature point on the reference element of the intersection
      const typename FaceQuadratureType::LocalCoordinateType &x = quadInside.localPoint( pt );
      const DomainType normal = intersection.integrationOuterNormal( x );
      const double weight = quadInside.weight( pt );

      RangeType vu( RangeType( 0 ) );
      uLocal.evaluate( quadInside[ pt ], vu );
      JacobianRangeType du,dun;
      uLocal.jacobian( quadInside[ pt ], du );

      RangeType value( RangeType( 0 ) );
      JacobianRangeType dvalue, advalue, gpdadvalue( 0 ), wpdadvalue( 0 ), cpdadvalue( 0 );
      JacobianRangeType dvalue0( 0 ), dvalue1( 0 );

      RangeType vuIn( RangeType( 0 ) ), vuOut( RangeType( 0 ) ), jump( RangeType( 0 ) );

      JacobianRangeType dunIn,dunOut;

      JacobianRangeType duIn, aduIn, duOut, aduOut;

      JacobianRangeType gpduIn( 0 ), wpduIn( 0 ),cpduIn( 0 ), gpduOut( 0 ), wpduOut( 0 ),cpduOut( 0 );

      uLocal.evaluate( quadInside[ pt ], vuIn );
      uLocal.jacobian( quadInside[ pt ], duIn );

      double upwMob( 0 );
      double upwgradnonwetmob( 0 );
      double permeabilityIn( 0 ), permeabilityOut( 0 );
      model_.permeability_K( entity,quadInside[ pt ], permeabilityIn);
      model_.permeability_K( outside,quadOutside[ pt ], permeabilityOut);

      if ( with_upw == true  )
      {
        model_.upwindingMobility(entity,quadInside[ pt ],outside,quadOutside[ pt ],normal,vuIn,vuOut,duIn,duOut,upwMob,upwgradnonwetmob);
      }

      double betapress( 0 );
      double betasat( 0 );
      model_.penality_pressure(entity,quadInside[ pt ],outside,quadOutside[ pt ], betapress);
      model_.penality_saturation(entity,quadInside[ pt ],outside,quadOutside[ pt ], betasat);
      betapress *= intersectionArea / std::min( area, outside.geometry().volume() );
      betasat *= intersectionArea / std::min( area, outside.geometry().volume() );

      model().gravity_Flux( entity, quadInside[ pt ], vuIn, duIn, gpduIn, upwMob,with_upw );
      model().wetting_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, duIn, wpduIn,upwMob,with_upw );
      model().capillary_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, duIn, cpduIn , upwMob,with_upw );

      aduIn = wpduIn;
      aduIn += gpduIn;
      aduIn += cpduIn;

      uOutLocal.evaluate( quadOutside[ pt ], vuOut );
      uOutLocal.jacobian( quadOutside[ pt ], duOut );

      model().wetting_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue0, wpdadvalue, upwMob, with_upw );
      model().gravity_Flux( outside, quadOutside[ pt ], vuOut, duOut, gpduOut, upwMob, with_upw );
      model().wetting_Pressure_DiffusiveFlux( outside, quadOutside[ pt ], vuOut, duOut, wpduOut, upwMob, with_upw );
      model().capillary_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue1, cpdadvalue , upwMob, with_upw );
      model().capillary_Pressure_DiffusiveFlux( outside, quadOutside[ pt ], vuOut, duOut, cpduOut , upwMob, with_upw );

      advalue = wpdadvalue;
      // advalue+=cpdadvalue;

      aduOut = wpduOut;
      aduOut += gpduOut;
      aduOut += cpduOut;

      //! [Compute skeleton terms: obtain required values on the intersection]

      //! [Compute skeleton terms: compute factors for axpy method]
      // penalty term : beta [u] [phi] = beta (u+ - u-)(phi+ - phi-)=beta (u+ - u-)phi+

      RangeType cap_pressIn( RangeType( 0 ) ),cap_pressOut( RangeType( 0 ) );
      model_.cap_Press( entity, quadInside[ pt ], vuIn, cap_pressIn );
      model_.cap_Press( entity, quadOutside[ pt ], vuOut, cap_pressOut );

      jump[0] = vuIn[0] - vuOut[0];
      jump[1] = vuIn[1] - vuOut[1];

      value[0] = jump[0];
      value [1] = jump[1];//[sn]

      value[0] *= betapress * intersectionGeometry.integrationElement( x );
      value[1] *= betasat * intersectionGeometry.integrationElement( x );

      aduIn*=permeabilityOut;
      aduOut*=permeabilityIn;
      aduIn += aduOut;
      aduIn /= -(permeabilityIn+permeabilityOut);
      aduIn.umv(normal,value);
      //
      for (int r=0;r<dimRange;++r)
      {
        for (int d=0;d<dimDomain;++d)
        {
          dvalue0[0][d] = -( permeabilityOut / ( permeabilityIn+permeabilityOut ) ) *epsil* normal[d] * jump[0];
          dvalue0[1][d] = -( permeabilityOut / ( permeabilityIn+permeabilityOut ) ) *epsil* normal[d] * jump[0];

          dvalue1[0][d] = -( permeabilityOut / ( permeabilityIn+permeabilityOut ) ) *epsil* normal[d] * jump[1];
          dvalue1[1][d] = -( permeabilityOut / ( permeabilityIn+permeabilityOut ) ) *epsil* normal[d] * jump[1];
        }
      }
      value *= weight;
      advalue *= weight;
      wLocalEn.axpy( quadInside[ pt ], value, advalue );
    }
  }
  else if( intersection.boundary() && model().isDirichletIntersection( intersection ) )
  {
    typedef typename IntersectionType::Geometry  IntersectionGeometryType;
    const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

    const double intersectionArea = intersectionGeometry.volume();

    FaceQuadratureType quadInside( dfSpace.gridPart(), intersection, quadOrder, FaceQuadratureType::INSIDE );
    const size_t numQuadraturePoints = quadInside.nop();

    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename FaceQuadratureType::LocalCoordinateType &x = quadInside.localPoint( pt );
      const DomainType normal = intersection.integrationOuterNormal( x );
      const double weight = quadInside.weight( pt );

      RangeType value( RangeType( 0 ) );
      JacobianRangeType dvalue( 0 ), dvalue0( 0 ), dvalue1( 0 ),advalue( 0 ),gpdadvalue( 0 ),wpdadvalue( 0 ),cpdadvalue( 0 );

      double upwMob( 0 );
      // double upwgradnonwetmob( 0 );
      RangeType vuIn( RangeType( 0 ) ),jump( RangeType( 0 ) );
      JacobianRangeType duIn,dunIn, aduIn( 0 );
      JacobianRangeType gpduIn( 0 ), wpduIn( 0 ),cpduIn( 0 );

      uLocal.evaluate( quadInside[ pt ], vuIn );
      uLocal.jacobian( quadInside[ pt ], duIn );

      if (model().buildExplicitOp())
      {
        model().g( RangeType( 0 ), entity, quadInside.point(pt), vuIn );
      }
      else
      {
        model().gravity_Flux( entity, quadInside[ pt ], vuIn, duIn, gpduIn, upwMob );
        model().wetting_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, duIn, wpduIn);
        model().capillary_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, duIn, cpduIn);

        aduIn = wpduIn;
        aduIn += gpduIn;
        aduIn += cpduIn;
      }
      jump[0] = vuIn[0];
      jump[1] = vuIn[1];

      value = jump;

      double  betapress( 0 );
      double  betasat( 0 );
      model_.penality_pressure( entity, quadInside[ pt ], entity, quadInside[ pt ], betapress);
      model_.penality_saturation( entity, quadInside[ pt ], entity, quadInside[ pt ], betasat);
      betapress*= intersectionArea / area;
      betasat*= intersectionArea / area;

      value[0] *= betapress * intersectionGeometry.integrationElement( x );
      value[1] *= betasat * intersectionGeometry.integrationElement( x );

      aduIn.umv(normal,value);

      for (int r=0;r<dimRange;++r)
        for (int d=0;d<dimDomain;++d)
        {
          dvalue0[0][d] = -1 *epsil* normal[d] * jump[0];
          dvalue0[1][d] = -1 *epsil* normal[d] * jump[0];

          dvalue1[0][d] = -1 *epsil* normal[d] * jump[1];
          dvalue1[1][d] = -1 *epsil* normal[d] * jump[1];
        }

      if (model().buildExplicitOp())
      {
        model().g( RangeType( 0 ), entity, quadInside.point( pt ), vuIn );

        model_.wetting_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue0, wpdadvalue, upwMob, false, true );
        model_.capillary_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue1, cpdadvalue, upwMob, false, true );

        advalue=wpdadvalue;
        // advalue+=cpdadvalue;
      }
      else
      {
        //    model_.Gravity_Flux( vuIn, duIn, entity, quadInside[ pt ], vuIn, dvalue0, gpdadvalue );
        model_.wetting_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue0, wpdadvalue );
        model_.capillary_Pressure_DiffusiveFlux( entity, quadInside[ pt ], vuIn, dvalue1, cpdadvalue );
        advalue=wpdadvalue;
        //      advalue+=cpdadvalue;
      }

      value *= weight;
      advalue *= weight;
      wLocalEn.axpy( quadInside[ pt ], value, advalue );
    }
  }
  else if( intersection.boundary() && model().isNeumannIntersection( intersection ) )
  {
    typedef typename IntersectionType::Geometry  IntersectionGeometryType;

    FaceQuadratureType quadInside( dfSpace.gridPart(), intersection, quadOrder, FaceQuadratureType::INSIDE );
    const size_t numQuadraturePoints = quadInside.nop();

    for( size_t pt = 0; pt < numQuadraturePoints; ++pt )
    {
      const typename FaceQuadratureType::LocalCoordinateType &x = quadInside.localPoint( pt );
      const DomainType normal = intersection.integrationOuterNormal( x );
      double faceVol = normal.two_norm();

      const double weight = quadInside.weight( pt )*faceVol;

      RangeType value( RangeType( 0 ) );
      JacobianRangeType dvalue( 0 ), dvalue0( 0 ), dvalue1( 0 ),advalue( 0 ),wpdadvalue( 0 ),cpdadvalue( 0 );

      RangeType vuIn( RangeType( 0 ) ), jump(RangeType( 0 ) );
      JacobianRangeType duIn,dunIn, aduIn( 0 );
      JacobianRangeType wpduIn( 0 ),cpduIn( 0 );

      uLocal.jacobian( quadInside[ pt ], duIn );

      if (model().buildExplicitOp())
      {
        model().gN( RangeType( 0 ), entity, quadInside.point( pt ), vuIn );
      }
      else
      {
        vuIn = 0;
      }

      jump[0] = -1* vuIn[0];
      jump[1] = -1*vuIn[1];

      value = jump;

      value *= weight;
      advalue = 0;
      wLocalEn.axpy( quadInside[ pt ], value, advalue );
    }
  }
  else
  {
  }
}


template< class JacobianOperator, class Model >
void DifferentiableFlowOperator< JacobianOperator, Model >
::element_Local_Contribution(const EntityType& entity,
                             const GeometryType &geometry,
                             const DiscreteFunctionSpaceType &dfSpace,
                             const BasisFunctionSetType &baseSet,
                             const LocalFunctionType& uLocal,
                             LocalMatrixType& jLocal) const
{
  QuadratureType quadrature( entity, 2*dfSpace.order()+1 );
  const int blockSize = dfSpace.localBlockSize; // is equal to 1 for scalar functions
  std::vector< typename LocalFunctionType::RangeType > phi( dfSpace.blockMapper().maxNumDofs()*blockSize );
  std::vector< typename LocalFunctionType::JacobianRangeType > dphi( dfSpace.blockMapper().maxNumDofs()*blockSize );
  const unsigned int numBasisFunctions = baseSet.size();

  const size_t numQuadPoints = quadrature.nop();
  for( size_t pt = 0; pt < numQuadPoints; ++pt )
  {

    const typename QuadratureType::CoordinateType &x = quadrature.point( pt );
    const double weight = quadrature.weight( pt ) * geometry.integrationElement( x );

    // evaluate all basis functions at given quadrature point
    baseSet.evaluateAll( quadrature[ pt ], phi );

    // evaluate jacobians of all basis functions at given quadrature point
    baseSet.jacobianAll( quadrature[ pt ], dphi );

    RangeType u0( RangeType( 0 ) );
    uLocal.evaluate( quadrature[ pt ], u0 );
    JacobianRangeType jacU0;
    uLocal.jacobian( quadrature[ pt ], jacU0 );

    RangeType aphi( RangeType( 0 ) );
    JacobianRangeType adphi( 0 ), lgpdphi( 0 ),lwpdphi( 0 ),cpdphi( 0 );
    for( unsigned int localCol = 0; localCol < numBasisFunctions; ++localCol )
    {
      // if mass terms or right hand side is present
      model().linSource( u0, entity, quadrature[ pt ], phi[ localCol ], aphi );
      // if gradient term is present
      model().linGravity_Flux( u0, jacU0, entity, quadrature[ pt ], phi[ localCol ], dphi[ localCol ], lgpdphi );
      model().linWetting_Pressure_DiffusiveFlux( u0, jacU0, entity, quadrature[ pt ], phi[ localCol ], dphi[ localCol ], lwpdphi );
      model().linCapillary_Pressure_DiffusiveFlux( u0, jacU0, entity, quadrature[ pt ], phi[ localCol ], dphi[ localCol ], cpdphi );
      adphi=lwpdphi;
      adphi+=lgpdphi;
      adphi+=cpdphi;
      // get column object and call axpy method
      jLocal.column( localCol ).axpy( phi, dphi, aphi, adphi, weight );
    }
  }
}



template< class JacobianOperator, class Model >
void DifferentiableFlowOperator< JacobianOperator, Model >
::interface_Local_Contribution(const EntityType& entity,
                               const GeometryType &geometry,
                               const DiscreteFunctionSpaceType &dfSpace,
                               const BasisFunctionSetType &baseSet,
                               const IntersectionType& intersection,
                               const LocalFunctionType& uLocal,
                               const DiscreteFunctionType& u,
                               const JacobianOperatorType &jOp,
                               LocalMatrixType& jLocal) const
{
  double area = geometry.volume();

  const GridPartType& gridPart = dfSpace.gridPart();
  const int blockSize = dfSpace.localBlockSize; // is equal to 1 for scalar functions
  std::vector< typename LocalFunctionType::RangeType > phi( dfSpace.blockMapper().maxNumDofs()*blockSize );
  std::vector< typename LocalFunctionType::JacobianRangeType > dphi( dfSpace.blockMapper().maxNumDofs()*blockSize );
  std::vector< typename LocalFunctionType::RangeType > phiNb( dfSpace.blockMapper().maxNumDofs()*blockSize );
  std::vector< typename LocalFunctionType::JacobianRangeType > dphiNb( dfSpace.blockMapper().maxNumDofs()*blockSize );

  const unsigned int numBasisFunctions = baseSet.size();


  if( intersection.neighbor() )
  {
    // EntityPointerType ep = intersection.outside();
    // const EntityType& neighbor = ep;
    const EntityType& neighbor = intersection.outside();

    typedef typename IntersectionType::Geometry  IntersectionGeometryType;
    const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

    const LocalFunctionType uOutLocal = u.localFunction( neighbor );

    //! [Assemble skeleton terms: get contributions on off diagonal block]
    // get local matrix for face entries
    LocalMatrixType localOpNb = jOp.localMatrix( neighbor, entity );
    // get neighbor's base function set
    const BasisFunctionSetType &baseSetNb = localOpNb.domainBasisFunctionSet();
    //! [Assemble skeleton terms: get contributions on off diagonal block]

    const double intersectionArea = intersectionGeometry.volume();

    FaceQuadratureType faceQuadInside(gridPart, intersection, 2*dfSpace.order() + 1,
              FaceQuadratureType::INSIDE);
    FaceQuadratureType faceQuadOutside(gridPart, intersection, 2*dfSpace.order() + 1,
               FaceQuadratureType::OUTSIDE);

    const size_t numFaceQuadPoints = faceQuadInside.nop();
    for( size_t pt = 0; pt < numFaceQuadPoints; ++pt )
    {
      const typename FaceQuadratureType::LocalCoordinateType &x = faceQuadInside.localPoint( pt );
      DomainType normal = intersection.integrationOuterNormal( x );
      double faceVol = normal.two_norm();
      normal /= faceVol; // make it into a unit normal

      const double quadWeight = faceQuadInside.weight( pt );
      const double weight = quadWeight * faceVol;

      //! [Assemble skeleton terms: obtain values om quadrature point]
      RangeType u0En( RangeType( 0 ) );
      JacobianRangeType u0EnJac;
      uLocal.evaluate( faceQuadInside[ pt ], u0En );
      uLocal.jacobian( faceQuadInside[ pt ], u0EnJac );

      RangeType u0OutEn( RangeType( 0 ) );
      JacobianRangeType u0OutEnJac;
      uOutLocal.evaluate( faceQuadOutside[ pt ], u0OutEn );
      uOutLocal.jacobian( faceQuadOutside[ pt ], u0OutEnJac );

      /////////////////////////////////////////////////////////////
      // evaluate basis function of face inside E^- (entity)
      /////////////////////////////////////////////////////////////

      // evaluate all basis functions for quadrature point pt
      baseSet.evaluateAll( faceQuadInside[ pt ], phi );

      // evaluate the jacobians of all basis functions
      baseSet.jacobianAll( faceQuadInside[ pt ], dphi );

      /////////////////////////////////////////////////////////////
      // evaluate basis function of face inside E^+ (neighbor)
      /////////////////////////////////////////////////////////////

      // evaluate all basis functions for quadrature point pt on neighbor
      baseSetNb.evaluateAll( faceQuadOutside[ pt ], phiNb );

      // evaluate the jacobians of all basis functions on neighbor
      baseSetNb.jacobianAll( faceQuadOutside[ pt ], dphiNb );

      JacobianRangeType dvalue0En( 0 ), dvalue0Nb( 0 ), dvalue1En( 0 ), dvalue1Nb( 0 ), advalueEn( 0 ),advalueNb( 0 );

      for( unsigned int i = 0; i < numBasisFunctions; ++i )
      {
        JacobianRangeType lgpdphiEn( 0 ), lwpdphiEn( 0 ),cpdphiEn( 0 ), lgpdphiNb( 0 ), lwpdphiNb( 0 ),cpdphiNb( 0 ),gpdadvalueEn( 0 ), wpdadvalueEn( 0 ), cpdadvalueEn( 0 ), gpdadvalueNb( 0 ), wpdadvalueNb( 0 ), cpdadvalueNb( 0 );
        JacobianRangeType adphiEn = dphi[ i ];
        JacobianRangeType adphiNb = dphiNb[ i ];
        double upwMob( 0 );
        double upwgradnonwetmob( 0 );

        double permeabilityIn, permeabilityOut;
        model().permeability_K( entity, faceQuadInside[ pt ], permeabilityIn );
        model().permeability_K( neighbor, faceQuadOutside[ pt ], permeabilityOut );

        if ( with_upw == true )
        {
          model().upwindingMobility( entity, faceQuadInside[ pt ], neighbor, faceQuadOutside[ pt ], normal, u0En, u0OutEn, u0EnJac, u0OutEnJac, upwMob, upwgradnonwetmob );
        }

        model().linGravity_Flux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], adphiEn, lgpdphiEn, upwMob, upwgradnonwetmob, with_upw );
        model().linWetting_Pressure_DiffusiveFlux( u0En, u0EnJac, entity, faceQuadInside[ pt ], phi[i], adphiEn, lwpdphiEn, upwMob, upwgradnonwetmob, with_upw );
        model().linCapillary_Pressure_DiffusiveFlux( u0En, u0EnJac, entity, faceQuadInside[ pt ], phi[i], adphiEn, cpdphiEn);

        dphi[ i ] = lwpdphiEn;
        dphi[ i ] += lgpdphiEn;
        dphi[ i ] += cpdphiEn;

        model().linGravity_Flux(u0OutEn, u0OutEnJac, neighbor, faceQuadOutside[ pt ], phiNb[i], adphiNb, lgpdphiNb , upwMob, upwgradnonwetmob, with_upw );
        model().linWetting_Pressure_DiffusiveFlux( u0OutEn, u0OutEnJac,  neighbor, faceQuadOutside[ pt ], phiNb[i], adphiNb, lwpdphiNb , upwMob, upwgradnonwetmob, with_upw );
        model().linWetting_Pressure_DiffusiveFlux( u0En, u0EnJac, entity, faceQuadInside[ pt ], phi[i], dvalue0En, wpdadvalueEn, upwMob, upwgradnonwetmob, with_upw );
        model().linWetting_Pressure_DiffusiveFlux( u0OutEn, u0OutEnJac, neighbor, faceQuadOutside[ pt ], phiNb[i], dvalue0Nb, wpdadvalueNb, upwMob, upwgradnonwetmob, with_upw );
        model().linCapillary_Pressure_DiffusiveFlux(  u0OutEn, u0OutEnJac,neighbor, faceQuadOutside[ pt ], phiNb[i], adphiNb, cpdphiNb );
        model().linCapillary_Pressure_DiffusiveFlux(  u0OutEn, u0OutEnJac,  neighbor, faceQuadOutside[ pt ], phiNb[i], dvalue1Nb, cpdadvalueNb );
        model().linCapillary_Pressure_DiffusiveFlux( u0En, u0EnJac,  entity,   faceQuadInside[ pt ], phi[i], dvalue1En, cpdadvalueEn );

        dphiNb[ i ] = lwpdphiNb;
        dphiNb[ i ] += lgpdphiNb;
        dphiNb[ i ] += cpdphiNb;

        // model().linGravity_Flux( u0En, u0EnJac, u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], dvalue0En, gpdadvalueEn);
        // model().linGravity_Flux(  u0OutEn, u0OutEnJac, u0En, u0EnJac, neighbor,   faceQuadOutside[ pt ], phiNb[i], dvalue0Nb, gpdadvalueNb);

        advalueEn = wpdadvalueEn;
        //   advalueEn += cpdadvalueEn;
        advalueNb = wpdadvalueNb;
        //   advalueNb += cpdadvalueNb;
      }

      //! [Assemble skeleton terms: compute factors for axpy method]
      for( unsigned int localCol = 0; localCol < numBasisFunctions; ++localCol )
      {
        RangeType valueEn( RangeType( 0 ) ), valueNb( RangeType( 0 ) );
        JacobianRangeType dvalueEn( 0 ), dvalueNb( 0 );
        double Kin( 0 ),Kout( 0 );
        model().permeability_K( entity, faceQuadInside[ pt ], Kin);
        model().permeability_K( neighbor, faceQuadOutside[ pt ], Kout);

        dphi[localCol].usmv( -( Kout / ( Kin + Kout ) ), normal, valueEn );

        dphiNb[localCol].usmv( -( Kin /( Kin + Kout ) ), normal, valueNb );

        double  betapress( 0 );
        double  betasat( 0 );
        model().penality_pressure( entity, faceQuadInside[ pt ], neighbor, faceQuadOutside[ pt ], betapress );
        model().penality_saturation( entity, faceQuadInside[ pt ], neighbor, faceQuadOutside[ pt ], betasat );

        betapress*= intersectionArea / std::min( area, neighbor.geometry().volume() );
        betasat*= intersectionArea / std::min( area, neighbor.geometry().volume() );

        valueEn[0] += betapress*phi[localCol][0];
        valueEn[1] += betasat*phi[localCol][1];
        valueNb[0] -= betapress*phiNb[localCol][0];
        valueNb[1] -= betasat*phiNb[localCol][1];

        for ( int r = 0; r < dimRange; ++r )
          for ( int d = 0; d < dimDomain; ++d )
          {
            dvalue0En[r][d] = -(Kout/(Kin+Kout)) *epsil* normal[d] * phi[localCol][0];
            dvalue1En[r][d] = -(Kout/(Kin+Kout)) *epsil* normal[d] * phi[localCol][1];
            //
            dvalue0Nb[r][d] = (Kin/(Kin+Kout)) *epsil* normal[d] * phiNb[localCol][0];
            dvalue1Nb[r][d] = (Kin/(Kin+Kout)) *epsil* normal[d] * phiNb[localCol][1];
          }

        jLocal.column( localCol ).axpy( phi, dphi, valueEn, advalueEn, weight );
        localOpNb.column( localCol ).axpy( phi, dphi, valueNb, advalueNb, weight );
      }
      //! [Assemble skeleton terms: compute factors for axpy method]
    }
  }
  else if( intersection.boundary() )
  {
    if ( ! model().isDirichletIntersection( intersection ) )
    {
    }

    else
    {
      typedef typename IntersectionType::Geometry  IntersectionGeometryType;
      const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

      const double intersectionArea = intersectionGeometry.volume();

      FaceQuadratureType faceQuadInside(gridPart, intersection, 2*dfSpace.order() + 1,
                FaceQuadratureType::INSIDE);

      const size_t numFaceQuadPoints = faceQuadInside.nop();
      for( size_t pt = 0; pt < numFaceQuadPoints; ++pt )
      {
        const typename FaceQuadratureType::LocalCoordinateType &x = faceQuadInside.localPoint( pt );
        DomainType normal = intersection.integrationOuterNormal( x );
        double faceVol = normal.two_norm();
        normal /= faceVol; // make it into a unit normal

        const double quadWeight = faceQuadInside.weight( pt );
        const double weight = quadWeight * faceVol;

        RangeType u0En( RangeType( 0 ) );
        JacobianRangeType u0EnJac;
        uLocal.evaluate( faceQuadInside[ pt ], u0En );
        uLocal.jacobian( faceQuadInside[ pt ], u0EnJac );

        /////////////////////////////////////////////////////////////
        // evaluate basis function of face inside E^- (entity)
        /////////////////////////////////////////////////////////////

        // evaluate all basis functions for quadrature point pt
        baseSet.evaluateAll( faceQuadInside[ pt ], phi );

        // evaluate the jacobians of all basis functions
        baseSet.jacobianAll( faceQuadInside[ pt ], dphi );

        // RangeType cap_pressEn( RangeType( 0 ) );
        JacobianRangeType dvalue0En( 0 ),dvalue1En( 0 ), advalueEn( 0 );

          for( unsigned int i = 0; i < numBasisFunctions; ++i )
          {
          JacobianRangeType adphiEn = dphi[ i ];
          JacobianRangeType lgpdphiEn( 0 ), lwpdphiEn( 0 ),cpdphiEn( 0 );
          JacobianRangeType gpdadvalueEn( 0 ), wpdadvalueEn( 0 ), cpdadvalueEn( 0 );

          model().linGravity_Flux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], adphiEn, lgpdphiEn );

          model().linWetting_Pressure_DiffusiveFlux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], adphiEn, lwpdphiEn );
          model().linCapillary_Pressure_DiffusiveFlux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], adphiEn, cpdphiEn );

          dphi[ i ] = lwpdphiEn;
          dphi[ i ] += lgpdphiEn;
          dphi[ i ] += cpdphiEn;
          model().linWetting_Pressure_DiffusiveFlux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], dvalue0En, wpdadvalueEn );
          model().linCapillary_Pressure_DiffusiveFlux( u0En, u0EnJac, entity,   faceQuadInside[ pt ], phi[i], dvalue1En, cpdadvalueEn );

          advalueEn = wpdadvalueEn;
          //advalueEn+=cpdadvalueEn;
        }

        for( unsigned int localCol = 0; localCol < numBasisFunctions; ++localCol )
        {
          RangeType valueEn( RangeType( 0 ) );
          JacobianRangeType dvalueEn( 0 );

          dphi[localCol].usmv( -1.0 , normal, valueEn );

          double  betapress( 0 );
          double  betasat( 0 );
          model().penality_pressure(entity,faceQuadInside[ pt ],entity,faceQuadInside[ pt ], betapress);
          model().penality_saturation(entity,faceQuadInside[ pt ],entity,faceQuadInside[ pt ], betasat);
          betapress *=  intersectionArea / area;
          betasat *=  intersectionArea / area;

          valueEn[0] += betapress*phi[localCol][0];
          valueEn[1] += betasat*phi[localCol][1];

          for ( int r=0; r< dimRange; ++r )
            for ( int d=0; d< dimDomain; ++d )
            {
              dvalue0En[r][d] = - 1.0 * epsil * normal[d] * phi[localCol][0];
              dvalue1En[r][d] = - 1.0 * epsil * normal[d] * phi[localCol][1];
            }

          jLocal.column( localCol ).axpy( phi, dphi, valueEn, advalueEn, weight );
        }
      }

    }
  }

}


// Implementation of FlowOperator
// ----------------------------------

template< class DiscreteFunction, class Model >
void FlowOperator< DiscreteFunction, Model >
::operator() ( const DiscreteFunctionType &u, DiscreteFunctionType &w ) const
{
  // clear destination
  w.clear();

  // get discrete function space
  const DiscreteFunctionSpaceType &dfSpace = w.space();

  // iterate over grid
  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    // get entity (here element)
    const EntityType &entity = *it;
    // get elements geometry
    const GeometryType &geometry = entity.geometry();

    // get local representation of the discrete functions
    const LocalFunctionType uLocal = u.localFunction( entity );
    LocalFunctionType wLocal = w.localFunction( entity );

    // obtain quadrature order
    const int quadOrder = uLocal.order() + wLocal.order();

    //Computing local contribution from elements
    volumetricPart( entity, quadOrder, geometry, uLocal, wLocal );

    if ( ! dfSpace.continuous() )
    {
      const IntersectionIteratorType iitend = dfSpace.gridPart().iend( entity );
      for( IntersectionIteratorType iit = dfSpace.gridPart().ibegin( entity ); iit != iitend; ++iit ) // looping over intersections
      {
        const IntersectionType &intersection = *iit;
        //Computing local contribution from interfaces and boundaries
        internal_Bnd_terms( entity, quadOrder, geometry, intersection, dfSpace, uLocal, u, wLocal );
      }
    }
  }

  // communicate data (in parallel runs)
  w.communicate();
}

// Implementation of DifferentiableFlowOperator
// ------------------------------------------------

template< class JacobianOperator, class Model >
void DifferentiableFlowOperator< JacobianOperator, Model >
::jacobian ( const DiscreteFunctionType &u, JacobianOperator &jOp ) const
{
  typedef typename JacobianOperator::LocalMatrixType LocalMatrixType;
  typedef typename DiscreteFunctionSpaceType::BasisFunctionSetType BasisFunctionSetType;

  const DiscreteFunctionSpaceType &dfSpace = u.space();
  const GridPartType& gridPart = dfSpace.gridPart();
  Dune::Fem::DiagonalAndNeighborStencil<DiscreteFunctionSpaceType,DiscreteFunctionSpaceType> stencil( dfSpace, dfSpace );
  jOp.reserve(stencil);
  jOp.clear();

  const IteratorType end = dfSpace.end();
  for( IteratorType it = dfSpace.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;
    const GeometryType &geometry = entity.geometry();

    const LocalFunctionType uLocal = u.localFunction( entity );

    LocalMatrixType jLocal = jOp.localMatrix( entity, entity );

    const BasisFunctionSetType &baseSet = jLocal.domainBasisFunctionSet();

    element_Local_Contribution( entity, geometry, dfSpace, baseSet, uLocal, jLocal );

    if ( dfSpace.continuous() )
      continue;

    const IntersectionIteratorType endiit = gridPart.iend( entity );
    for ( IntersectionIteratorType iit = gridPart.ibegin( entity ); iit != endiit ; ++ iit )
    {
      const IntersectionType& intersection = *iit ;
      //Computing local contribution from interfaces and boundaries
      interface_Local_Contribution( entity, geometry, dfSpace, baseSet, intersection, uLocal, u, jOp, jLocal );
    } //! [Assembling the local matrix]
  } // end grid traversal

  jOp.communicate();
}
#endif // #ifndef OPERATOR_HH
