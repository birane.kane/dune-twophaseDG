#ifndef DUNE_SANDLENS_NEWTONINVERSEOPERATOR_HH
#define DUNE_SANDLENS_NEWTONINVERSEOPERATOR_HH

#include <cfloat>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <numeric>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/operator/common/operator.hh>
#include <dune/fem/operator/common/differentiableoperator.hh>

namespace Dune
{

  namespace Fem
  {

    struct NewtonParameter
      #ifndef DOXYGEN
      : public LocalParameter< NewtonParameter, NewtonParameter>
      #endif
    {
      NewtonParameter(){}

      virtual double toleranceParameter () const
      {
        return Parameter::getValue< double >( "fem.solver.newton.tolerance", 1e-6 );
      }

      virtual double linAbsTolParameter ( const double &tolerance )  const
      {
        return Parameter::getValue< double >( "fem.solver.newton.linabstol", tolerance / 8 );
      }

      virtual double linReductionParameter ( const double &tolerance ) const
      {
        return Parameter::getValue< double >( "fem.solver.newton.linreduction", tolerance / 8 );
      }

      virtual bool verbose () const
      {
        const bool v = Parameter::getValue< bool >( "fem.solver.verbose", false );
        return Parameter::getValue< bool >( "fem.solver.newton.verbose", v );
      }

      virtual bool linearSolverVerbose () const
      {
        const bool v = Parameter::getValue< bool >( "fem.solver.verbose", false );
        return Parameter::getValue< bool >( "fem.solver.newton.linear.verbose", v );
      }

      virtual int maxIterationsParameter () const
      {
        return Parameter::getValue< int >( "fem.solver.newton.maxiterations", std::numeric_limits< int >::max() );
      }

      virtual int maxLinearIterationsParameter () const
      {
        return Parameter::getValue< int >( "fem.solver.newton.maxlineariterations", std::numeric_limits< int >::max() );
      }
    };

    /** \class NewtonInverseOperator >
     *  \brief inverse operator based on a newton scheme
     *
     *  \tparam  Op      operator to invert (must be a DifferentiableOperator)
     *  \tparam  LInvOp  linear inverse operator
     *
     *  \note Verbosity of the NewtonInverseOperator is controlled via the
     *        paramter <b>fem.solver.newton.verbose</b>; it defaults to
     *        <b>fem.solver.verbose</b>.
     */
    template< class JacobianOperator, class LInvOp>
    class NewtonInverseOperator
      : public Operator< typename JacobianOperator :: DomainFunctionType,
      typename JacobianOperator :: RangeFunctionType >
    {
      typedef NewtonInverseOperator< JacobianOperator, LInvOp > ThisType;
      typedef Operator< typename JacobianOperator :: DomainFunctionType,
      typename JacobianOperator :: RangeFunctionType > BaseType;

    public:
      //! type of operator's Jacobian
      typedef JacobianOperator JacobianOperatorType;

      //! type of operator to invert
      typedef DifferentiableOperator<JacobianOperatorType> OperatorType;

      //! type of linear inverse operator
      typedef LInvOp LinearInverseOperatorType;

      typedef typename BaseType::DomainFunctionType DomainFunctionType;
      typedef typename BaseType::RangeFunctionType RangeFunctionType;

      typedef typename BaseType::DomainFieldType DomainFieldType;

      /** constructor
       *
       *  \param[in]  op       operator to invert
       *
       *  \note The tolerance is read from the paramter
       *        <b>fem.solver.newton.tolerance</b>
       */
      explicit NewtonInverseOperator ( const OperatorType &op,
        const NewtonParameter &parameter = NewtonParameter() )
        : op_( op ),
        tolerance_( parameter.toleranceParameter() ),
        linAbsTol_( parameter.linAbsTolParameter( tolerance_ ) ),
        linReduction_( parameter.linReductionParameter( tolerance_ ) ),
        verbose_( parameter.verbose() ),
        linVerbose_( parameter.linearSolverVerbose() ),
        maxIterations_( parameter.maxIterationsParameter() ),
        maxLinearIterations_( parameter.maxLinearIterationsParameter() )
      {}

      /** constructor
       *
       *  \param[in]  op       operator to invert
       *  \param[in]  epsilon  tolerance for norm of residual
       */
      NewtonInverseOperator ( const OperatorType &op, const DomainFieldType &epsilon,
        const NewtonParameter &parameter = NewtonParameter() )
        : op_( op ),
        tolerance_( epsilon ),
        linAbsTol_( parameter.linAbsTolParameter( tolerance_ ) ),
        linReduction_( parameter.linReductionParameter( tolerance_ ) ),
        verbose_( parameter.verbose() ),
        linVerbose_( parameter.linearSolverVerbose() ),
        maxIterations_( parameter.maxIterationsParameter() ),
        maxLinearIterations_( parameter.maxLinearIterationsParameter() )
        {}

      virtual void operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const;

      int iterations() const { return iterations_; }

      int linearIterations() const { return linearIterations_; }

      bool maxIterNotReached() const
      {
        return (iterations_ < maxIterations_) && (linearIterations_ < maxLinearIterations_);
      }

      double avgAssTime()
      {
        return avgAssemTime_;
      }

      double avgInvSolveTime()
      {
        return avgInvSolveTime_;
      }

      double avgNbIt()
      {
        return avgnbit_;
      }

    private:
      const OperatorType &op_;
      const double tolerance_, linAbsTol_, linReduction_;
      const bool verbose_;
      const bool linVerbose_;
      const int maxIterations_;
      const int maxLinearIterations_;

      mutable int iterations_;
      mutable int linearIterations_;

      mutable double avgAssemTime_;
      mutable double avgInvSolveTime_;
      mutable double avgnbit_;

    };

    template< class JacobianOperator, class LInvOp >
    inline void NewtonInverseOperator< JacobianOperator, LInvOp >
    ::operator() ( const DomainFunctionType &u, RangeFunctionType &w ) const
    {
      std::vector<double> assemTimeVect_(0, 0), invsolveTimeVect_(0, 0), nbiterVect_(0, 0);
      DomainFunctionType residual( u );
      RangeFunctionType dw( w );
      JacobianOperatorType jOp( "jacobianOperator", dw.space(), u.space() );

      // compute initial residual
      op_( w, residual );
      residual -= u;
      DomainFieldType delta = std::sqrt( residual.scalarProductDofs( residual ) );

      iterations_=0;
      linearIterations_ = 0;

      while (maxIterNotReached() && (delta > tolerance_))
       {
        if( verbose_ )
        std::cerr << "Newton iteration " << iterations_ << ": |residual| = " << delta << std::endl;

        // evaluate operator's jacobian
        Dune::Timer assembltimer;
        op_.jacobian( w, jOp );
        const double assemTime = assembltimer.elapsed();
        //
        const int remLinearIts = maxLinearIterations_ - linearIterations_;
        const LinearInverseOperatorType jInv( jOp, linReduction_, linAbsTol_ / delta, remLinearIts, linVerbose_ );

        int nbrit(0);

        dw.clear();
        Dune::Timer invtimer;
        jInv( residual, dw );
        const double invsolveTime = invtimer.elapsed();
        linearIterations_ += jInv.iterations();
        nbrit=jInv.iterations();

        w -= dw;

        op_( w, residual );
        residual -= u;

        delta = std::sqrt( residual.scalarProductDofs( residual ) );

        nbiterVect_.push_back(nbrit);
        avgnbit_ = std::accumulate(nbiterVect_.cbegin(), nbiterVect_.cend(), 0.0);
        avgnbit_ /= nbiterVect_.size();

        assemTimeVect_.push_back(assemTime);
        avgAssemTime_ = std::accumulate(assemTimeVect_.cbegin(), assemTimeVect_.cend(), 0.0);
        avgAssemTime_ /= assemTimeVect_.size();

        invsolveTimeVect_.push_back(invsolveTime);
        avgInvSolveTime_ = std::accumulate(invsolveTimeVect_.cbegin(), invsolveTimeVect_.cend(), 0.0);
        avgInvSolveTime_ /= invsolveTimeVect_.size();

        iterations_++;
      }
      if( verbose_ )
      std::cerr << "Newton iteration " << iterations_ << ": |residual| = " << delta << std::endl;

    }

  } // namespace Fem

} // namespace Dune

#endif // #ifndef DUNE_SANDLENS_NEWTONINVERSEOPERATOR_HH
