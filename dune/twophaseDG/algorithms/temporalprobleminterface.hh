#ifndef TEMPORAL_PROBLEMINTERFACE_HH
#define TEMPORAL_PROBLEMINTERFACE_HH

#include <cassert>
#include <cmath>

#include <dune/fem/solver/timeprovider.hh>

#include "probleminterface.hh"

template <class FunctionSpace>
class TemporalProblemInterface : public ProblemInterface<FunctionSpace>
{
public:
  typedef Dune::Fem::TimeProviderBase  TimeProviderType ;

  //! constructor taking time provider
  TemporalProblemInterface( const TimeProviderType &timeProvider )
    : timeProvider_(timeProvider)
  {
  }

  //! return current simulation time
  double time() const
  {
    return timeProvider_.time();
  }

  //! return current time step size (\f$ \delta t \f$)
  double deltaT() const
  {
    return timeProvider_.deltaT() ;
  }

  //! return reference to Problem's time provider
  const TimeProviderType & timeProvider() const
  {
    return timeProvider_;
  }

protected:
  const TimeProviderType &timeProvider_;
};
#endif // #ifndef TEMPORAL_PROBLEMINTERFACE_HH
