#ifndef NNRELPERM_HH
#define NNRELPERM_HH
#define PI 3.141592653589793238463
#define N

// #define epsilon 0.05
#define epoch 500

using namespace std;
extern "C" FILE *popen(const char *command, const char *mode);

//- Dune-fem includes
#include <dune/fem/quadrature/caching/twistutility.hh>
#include <dune/fem/quadrature/cachingquadrature.hh>
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/operator/matrix/blockmatrix.hh>
// Local includes
#include "../models/phaseflowmodel.hh"
#include "../algorithms/phaseflowscheme.hh"



// //Shuffling made right
//Added linear activation beside tanh
#include <iostream>
#include<vector>
#include <list>
#include <cstdlib>
#include <math.h>



// ---------
template< class DiscreteFunction, class ImplicitModel, class ExplicitModel >
class Nnrelperm
{
  typedef Nnrelperm< DiscreteFunction,ImplicitModel,ExplicitModel > ThisType;

public:
  typedef DiscreteFunction DiscreteFunctionType;

  typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
  DiscreteFunctionSpaceType;
  typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;

  typedef typename DiscreteFunctionSpaceType :: DomainFieldType DomainFieldType;
  typedef typename DiscreteFunctionSpaceType :: RangeFieldType RangeFieldType;
  typedef typename DiscreteFunctionSpaceType :: DomainType DomainType;
  typedef typename DiscreteFunctionSpaceType :: RangeType RangeType;
  typedef typename DiscreteFunctionSpaceType :: JacobianRangeType JacobianRangeType;
  typedef typename DiscreteFunctionSpaceType :: GridPartType GridPartType;
  typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType;

  typedef typename GridPartType :: GridType GridType;
  typedef typename GridPartType :: IndexSetType IndexSetType;
  typedef typename GridPartType :: IntersectionIteratorType IntersectionIteratorType;

  typedef typename IntersectionIteratorType :: Intersection IntersectionType;

  typedef typename GridType :: template Codim< 0 > :: Entity ElementType;
  typedef typename GridType :: template Codim< 0 > :: Entity ElementPointerType;
  typedef typename ElementType::Geometry GeometryType;
  static const int dimension = GridType :: dimension;

  typedef Dune::Fem::CachingQuadrature< GridPartType, 0 > ElementQuadratureType;
  typedef Dune::Fem::CachingQuadrature< GridPartType, 1 > FaceQuadratureType;

  typedef Dune::FieldMatrix<double,dimension,dimension> JacobianInverseType;
  typedef std :: vector< double > ErrorIndicatorType;

  typedef Dune::Fem::FunctionSpace< double, double, GridType::dimensionworld, 2 > FunctionSpaceType;

  typedef ImplicitModel ImplicitModelType;


private:
  const DiscreteFunctionType &uh_;
  const DiscreteFunctionSpaceType &dfSpace_;
  GridPartType &gridPart_;
  const IndexSetType &indexSet_;
  GridType &grid_;
  ErrorIndicatorType indicator_;
  const int maxLevel_;
  DiscreteFunctionType un_;
  ImplicitModelType model_;


    //double sigmoid(double x) { return 1.0f / (1.0f + exp(-x)); }
    //double dsigmoid(double x) { return x * (1.0f - x); }
    double tanh(double x) { return (exp(x)-exp(-x))/(exp(x)+exp(-x)) ;}
    double dtanh(double x) {return 1.0f - x*x ;}

    double lin(double x) { return x;}
    double dlin(double x) { return 1.0f;}

    double init_weight() { return (2.*rand()/RAND_MAX -1); }
    double MAXX = -9999999999999999; //maximum value of input example
    //double init_weight() { return ((double)rand())/((double)RAND_MAX); }

    int numInputs = 1;
    int numHiddenNodes = 5;
    int numOutputs = 1;

    const double lr = 0.05f;

    double hiddenLayer[5];
    double outputLayer[1];

    double hiddenLayerBias[5];
    double outputLayerBias[1];

    double hiddenWeights[1][5];
    double outputWeights[5][1];

    int numTrainingSets = 30;
    double training_inputs[30][1];
    double training_outputs[30][1];

protected:
  const ImplicitModelType &model () const { return model_; }
  const DiscreteFunctionType &un () const { return un_; }

public:
  explicit Nnrelperm ( const ImplicitModelType &model, const DiscreteFunctionType &uh )
    : uh_( uh ),
      dfSpace_( uh.space() ),
      gridPart_( dfSpace_.gridPart() ),
      indexSet_( gridPart_.indexSet() ),
      grid_( gridPart_.grid() ),
      indicator_( indexSet_.size( 0 ) ),
      maxLevel_( Dune::Fem::Parameter::getValue< int >( "phaseflow.maxlevel", 5 ) ), un_( uh_ ),
      model_( model )
  {}

  void setUn( DiscreteFunctionType &u )
  {
    un_.assign( u );
  }

  //! mark all elements due to given tolerance
  bool mark ( const double tolerance ) const
  {
    int marked = 0;
    // loop over all elements
    const IteratorType end = dfSpace_.end();
    for( IteratorType it = dfSpace_.begin(); it != end; ++it )
      {
        const ElementType &entity = *it;

        const Dune::ReferenceElement< double, dimension > &refElement
        = Dune::ReferenceElements< double, dimension >::general( entity.type() );
        RangeType val;
        // evaluate the phase field at the barycenter (note
        // refElement.position(0,0) is the barycenter in local coordinates)
        double markVal;
        JacobianRangeType grad;
        uh_.localFunction( entity ).jacobian( refElement.position(0,0),grad );
        markVal = grad[1].two_norm();

        if( markVal > tolerance )
        {
          // make sure grid is not overly refined...
          if ( entity.level() < maxLevel_ )
          {
            // mark entity for refinement
            grid_.mark( 1, entity );
            // grid was marked
            marked = 1;
           }
         }
         else
         {
           // mark for coarsening
           grid_.mark( -1, entity );
         }
      }
    // get global max
    marked = grid_.comm().max( marked );
    return bool( marked );
  }



  void shuffle(int *array, size_t n)
  {
      if (n > 1) //If no. of training examples > 1
      {
          size_t i;
          for (i = 0; i < n - 1; i++)
          {
              size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
              int t = array[j];
              array[j] = array[i];
              array[i] = t;
          }
      }
  }

  void predict(double test_sample[])
  {
      for (int j=0; j<numHiddenNodes; j++)
      {
          double activation=hiddenLayerBias[j];
          for (int k=0; k<numInputs; k++)
          {
              activation+=test_sample[k]*hiddenWeights[k][j];
          }
          hiddenLayer[j] = tanh(activation);
      }

      for (int j=0; j<numOutputs; j++)
      {
          double activation=outputLayerBias[j];
          for (int k=0; k<numHiddenNodes; k++)
          {
              activation+=hiddenLayer[k]*outputWeights[k][j];
          }
          outputLayer[j] = lin(activation);
      }
      //std::cout<<outputLayer[0]<<"\n";
      //return outputLayer[0];
      //std::cout << "Input:" << training_inputs[i][0] << " " << training_inputs[i][1] << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[i][0] << "\n";
  }


  void launchdnn( vector<double>& epch, vector<double>&perf )
  {
      ///TRAINING DATA GENERATION
      // cout<< "Initial Data: ";
      for (int i = 0; i < numTrainingSets; i++)
      {
      double p = (1.0*(double)i/numTrainingSets);
      training_inputs[i][0] = (p);

      double tetha_in( 0 );
      double effsat;
      double Krw;
      //
      // inhomegeneity_tetha( entity, x, tetha_in );
      // effective_sat( entity, x, p, effsat );
      //
      if (1>p)
        Krw = (std::pow((1-p),(2.+3*2.0)/2.0));
      else
        Krw =0;

       auto testwmob = Krw/1.0e-3;


      // // training_outputs[i][0] = p*cos(p);
      // auto powS = pow(p,3.5);
      // auto pow1mS = pow(1.0-p,1.5);
      // auto kr = 1.0*powS/(powS+pow1mS*3);
      training_outputs[i][0] = Krw;
      /***************************Try Avoiding Edits In This part*******************************/
          ///FINDING NORMALIZING FACTOR
          for(int m=0; m<numInputs; ++m)
              if(MAXX < training_inputs[i][m])
                  MAXX = training_inputs[i][m];
          for(int m=0; m<numOutputs; ++m)
              if(MAXX < training_outputs[i][m])
                  MAXX = training_outputs[i][m];

    }
      ///NORMALIZING
      for (int i = 0; i < numTrainingSets; i++)
      {
            for(int m=0; m<numInputs; ++m)
                training_inputs[i][m] /= 1.0f*MAXX;

            for(int m=0; m<numOutputs; ++m)
                training_outputs[i][m] /= 1.0f*MAXX;
            //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
            //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
      }
      // cout<<endl;
      ///WEIGHT & BIAS INITIALIZATION
      for (int i=0; i<numInputs; i++) {
          for (int j=0; j<numHiddenNodes; j++) {
              hiddenWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numHiddenNodes; i++) {
          hiddenLayerBias[i] = init_weight();
          for (int j=0; j<numOutputs; j++) {
              outputWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numOutputs; i++) {
          //outputLayerBias[i] = init_weight();
          outputLayerBias[i] = 0;
      }

      ///FOR INDEX SHUFFLING
      int trainingSetOrder[numTrainingSets];
      for(int j=0; j<numTrainingSets; ++j)
          trainingSetOrder[j] = j;


      ///TRAINING
      //std::cout<<"start train\n";
      vector<double> performance, epo; ///STORE MSE, EPOCH
      for (int n=0; n < epoch; n++)
      {
          double MSE = 0;
          shuffle(trainingSetOrder,numTrainingSets);
          // std::cout<<"\nepoch :"<<n;
          for (int x=0; x<numTrainingSets; x++)
          {
              int i = trainingSetOrder[x];
              //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
              //int x=i;
              //std::cout<<"Training Set :"<<x<<"\n";
              /// Forward pass
              for (int j=0; j<numHiddenNodes; j++)
              {
                  double activation=hiddenLayerBias[j];
                  //std::cout<<"Training Set :"<<x<<"\n";
                   for (int k=0; k<numInputs; k++) {
                      activation+=training_inputs[i][k]*hiddenWeights[k][j];
                  }
                  hiddenLayer[j] = tanh(activation);
              }

              for (int j=0; j<numOutputs; j++) {
                  double activation=outputLayerBias[j];
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      activation+=hiddenLayer[k]*outputWeights[k][j];
                  }
                  outputLayer[j] = lin(activation);
              }

              //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
              for(int k=0; k<numOutputs; ++k)
                  MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);

             /// Backprop
             ///   For V
              double deltaOutput[numOutputs];
              for (int j=0; j<numOutputs; j++) {
                  double errorOutput = (training_outputs[i][j]-outputLayer[j]);
                  deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
              }

              ///   For W
              double deltaHidden[numHiddenNodes];
              for (int j=0; j<numHiddenNodes; j++) {
                  double errorHidden = 0.0f;
                  for(int k=0; k<numOutputs; k++) {
                      errorHidden+=deltaOutput[k]*outputWeights[j][k];
                  }
                  deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
              }

              ///Updation
              ///   For V and b
              for (int j=0; j<numOutputs; j++) {
                  //b
                  outputLayerBias[j] += deltaOutput[j]*lr;
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
                  }
              }

              ///   For W and c
              for (int j=0; j<numHiddenNodes; j++) {
                  //c
                  hiddenLayerBias[j] += deltaHidden[j]*lr;
                  //W
                  for(int k=0; k<numInputs; k++) {
                    hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
                  }
              }
          }
          //Averaging the MSE
          MSE /= 1.0f*numTrainingSets;
          //cout<< "  MSE: "<< MSE<<endl;
          ///Steps to PLOT PERFORMANCE PER EPOCH
          performance.push_back(MSE*100);
          epo.push_back(n);
      }

      epch =epo;
      perf = performance;
      // return epo;
      //
      // //  // Print weights
      //  // std::cout << "Final Hidden Weights\n[ ";
      //  for (int j=0; j<numHiddenNodes; j++) {
      //      // std::cout << "[ ";
      //      for(int k=0; k<numInputs; k++) {
      //          // std::cout << hiddenWeights[k][j] << " ";
      //      }
      //      // std::cout << "] ";
      //  }
      //  // std::cout << "]\n";
      //
      //  // std::cout << "Final Hidden Biases\n[ ";
      //  for (int j=0; j<numHiddenNodes; j++) {
      //      // std::cout << hiddenLayerBias[j] << " ";
      //  }
       // std::cout << "]\n";
       // std::cout << "Final Output Weights";
       // for (int j=0; j<numOutputs; j++) {
       //     // std::cout << "[ ";
       //     for (int k=0; k<numHiddenNodes; k++) {
       //         std::cout << outputWeights[k][j] << " ";
       //     }
       //     std::cout << "]\n";
       // }
       // std::cout << "Final Output Biases\n[ ";
       // for (int j=0; j<numOutputs; j++) {
       //     std::cout << outputLayerBias[j] << " ";
       // }
       // std::cout << "]\n";

        //  //Plot the results
        // vector<float> x;
        // vector<float> y1, y2;
        //  double test_input[1000][numInputs];
        //  int numTestSets = numTrainingSets;
        // for (float i = 0; i < numTestSets; i=i+0.25)
        // {
        //      double p = (1.0*(double)i/numTestSets);
        //  x.push_back(p);
        //  // // y1.push_back(p*cos(p));
        //  // auto powS = pow(p,3.5);
        //  // auto pow1mS = pow(1.0-p,1.5);
        //  // auto kr = 1.0*powS/(powS+pow1mS*3);
        //  double Kr;
        //
        //  if (1>p)
        //    Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
        //  else
        //    Kr =0;
        //
        //  // auto testwmob = Krw/1.0e-3;
        //  y1.push_back(Kr);
        //
        //  double test_input[1];
        //  test_input[0] = p/MAXX;
        //      predict(test_input);
        //  y2.push_back(outputLayer[0]*MAXX);
        //
        // }
        // std::cout <<"yyyyy Output "<< y2.size()<< " ";
        // //
        // FILE * gp = popen("gnuplot", "w");
        // fprintf(gp, "set terminal wxt size 600,400 \n");
        // fprintf(gp, "set grid \n");
        // fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
        // fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
        // fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
        // fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
        //
        // ///Exact f(x) = sin(x) -> Green Graph
        // for (int k = 0; k < x.size(); k++) {
        //   fprintf(gp, "%f %f \n", x[k], y1[k]);
        // }
        // fprintf(gp, "e\n");
        // //
        // // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
        // for (int k = 0; k < x.size(); k++) {
        //   fprintf(gp, "%f %f \n", x[k], y2[k]);
        //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
        //
        // }
        // fprintf(gp, "e\n");
        //
        // fflush(gp);
        //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
        //   FILE * gp1 = popen("gnuplot", "w");
        // fprintf(gp1, "set terminal wxt size 600,400 \n");
        // fprintf(gp1, "set grid \n");
        // fprintf(gp1, "set title '%s' \n", "Performance");
        // fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
        // fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
        // fprintf(gp1, "plot '-' w p ls 1 \n");
        //
        //   for (int k = 0; k < epo.size(); k++) {
        //   fprintf(gp1, "%f %f \n", epo[k], performance[k]);
        // }
        // fprintf(gp1, "e\n");
        //
        // fflush(gp1);
        // //
        // system("pause");
        // //_pclose(gp);
        // cin.get();
        //  resultdnn = y2;
        //  inputset = x;
        //    // return 0;
        //  // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
        //  // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
       }


       void dnnprediction(vector<double>&epo, vector<double>&performance,std::vector<float>& inputset, std::vector<float>& resultdnn )
       {

             vector<float> x;
             vector<float> y1, y2;
              double test_input[1000][numInputs];
              int numTestSets = numTrainingSets;
             for (float i = 0; i < numTestSets; i=i+0.25)
             {
                  double p = (1.0*(double)i/numTestSets);
              x.push_back(p);
              // // y1.push_back(p*cos(p));
              // auto powS = pow(p,3.5);
              // auto pow1mS = pow(1.0-p,1.5);
              // auto kr = 1.0*powS/(powS+pow1mS*3);
              double Kr;

              if (1>p)
                Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
              else
                Kr =0;

              // auto testwmob = Krw/1.0e-3;
              y1.push_back(Kr);

              double test_input[1];
              test_input[0] = p/MAXX;
                  predict(test_input);
              y2.push_back(outputLayer[0]*MAXX);

             }
             std::cout <<"yyyyy Output "<< y2.size()<< " ";
             //
             FILE * gp = popen("gnuplot", "w");
             fprintf(gp, "set terminal wxt size 600,400 \n");
             fprintf(gp, "set grid \n");
             fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
             fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
             fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
             fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");

             ///Exact f(x) = sin(x) -> Green Graph
             for (int k = 0; k < x.size(); k++) {
               fprintf(gp, "%f %f \n", x[k], y1[k]);
             }
             fprintf(gp, "e\n");
             //
             // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
             for (int k = 0; k < x.size(); k++) {
               fprintf(gp, "%f %f \n", x[k], y2[k]);
               std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";

             }
             fprintf(gp, "e\n");

             fflush(gp);
               ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
               FILE * gp1 = popen("gnuplot", "w");
             fprintf(gp1, "set terminal wxt size 600,400 \n");
             fprintf(gp1, "set grid \n");
             fprintf(gp1, "set title '%s' \n", "Performance");
             fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
             fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
             fprintf(gp1, "plot '-' w p ls 1 \n");

               for (int k = 0; k < epo.size(); k++) {
               fprintf(gp1, "%f %f \n", epo[k], performance[k]);
             }
             fprintf(gp1, "e\n");

             fflush(gp1);
             //
             system("pause");
             //_pclose(gp);
             cin.get();
              resultdnn = y2;
              inputset = x;
                // return 0;
              // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
              // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
            }
  // Nnrelperm

};

#endif // #ifndef NNRELPERM_HH
