#ifndef FLOW_FEMSCHEME_HH
#define FLOW_FEMSCHEME_HH

#include <string>
// lagrange interpolation
// #include <dune/fem/operator/lagrangeinterpolation.hh>
// local includes
#include "femscheme.hh"
#include "../estimator/estimator.hh"
#include "nnrelperm.hh"

// l2-projection
#include <dune/fem/operator/projection/l2projection.hh>
// Newton method
#include "../operator/newtoninvop.hh"


// Scheme
//----------

template < class ImplicitModel, class ExplicitModel >
struct PhaseFlowScheme : public FemScheme<ImplicitModel>
{
  typedef FemScheme<ImplicitModel> BaseType;
  typedef typename BaseType::GridType GridType;
  typedef typename BaseType::GridPartType GridPartType;
  typedef typename BaseType::ModelType ImplicitModelType;
  typedef ExplicitModel ExplicitModelType;
  typedef typename BaseType::FunctionSpaceType FunctionSpaceType;
  typedef typename BaseType::DiscreteFunctionType DiscreteFunctionType;
  typedef Dune::Fem::NewtonInverseOperator< typename BaseType::LinearOperatorType, typename BaseType::LinearInverseOperatorType > NonLinearInverseOperatorType;

  //! type of restriction/prolongation projection for adaptive simulations
  typedef Dune::Fem::RestrictProlongDefault< DiscreteFunctionType >  RestrictionProlongationType;
  //! type of adaptation manager handling adaptation and DoF compression
  typedef Dune::Fem::AdaptationManager< GridType, RestrictionProlongationType > AdaptationManagerType;
  //! Estimator
  typedef Estimator< DiscreteFunctionType,ImplicitModel, ExplicitModel > EstimatorType ;
  typedef Nnrelperm< DiscreteFunctionType,ImplicitModel, ExplicitModel > NnrelpermType ;

  //!limiter
  const bool with_limiter  = Dune::Fem::Parameter::getValue< bool >( "phaseflow.with_limiter", false );

  PhaseFlowScheme( GridPartType &gridPart,
                   const ImplicitModelType& implicitModel,
                   const ExplicitModelType& explicitModel )
    : BaseType(gridPart, implicitModel),
      explicitModel_(explicitModel),
      explicitOperator_( explicitModel_, solution_, discreteSpace_),
      invOp_( implicitOperator_ ),
      assemTimeVect_(0, 0),
      invsolveTimeVect_(0, 0),
      nbiterVect_(0, 0),
      avgnbNewtonItVect_(0, 0),
      estimator_(implicitModel, solution_ ),
      nnrelperm_(implicitModel, solution_ ),

      // restriction/prolongation operator
      restrictProlong_( solution_ ),
      // adaptation manager
      adaptationManager_( gridPart_.grid(), restrictProlong_ ),
      inputset(0),
      epo(0),
      perf(0),
      resultdnn(0)
  {
  }

  void prepare()
  {
// std::vector<float> inputset(0.0);
//  std::vector<float> resultdnn (0.0);
//  vector<double> perf(0), epo(0);
     estimator_.setUn(solution_);
    // nnrelperm_.launchdnn(epo, perf);

    explicitOperator_( solution_, operandExplOperator_ );
    // apply constraints, e.g. Dirichlet constraints, to the result
    explicitOperator_.prepare( solution_, operandExplOperator_ );
  }

  void initialize ()
  {
    Dune::Fem::DGL2ProjectionImpl::project( explicitModel_.initialFunction(), solution_ );
  }

  void solve ( bool assemble )
  {
    // nnrelperm_.dnnprediction(epo,perf, inputset, resultdnn);

    // // for(int k=0; k<120; k++) {
    // std::cout << "resultdnn10";
    //
    //   std::cout << resultdnn[10] << " ";
    //   std::cout << "]\n";
    // }
    invOp_( operandExplOperator_, solution_ );
  }

  void postpro ()
  {
    assemTimeVect_.push_back(invOp_.avgAssTime());
    invsolveTimeVect_.push_back(invOp_.avgInvSolveTime());
    nbiterVect_.push_back(invOp_.avgNbIt());
    avgnbNewtonItVect_.push_back(invOp_.iterations());
  }

  void finalize ()
  {
    auto avgAssemTime_ = std::accumulate(assemTimeVect_.cbegin(), assemTimeVect_.cend(), 0.0);
    avgAssemTime_ /= assemTimeVect_.size();

    auto avgInvsolveTime_ = std::accumulate(invsolveTimeVect_.cbegin(), invsolveTimeVect_.cend(), 0.0);
    avgInvsolveTime_ /= invsolveTimeVect_.size();

    auto avgNbIt_ = std::accumulate(nbiterVect_.cbegin(), nbiterVect_.cend(), 0.0);
    avgNbIt_ /= nbiterVect_.size();

    auto avgnbNewtonIt_ = std::accumulate(avgnbNewtonItVect_.cbegin(), avgnbNewtonItVect_.cend(), 0.0);
    avgnbNewtonIt_ /= avgnbNewtonItVect_.size();

    const std::string postprofile = Dune::Fem::Parameter::getValue< std::string >( "phaseflow.postpro" );
    const std::string filepathdir = Dune::Fem::Parameter::getValue< std::string >( "phaseflow.outputfilepath" );
    std::string filepath;
    filepath=filepathdir;

    system(("mkdir -p ./"+filepath).c_str());

    std::string fileName;
    fileName=postprofile;
    fileName += ".txt";
    std::ofstream postpro_summary;
    postpro_summary.open((filepath + "/" + fileName).c_str(), std::ofstream::out | std::ofstream::trunc);
    postpro_summary.close();
    postpro_summary.open ((filepath + "/" + fileName).c_str(), std::fstream::app);
    //
    postpro_summary <<"Average nb of Newton cycle: "<<avgnbNewtonIt_<<" iterations "<<"\n";
    postpro_summary <<"Average nb of iterations / Newton: "<<avgNbIt_<<" iterations "<<"\n";
    postpro_summary <<"Average inversion time / Newton: "<<avgInvsolveTime_<<" secs "<<"\n";
    postpro_summary <<"Average assembly time / Newton: "<<avgAssemTime_<<" secs "<<"\n";
    //
    postpro_summary.close();
  }

  //! mark elements for adaptation
  bool mark ( const double tolerance )
  {
    return estimator_.mark( tolerance );
  }

  //! do the adaptation for a given marking
  void adapt()
  {
    // apply adaptation and load balancing
    adaptationManager_.adapt();
  }

private:
  using BaseType::gridPart_;
  using BaseType::discreteSpace_;
  using BaseType::solution_;
  using BaseType::implicitModel_;
  using BaseType::operandExplOperator_;
  const ExplicitModelType &explicitModel_;

  typename BaseType::FlowOperatorType explicitOperator_; // the operator for the operandExplOperator
  using BaseType::implicitOperator_;                     // the operator for the lhs
  NonLinearInverseOperatorType invOp_;

  std::vector<double> assemTimeVect_;
  std::vector<double> invsolveTimeVect_;
  std::vector<double> nbiterVect_;
  std::vector<double> avgnbNewtonItVect_;
  std::vector<float> inputset;
   std::vector<float> resultdnn ;
  std::vector<double> perf;
  std::vector<double>  epo;
  EstimatorType  estimator_;
  NnrelpermType  nnrelperm_;

  RestrictionProlongationType restrictProlong_ ; // local restriction/prolongation object
  AdaptationManagerType  adaptationManager_ ;    // adaptation manager handling adaptation
};

#endif // end #if FLOW_FEMSCHEME_HH
