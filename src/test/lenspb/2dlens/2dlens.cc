#include <config.h>

// iostream includes
#include <iostream>

// include grid part
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
//#include <dune/alugrid/dgf.hh>
// include output
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/space/common/adaptmanager.hh>
#include <dune/fem/space/common/restrictprolongfunction.hh>

#include <dune/twophaseDG/algorithms/phaseflowscheme.hh>
#include <dune/twophaseDG/algorithms/algorithm.hh>
#include <examples/lenspb/lenspbinitialdata.hh>
#include <examples/lenspb/lenspbmodel.hh>
#include <examples/lenspb/lenspbbndmodel.hh>
#include <examples/lenspb/lenspbphysicalparmodel.hh>


int main ( int argc, char **argv )
  try
    {
      // initialize MPI, if necessary
      Dune::Fem::MPIManager::initialize( argc, argv );

      // append overloaded parameters from the command line
      Dune::Fem::Parameter::append( argc, argv );

      // append possible given parameter files
      for( int i = 1; i < argc; ++i )
        Dune::Fem::Parameter::append( argv[ i ] );

      // append default parameter file
      Dune::Fem::Parameter::append( "../data/parameter" );

      // type of hierarchical grid
      typedef Dune::GridSelector::GridType  HGridType ;
      typedef Dune::Fem::FunctionSpace< double, double, HGridType::dimensionworld, 2 > FunctionSpaceType;

      typedef InitialData< FunctionSpaceType > InitialDataType;

      typedef Dune::Fem::AdaptiveLeafGridPart< HGridType, Dune::InteriorBorder_Partition > GridPartType;
      typedef BoundaryModel<FunctionSpaceType,GridPartType> BndModelType;
      typedef PhysicalParModel<FunctionSpaceType,GridPartType> PhysicalParModelType;

      typedef LenspbModel< FunctionSpaceType, GridPartType, BndModelType, PhysicalParModelType > ModelType;

      typedef Algorithm< HGridType, InitialDataType, ModelType > AlgorithmType;

      // create grid from DGF file
      const std::string gridkey = Dune::Fem::IOInterface::defaultGridKey( HGridType::dimension );
      const std::string gridfile = Dune::Fem::Parameter::getValue< std::string >( gridkey );

      // the method rank and size from MPIManager are static
      if( Dune::Fem::MPIManager::rank() == 0 )
        std::cout << "Loading macro grid: " << gridfile << std::endl;

      // construct macro using the DGF Parser
      Dune::GridPtr< HGridType > gridPtr( gridfile );
      HGridType& grid = *gridPtr ;

      const int level = Dune::Fem::Parameter::getValue< int >( "phaseflow.level" );
      const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();
      Dune::Fem::GlobalRefine::apply( grid, level * refineStepsForHalf );
      AlgorithmType myalgorithm (grid);
      // do initial load balance
      grid.loadBalance();
      // Compute algorithm
      Dune::Timer computetimer;
      //Compute the algorithm
      myalgorithm.compute();
      const double compuTime = computetimer.elapsed();

      const std::string postprofile = Dune::Fem::Parameter::getValue< std::string >( "phaseflow.postpro" );
      std::string fileName;
      const std::string filepathdir = Dune::Fem::Parameter::getValue< std::string >( "phaseflow.outputfilepath" );
      std::string filepath;
      filepath=filepathdir;

      system(("mkdir -p ./"+filepath).c_str());

      fileName=postprofile;
      fileName += ".txt";
      std::ofstream postpro_summary;
      postpro_summary.open ((filepath + "/" + fileName).c_str(), std::ios_base::app);
      postpro_summary <<"Total cpu time: "<<compuTime<<" secs "<<"\n";
      postpro_summary.close();

      return 0;
    }
  catch( const Dune::Exception &exception )
    {
      std::cerr << "Error: " << exception << std::endl;
      return 1;
    }
