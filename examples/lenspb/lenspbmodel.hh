#ifndef LENSPB_MODEL_HH
#define LENSPB_MODEL_HH

#include <cassert>
#include <cmath>
#include <iostream>

#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>

#include <dune/twophaseDG/algorithms/temporalprobleminterface.hh>

#include <dune/twophaseDG/models/phaseflowmodel.hh>

using namespace Dune::Fem;

template< class FunctionSpace, class GridPart, class BndModel, class PhysParModel >
struct LenspbModel : protected PhaseFlowModel< FunctionSpace, GridPart, PhysParModel >
{
  typedef PhaseFlowModel< FunctionSpace, GridPart, PhysParModel > BaseType;

  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;
  typedef BndModel BndModelType;
  typedef PhysParModel PhysParModelType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef TemporalProblemInterface< FunctionSpaceType > ProblemType ;

  typedef typename BaseType::ProblemType InitialFunctionType;

  typedef Dune::Fem::TimeProviderBase TimeProviderType;

  typedef  std::vector< double > GravitationVectorType;

  enum { dimRange  = FunctionSpaceType :: dimRange };
  enum { dimDomain = FunctionSpaceType :: dimDomain };

  LenspbModel( const ProblemType& problem,
                  const GridPart &gridPart,
                  const bool implicit )
    : BaseType( problem, gridPart, implicit ),
      bndModel_( problem, gridPart ),
      physparModel_( problem, gridPart ),
      implicit_( implicit ),
      timeStepFactor_( 0 )
  {
    // get theta for theta scheme (only used for non-linear source)
    double theta = Dune::Fem::Parameter::getValue< double >("phaseflow.theta", 1.0 );

    if (implicit)
      timeStepFactor_ = theta ;
    else
      timeStepFactor_ = -( 1.0 - theta );
  }

  //Effective saturation s_e= 1-\frac{1-s_w-s_rw}{1-s_rw -s_rn}
  template< class Entity, class Point >
  void effective_sat ( const Entity &entity,
                       const Point &x,
                       const RangeType &value,
                       double &effsat ) const
  {
    return BaseType::effective_sat(entity, x, value, effsat);
  }

  //Capillary pressure function p_c=p_d (1-s_n)^{-\frac{1}{inhom_theta_}}
  template< class Entity, class Point >
  void cap_Press( const Entity &entity,
                  const Point &x,
                  const RangeType &value,
                  RangeType &cap_press ) const
  {
    return BaseType::cap_Press( entity, x, value, cap_press );
  }

  //First derivative of the cappilary pressure
  template< class Entity, class Point >
  void first_der_cap_Press( const Entity &entity,
                            const Point &x,
                            const RangeType &value,
                            RangeType &first_der_cap_press ) const
  {
    return BaseType::first_der_cap_Press( entity, x, value, first_der_cap_press );
  }

  //Second derivative of the cappilary pressure
  template< class Entity, class Point >
  void second_der_cap_Press( const Entity &entity,
                             const Point &x,
                             const RangeType &value,
                             RangeType &sec_der_cap_press ) const
  {
    return BaseType::second_der_cap_Press( entity, x, value, sec_der_cap_press );
  }

  //gradient of the cappilary pressure
  template< class Entity, class Point >
  void grad_cap_Press ( const Entity &entity,
                        const Point &x,
                        const RangeType &value,
                        const JacobianRangeType & gradient,
                        JacobianRangeType &grad_cap_press ) const
  {
    return BaseType::grad_cap_Press( entity, x, value, gradient, grad_cap_press );
  }

  //Non wetting mobility
  template< class Entity, class Point >
  void nonWet_mobility( const Entity &entity,
                        const Point &x,
                        const RangeType &value,
                        double &nwmob ) const
  {
    return BaseType::nonWet_mobility( entity, x, value, nwmob );
  }

  //Wetting mobility
  template< class Entity, class Point >
  void wet_mobility( const Entity &entity,
                     const Point &x,
                     const RangeType &value,
                     double &wmob ) const
  {
    return BaseType::wet_mobility( entity, x, value, wmob );
  }

  //Total mobility
  template< class Entity, class Point >
  void tot_mobility ( const Entity &entity,
                      const Point &x,
                      const RangeType &value,
                      double &totmob ) const
  {
    return BaseType::tot_mobility( entity, x, value, totmob );
  }

  template< class EntityInside, class EntityOutside, class PointInside, class PointOutside , class DomainTypenormal>
  void upwindingMobility( const EntityInside &entityIn,
                          const PointInside &xIn,
                          const EntityOutside &entityOut,
                          const PointOutside &xOut,
                          const DomainTypenormal normal,
                          const RangeType& u0En,
                          const RangeType& u0OutEn,
                          const JacobianRangeType& u0EnJac1,
                          const JacobianRangeType& u0OutJac1,
                          double &upwMob,
                          double &upwgradnonwetmob) const
  {

    return BaseType::upwindingMobility (entityIn, xIn, entityOut, xOut, normal, u0En, u0OutEn, u0EnJac1, u0OutJac1, upwMob, upwgradnonwetmob);
  }

  //grad of the Non wetting mobility
  template< class Entity, class Point >
  void grad_nonWet_mobility( const Entity &entity,
                             const Point &x,
                             const RangeType &value,
                             double &gradnonwetmob ) const
  {
    return BaseType::grad_nonWet_mobility ( entity, x, value, gradnonwetmob );
  }

  //gradient of the wet_mobility
  template< class Entity, class Point >
  void grad_wet_mobility( const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          double &gradwetmob ) const
  {
    return BaseType::grad_wet_mobility( entity, x, value, gradwetmob );
  }

  //gradient of the tot mob
  template< class Entity, class Point >
  void grad_tot_mobility( const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          double &gradtotmob ) const
  {
    return BaseType::grad_tot_mobility( entity, x, value, gradtotmob );
  }

  template< class Entity, class Point >
  void source( const Entity &entity,
              const Point &x,
              const RangeType &value,
              RangeType &flux ) const
  {
    return BaseType::source( entity, x, value, flux );
  }

  template< class Entity, class Point >
  void linSource ( const RangeType& valueUn,
                   const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   RangeType &flux ) const
  {
    return BaseType::source( entity, x, value, flux );
  }

  template< class Entity, class Point >
  void gravity_Flux( const Entity &entity,
                     const Point &x,
                     const RangeType &value,
                     const JacobianRangeType &gradient,
                     JacobianRangeType &flux,
                     const double lambda_n_upw=1,
                     const bool &useupw=false,
                     const bool &buildRhs=false ) const
  {
    return BaseType::gravity_Flux( entity, x, value, gradient, flux, lambda_n_upw, useupw, buildRhs );
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void wetting_Pressure_DiffusiveFlux( const Entity &entity,
                                       const Point &x,
                                       const RangeType &value,
                                       const JacobianRangeType &gradient,
                                       JacobianRangeType &flux,
                                       const double lambda_n_upw=1,
                                       const bool &useupw=false,
                                       const bool &buildRhs=false ) const
  {
    return BaseType::wetting_Pressure_DiffusiveFlux( entity, x, value, gradient, flux, lambda_n_upw, useupw, buildRhs );
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void capillary_Pressure_DiffusiveFlux( const Entity &entity,
                                         const Point &x,
                                         const RangeType &value,
                                         const JacobianRangeType &gradient,
                                         JacobianRangeType &flux,
                                         const double lambda_n_upw=1,
                                         const bool &useupw=false,
                                         const bool &buildRhs=false ) const
  {
    return BaseType::capillary_Pressure_DiffusiveFlux( entity, x, value, gradient, flux, lambda_n_upw, useupw, buildRhs );
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void linGravity_Flux( const RangeType &valueUn,
                        const JacobianRangeType &gradientUn,
                        const Entity &entity,
                        const Point &x,
                        const RangeType &value,
                        const JacobianRangeType &gradient,
                        JacobianRangeType &flux,
                        const double lambda_n_upw = 1,
                        const double gradnonwetmob_upw = 1,
                        const bool useupw=false ) const
  {
    return BaseType::linGravity_Flux( valueUn, gradientUn, entity, x, value, gradient, flux, lambda_n_upw, gradnonwetmob_upw, useupw );
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void linWetting_Pressure_DiffusiveFlux( const RangeType &valueUn,
                                          const JacobianRangeType &gradientUn,
                                          const Entity &entity,
                                          const Point &x,
                                          const RangeType &value,
                                          const JacobianRangeType &gradient,
                                          JacobianRangeType &flux,
                                          const double lambda_n_upw = 1,
                                          const double gradnonwetmob_upw = 1,
                                          const bool useupw=false ) const
  {
    return BaseType::linWetting_Pressure_DiffusiveFlux( valueUn, gradientUn, entity, x, value, gradient, flux, lambda_n_upw, gradnonwetmob_upw, useupw );
  }

  //! return the diffusive flux
  template< class Entity, class Point >
  void linCapillary_Pressure_DiffusiveFlux( const RangeType &valueUn,
                                            const JacobianRangeType &gradientUn,
                                            const Entity &entity,
                                            const Point &x,
                                            const RangeType &value,
                                            const JacobianRangeType &gradient,
                                            JacobianRangeType &flux ) const
  {
    return BaseType::linCapillary_Pressure_DiffusiveFlux( valueUn, gradientUn, entity, x, value, gradient, flux );
  }

  bool hasDirichletBoundary () const
  {
    return BndModelType::hasDirichletBoundary() ;
  }

  //! return true if given intersection belongs to the Dirichlet boundary -
  //! we test here if the center is a dirichlet point
  template <class Intersection>
  bool isDirichletIntersection( const Intersection& inter ) const
  {
    return isDirichletPoint( inter, inter.geometry().center() );
  }

  //! return true if given point belongs to the Dirichlet boundary (default is false)
  template <class Intersection>
  bool isDirichletPoint( const Intersection& inter, const DomainType& x ) const
  {
    return bndModel_.isDirichletPoint(inter, x ) ;
  }

  bool hasNeumannBoundary () const
  {
    return BndModelType::hasNeumannBoundary() ;
  }

  //! return true if given intersection belongs to the Neumann boundary -
  //! we test here if the center is a dirichlet point
  template <class Intersection>
  bool isNeumannIntersection( const Intersection& inter ) const
  {
    return isNeumannPoint( inter.geometry().center() );
  }

  //! return true if given point belongs to the Neumann boundary (default is false)
  bool isNeumannPoint( const DomainType& x ) const
  {
    return bndModel_.isNeumannPoint( x );
  }

  //! return true if RHS
  bool buildExplicitOp() const
  {
    return !implicit_ ;
  }

  template< class Entity, class Point >
  void permeability_K( const Entity &entity,
                       const Point &x,
                       double & K_perm ) const
  {
    return BaseType::permeability_K(entity,x,K_perm);
  }

  template< class Entity, class Point >
  void porosity_PHI( const Entity &entity,
                     const Point &x,
                     double & phi_poro ) const
  {
    return BaseType::porosity_PHI( entity, x, phi_poro );
  }

  template< class Entity, class Point, class Entityneigh, class Pointneigh >
  void penality_pressure( const Entity &entity,
                          const Point &x1,
                          const Entityneigh &entityneigh,
                          const Pointneigh &x2,
                          double & betapress ) const
  {
    return BaseType::penality_pressure( entity, x1, entityneigh, x2, betapress );
  }

  template< class Entity, class Point,class Entityneigh, class Pointneigh >
  void penality_saturation( const Entity &entity,
                            const Point &x1,
                            const Entityneigh &entityneigh,
                            const Pointneigh &x2,
                            double & betasat ) const
  {
    return BaseType::penality_saturation( entity, x1, entityneigh, x2, betasat );
  }

  template< class Entity, class Point >
  void entry_Pressure( const Entity &entity,
                       const Point &x,
                       double & entry_press ) const
  {
    return BaseType::entry_Pressure( entity, x, entry_press );
  }

  template< class Entity, class Point >
  void inhomegeneity_tetha( const Entity &entity,
                            const Point &x,
                            double & tet_inhom ) const
  {
    return BaseType::inhomegeneity_tetha( entity, x, tet_inhom );
  }

  template< class Entity, class Point >
  void irreduct_water_satu( const Entity &entity,
                            const Point &x,
                            double & swr ) const
  {
    return BaseType::irreduct_water_satu( entity, x, swr );
  }

  template< class Entity, class Point >
  void g( const RangeType& uBar,
          const Entity &entity,
          const Point &x,
          RangeType &u ) const
  {
    bndModel_.g( uBar, entity, x, u);
  }

  template< class Entity, class Point >
  void gN( const RangeType& uBar,
           const Entity &entity,
           const Point &x,
           RangeType &u ) const
  {
    bndModel_.gN( uBar, entity, x, u);
  }

  // return Fem::Function for Dirichlet boundary values
  typename BndModelType::DirichletBoundaryType dirichletBoundary() const
  {
    return bndModel_.dirichletBoundary();
  }

  // return Fem::Function for Neumann boundary values
  typename BndModelType::NeumannBoundaryType NeumannBoundary() const
  {
    return bndModel_.NeumannBoundaryType();
  }

  //! return reference to Problem's time provider
  const TimeProviderType & timeProvider() const
  {
    return  BaseType::timeProvider();
  }

  const InitialFunctionType &initialFunction() const
  {
    return problem_;
  }

protected:
  using BaseType::problem_;
  using BaseType::timeProvider_;
  bool implicit_;
  double timeStepFactor_;
  BndModelType bndModel_;
  PhysParModelType physparModel_;
};
#endif // #ifndef LENSPB_MODEL_HH
