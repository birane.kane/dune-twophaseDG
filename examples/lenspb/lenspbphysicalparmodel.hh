#ifndef PHYSICAL_PAR_MODEL_HH
#define PHYSICAL_PAR_MODEL_HH

#include <cassert>
#include <cmath>

#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/twophaseDG/algorithms/probleminterface.hh>
//Had a lot of trouble with shuffle
//Added linear activation beside tanh
#include <iostream>
#include<vector>
#include <list>
#include <cstdlib>
// #include <math.h>

#define PI 3.141592653589793238463
#define N

// #define epsilon 0.05
#define epoch 500

using namespace std;
extern "C" FILE *popen(const char *command, const char *mode);

//double sigmoid(double x) { return 1.0f / (1.0f + exp(-x)); }
//double dsigmoid(double x) { return x * (1.0f - x); }
double tanh(double x) { return (exp(x)-exp(-x))/(exp(x)+exp(-x)) ;}
double dtanh(double x) {return 1.0f - x*x ;}

double lin(double x) { return x;}
double dlin(double x) { return 1.0f;}

double init_weight() { return (2.*rand()/RAND_MAX -1); }
double MAXX = -9999999999999999; //maximum value of input example
//double init_weight() { return ((double)rand(solvenn))/((double)RAND_MAX); }

static const int numInputs = 1;
static const int numHiddenNodes = 5;
static const int numOutputs = 1;

const double lr = 0.05f;

double hiddenLayer[numHiddenNodes];
double outputLayer[numOutputs];

double hiddenLayerBias[numHiddenNodes];
double outputLayerBias[numOutputs];

double hiddenWeights[numInputs][numHiddenNodes];
double outputWeights[numHiddenNodes][numOutputs];

static const int numTrainingSets = 30;
double training_inputs[numTrainingSets][numInputs];
double training_outputs[numTrainingSets][numOutputs];

void shuffle(int *array, size_t n)
{
    if (n > 1) //If no. of training examples > 1
    {
        size_t i;
        for (i = 0; i < n - 1; i++)
        {
            size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
            int t = array[j];
            array[j] = array[i];
            array[i] = t;
        }
    }
}

void predict(double test_sample[])
{
    for (int j=0; j<numHiddenNodes; j++)
    {
        double activation=hiddenLayerBias[j];
        for (int k=0; k<numInputs; k++)
        {
            activation+=test_sample[k]*hiddenWeights[k][j];
        }
        hiddenLayer[j] = tanh(activation);
    }

    for (int j=0; j<numOutputs; j++)
    {
        double activation=outputLayerBias[j];
        for (int k=0; k<numHiddenNodes; k++)
        {
            activation+=hiddenLayer[k]*outputWeights[k][j];
        }
        outputLayer[j] = lin(activation);
    }
    //std::cout<<outputLayer[0]<<"\n";
    //return outputLayer[0];
    //std::cout << "Input:" << training_inputs[i][0] << " " << training_inputs[i][1] << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[i][0] << "\n";
}


// #define PI 3.141592653589793238463
// #define N
//
// #define epsilon 0.05
// #define epoch 500
//
// using namespace std;
// extern "C" FILE *popen(const char *command, const char *mode);
//
// //double sigmoid(double x) { return 1.0f / (1.0f + exp(-x)); }
// //double dsigmoid(double x) { return x * (1.0f - x); }
// double tanh(double x) { return (exp(x)-exp(-x))/(exp(x)+exp(-x)) ;}
// double dtanh(double x) {return 1.0f - x*x ;}
//
// double lin(double x) { return x;}
// double dlin(double x) { return 1.0f;}
//
// double init_weight() { return (2.*rand()/RAND_MAX -1); }
// double MAXX = -9999999999999999; //maximum value of input example
// //double init_weight() { return ((double)rand(solvenn))/((double)RAND_MAX); }
//
// static const int numInputs = 1;
// static const int numHiddenNodes = 5;
// static const int numOutputs = 1;
//
// const double lr = 0.05f;
//
// double hiddenLayer[numHiddenNodes];
// double outputLayer[numOutputs];
//
// double hiddenLayerBias[numHiddenNodes];
// double outputLayerBias[numOutputs];
//
// double hiddenWeights[numInputs][numHiddenNodes];
// double outputWeights[numHiddenNodes][numOutputs];
//
// static const int numTrainingSets = 30;
// double training_inputs[numTrainingSets][numInputs];
// double training_outputs[numTrainingSets][numOutputs];
//
// void shuffle(int *array, size_t n)
// {
//     if (n > 1) //If no. of training examples > 1
//     {
//         size_t i;
//         for (i = 0; i < n - 1; i++)
//         {
//             size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
//             int t = array[j];
//             array[j] = array[i];
//             array[i] = t;
//         }
//     }
// }
//
// void predict(double test_sample[])
// {
//     for (int j=0; j<numHiddenNodes; j++)
//     {
//         double activation=hiddenLayerBias[j];
//         for (int k=0; k<numInputs; k++)
//         {
//             activation+=test_sample[k]*hiddenWeights[k][j];
//         }
//         hiddenLayer[j] = tanh(activation);
//     }
//
//     for (int j=0; j<numOutputs; j++)
//     {
//         double activation=outputLayerBias[j];
//         for (int k=0; k<numHiddenNodes; k++)
//         {
//             activation+=hiddenLayer[k]*outputWeights[k][j];
//         }
//         outputLayer[j] = lin(activation);
//     }
//     //std::cout<<outputLayer[0]<<"\n";
//     //return outputLayer[0];
//     //std::cout << "Input:" << training_inputs[i][0] << " " << training_inputs[i][1] << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[i][0] << "\n";
// }
//
//
// void prepdnn()
// {
//     ///TRAINING DATA GENERATION
//     // cout<< "Initial Data: ";
//     for (int i = 0; i < numTrainingSets; i++)
//     {
// 		double p = (1.0*(double)i/numTrainingSets);
// 		training_inputs[i][0] = (p);
// 		// training_outputs[i][0] = p*cos(p);
//     auto powS = pow(p,3.5);
//     auto pow1mS = pow(1.0-p,1.5);
//     auto kr = 1.0*powS/(powS+pow1mS*3);
//     training_outputs[i][0] = kr;
// 		/***************************Try Avoiding Edits In This part*******************************/
//         ///FINDING NORMALIZING FACTOR
//         for(int m=0; m<numInputs; ++m)
//             if(MAXX < training_inputs[i][m])
//                 MAXX = training_inputs[i][m];
//         for(int m=0; m<numOutputs; ++m)
//             if(MAXX < training_outputs[i][m])
//                 MAXX = training_outputs[i][m];
//
// 	}
// 	///NORMALIZING
// 	for (int i = 0; i < numTrainingSets; i++)
// 	{
//         for(int m=0; m<numInputs; ++m)
//             training_inputs[i][m] /= 1.0f*MAXX;
//
//         for(int m=0; m<numOutputs; ++m)
//             training_outputs[i][m] /= 1.0f*MAXX;
//         //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
//         //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
// 	}
// 	// cout<<endl;
//     ///WEIGHT & BIAS INITIALIZATION
//     for (int i=0; i<numInputs; i++) {
//         for (int j=0; j<numHiddenNodes; j++) {
//             hiddenWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numHiddenNodes; i++) {
//         hiddenLayerBias[i] = init_weight();
//         for (int j=0; j<numOutputs; j++) {
//             outputWeights[i][j] = init_weight();
//         }
//     }
//     for (int i=0; i<numOutputs; i++) {
//         //outputLayerBias[i] = init_weight();
//         outputLayerBias[i] = 0;
//     }
//
//     ///FOR INDEX SHUFFLING
//     int trainingSetOrder[numTrainingSets];
//     for(int j=0; j<numTrainingSets; ++j)
//         trainingSetOrder[j] = j;
//
//
//     ///TRAINING
//     //std::cout<<"start train\n";
//     vector<double> performance, epo; ///STORE MSE, EPOCH
//     for (int n=0; n < epoch; n++)
//     {
//         double MSE = 0;
//         shuffle(trainingSetOrder,numTrainingSets);
//         // std::cout<<"\nepoch :"<<n;
//         for (int x=0; x<numTrainingSets; x++)
//         {
//             int i = trainingSetOrder[x];
//             //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
//             //int x=i;
//             //std::cout<<"Training Set :"<<x<<"\n";
//             /// Forward pass
//             for (int j=0; j<numHiddenNodes; j++)
//             {
//                 double activation=hiddenLayerBias[j];
//                 //std::cout<<"Training Set :"<<x<<"\n";
//                  for (int k=0; k<numInputs; k++) {
//                     activation+=training_inputs[i][k]*hiddenWeights[k][j];
//                 }
//                 hiddenLayer[j] = tanh(activation);
//             }
//
//             for (int j=0; j<numOutputs; j++) {
//                 double activation=outputLayerBias[j];
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     activation+=hiddenLayer[k]*outputWeights[k][j];
//                 }
//                 outputLayer[j] = lin(activation);
//             }
//
//             //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
//             for(int k=0; k<numOutputs; ++k)
//                 MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);
//
//            /// Backprop
//            ///   For V
//             double deltaOutput[numOutputs];
//             for (int j=0; j<numOutputs; j++) {
//                 double errorOutput = (training_outputs[i][j]-outputLayer[j]);
//                 deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
//             }
//
//             ///   For W
//             double deltaHidden[numHiddenNodes];
//             for (int j=0; j<numHiddenNodes; j++) {
//                 double errorHidden = 0.0f;
//                 for(int k=0; k<numOutputs; k++) {
//                     errorHidden+=deltaOutput[k]*outputWeights[j][k];
//                 }
//                 deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
//             }
//
//             ///Updation
//             ///   For V and b
//             for (int j=0; j<numOutputs; j++) {
//                 //b
//                 outputLayerBias[j] += deltaOutput[j]*lr;
//                 for (int k=0; k<numHiddenNodes; k++)
//                 {
//                     outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
//                 }
//             }
//
//             ///   For W and c
//             for (int j=0; j<numHiddenNodes; j++) {
//                 //c
//                 hiddenLayerBias[j] += deltaHidden[j]*lr;
//                 //W
//                 for(int k=0; k<numInputs; k++) {
//                   hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
//                 }
//             }
//         }
//         //Averaging the MSE
//         MSE /= 1.0f*numTrainingSets;
//         //cout<< "  MSE: "<< MSE<<endl;
//         ///Steps to PLOT PERFORMANCE PER EPOCH
//         performance.push_back(MSE*100);
//         epo.push_back(n);
//     }
//
//     // // Print weights
//     // std::cout << "Final Hidden Weights\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << "[ ";
//     //     for(int k=0; k<numInputs; k++) {
//     //         std::cout << hiddenWeights[k][j] << " ";
//     //     }
//     //     std::cout << "] ";
//     // }
//     // std::cout << "]\n";
//     //
//     // std::cout << "Final Hidden Biases\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << hiddenLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//     // std::cout << "Final Output Weights";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << "[ ";
//     //     for (int k=0; k<numHiddenNodes; k++) {
//     //         std::cout << outputWeights[k][j] << " ";
//     //     }
//     //     std::cout << "]\n";
//     // }
//     // std::cout << "Final Output Biases\n[ ";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << outputLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//   //
//   //   //Plot the results
// 	// vector<float> x;
// 	// vector<float> y1, y2;
//   //   //double test_input[1000][numInputs];
//   //   int numTestSets = numTrainingSets;
// 	// for (float i = 0; i < numTestSets; i=i+0.25)
// 	// {
//   //       double p = (1.0*(double)i/numTestSets);
// 	// 	x.push_back(p);
// 	// 	// y1.push_back(p*cos(p));
//   //   auto powS = pow(p,3.5);
//   //   auto pow1mS = pow(1.0-p,1.5);
//   //   auto kr = 1.0*powS/(powS+pow1mS*3);
//   //   y1.push_back(kr);
//   //
// 	// 	double test_input[1];
// 	// 	test_input[0] = p/MAXX;
//   //       predict(test_input);
// 	// 	y2.push_back(outputLayer[0]*MAXX);
// 	// }
//   //
// 	// FILE * gp = popen("gnuplot", "w");
// 	// fprintf(gp, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp, "set grid \n");
// 	// fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
// 	// fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
//   //
// 	// ///Exact f(x) = sin(x) -> Green Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y1[k]);
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y2[k]);
//   //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
//   //
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// fflush(gp);
//   //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
//   //   FILE * gp1 = popen("gnuplot", "w");
// 	// fprintf(gp1, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp1, "set grid \n");
// 	// fprintf(gp1, "set title '%s' \n", "Performance");
// 	// fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp1, "plot '-' w p ls 1 \n");
//   //
//   //   for (int k = 0; k < epo.size(); k++) {
// 	// 	fprintf(gp1, "%f %f \n", epo[k], performance[k]);
// 	// }
// 	// fprintf(gp1, "e\n");
//   //
// 	// fflush(gp1);
//   //
// 	// system("pause");
// 	// //_pclose(gp);
//   // cin.get();
//
//     // return 0;
// }
//
//
// void launchdnn()
// {
//     ///TRAINING DATA GENERATION
//   //   // cout<< "Initial Data: ";
//   //   for (int i = 0; i < numTrainingSets; i++)
//   //   {
// 	// 	double p = (1.0*(double)i/numTrainingSets);
// 	// 	training_inputs[i][0] = (p);
// 	// 	// training_outputs[i][0] = p*cos(p);
//   //   auto powS = pow(p,3.5);
//   //   auto pow1mS = pow(1.0-p,1.5);
//   //   auto kr = 1.0*powS/(powS+pow1mS*3);
//   //   training_outputs[i][0] = kr;
// 	// 	/***************************Try Avoiding Edits In This part*******************************/
//   //       ///FINDING NORMALIZING FACTOR
//   //       for(int m=0; m<numInputs; ++m)
//   //           if(MAXX < training_inputs[i][m])
//   //               MAXX = training_inputs[i][m];
//   //       for(int m=0; m<numOutputs; ++m)
//   //           if(MAXX < training_outputs[i][m])
//   //               MAXX = training_outputs[i][m];
//   //
// 	// }
// 	// ///NORMALIZING
// 	// for (int i = 0; i < numTrainingSets; i++)
// 	// {
//   //       for(int m=0; m<numInputs; ++m)
//   //           training_inputs[i][m] /= 1.0f*MAXX;
//   //
//   //       for(int m=0; m<numOutputs; ++m)
//   //           training_outputs[i][m] /= 1.0f*MAXX;
//   //       //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
//   //       //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
// 	// }
// 	// // cout<<endl;
//   //   ///WEIGHT & BIAS INITIALIZATION
//   //   for (int i=0; i<numInputs; i++) {
//   //       for (int j=0; j<numHiddenNodes; j++) {
//   //           hiddenWeights[i][j] = init_weight();
//   //       }
//   //   }
//   //   for (int i=0; i<numHiddenNodes; i++) {
//   //       hiddenLayerBias[i] = init_weight();
//   //       for (int j=0; j<numOutputs; j++) {
//   //           outputWeights[i][j] = init_weight();
//   //       }
//   //   }
//   //   for (int i=0; i<numOutputs; i++) {
//   //       //outputLayerBias[i] = init_weight();
//   //       outputLayerBias[i] = 0;
//   //   }
//   //
//   //   ///FOR INDEX SHUFFLING
//   //   int trainingSetOrder[numTrainingSets];
//   //   for(int j=0; j<numTrainingSets; ++j)
//   //       trainingSetOrder[j] = j;
//   //
//   //
//   //   ///TRAINING
//   //   //std::cout<<"start train\n";
//   //   vector<double> performance, epo; ///STORE MSE, EPOCH
//   //   for (int n=0; n < epoch; n++)
//   //   {
//   //       double MSE = 0;
//   //       shuffle(trainingSetOrder,numTrainingSets);
//   //       // std::cout<<"\nepoch :"<<n;
//   //       for (int x=0; x<numTrainingSets; x++)
//   //       {
//   //           int i = trainingSetOrder[x];
//   //           //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
//   //           //int x=i;
//   //           //std::cout<<"Training Set :"<<x<<"\n";
//   //           /// Forward pass
//   //           for (int j=0; j<numHiddenNodes; j++)
//   //           {
//   //               double activation=hiddenLayerBias[j];
//   //               //std::cout<<"Training Set :"<<x<<"\n";
//   //                for (int k=0; k<numInputs; k++) {
//   //                   activation+=training_inputs[i][k]*hiddenWeights[k][j];
//   //               }
//   //               hiddenLayer[j] = tanh(activation);
//   //           }
//   //
//   //           for (int j=0; j<numOutputs; j++) {
//   //               double activation=outputLayerBias[j];
//   //               for (int k=0; k<numHiddenNodes; k++)
//   //               {
//   //                   activation+=hiddenLayer[k]*outputWeights[k][j];
//   //               }
//   //               outputLayer[j] = lin(activation);
//   //           }
//   //
//   //           //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
//   //           for(int k=0; k<numOutputs; ++k)
//   //               MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);
//   //
//   //          /// Backprop
//   //          ///   For V
//   //           double deltaOutput[numOutputs];
//   //           for (int j=0; j<numOutputs; j++) {
//   //               double errorOutput = (training_outputs[i][j]-outputLayer[j]);
//   //               deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
//   //           }
//   //

//   //           ///   For W
//   //           double deltaHidden[numHiddenNodes];
//   //           for (int j=0; j<numHiddenNodes; j++) {
//   //               double errorHidden = 0.0f;
//   //               for(int k=0; k<numOutputs; k++) {
//   //                   errorHidden+=deltaOutput[k]*outputWeights[j][k];
//   //               }
//   //               deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
//   //           }
//   //
//   //           ///Updation
//   //           ///   For V and b
//   //           for (int j=0; j<numOutputs; j++) {
//   //               //b
//   //               outputLayerBias[j] += deltaOutput[j]*lr;
//   //               for (int k=0; k<numHiddenNodes; k++)
//   //               {
//   //                   outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
//   //               }
//   //           }
//   //
//   //           ///   For W and c
//   //           for (int j=0; j<numHiddenNodes; j++) {
//   //               //c
//   //               hiddenLayerBias[j] += deltaHidden[j]*lr;
//   //               //W
//   //               for(int k=0; k<numInputs; k++) {
//   //                 hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
//   //               }
//   //           }
//   //       }
//   //       //Averaging the MSE
//   //       MSE /= 1.0f*numTrainingSets;
//   //       //cout<< "  MSE: "<< MSE<<endl;
//   //       ///Steps to PLOT PERFORMANCE PER EPOCH
//   //       performance.push_back(MSE*100);
//   //       epo.push_back(n);
//   //   }
//
//     // // Print weights
//     // std::cout << "Final Hidden Weights\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << "[ ";
//     //     for(int k=0; k<numInputs; k++) {
//     //         std::cout << hiddenWeights[k][j] << " ";
//     //     }
//     //     std::cout << "] ";
//     // }
//     // std::cout << "]\n";
//     //
//     // std::cout << "Final Hidden Biases\n[ ";
//     // for (int j=0; j<numHiddenNodes; j++) {
//     //     std::cout << hiddenLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//     // std::cout << "Final Output Weights";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << "[ ";
//     //     for (int k=0; k<numHiddenNodes; k++) {
//     //         std::cout << outputWeights[k][j] << " ";
//     //     }
//     //     std::cout << "]\n";
//     // }
//     // std::cout << "Final Output Biases\n[ ";
//     // for (int j=0; j<numOutputs; j++) {
//     //     std::cout << outputLayerBias[j] << " ";
//     // }
//     // std::cout << "]\n";
//
//     //Plot the results
// 	vector<float> x;
// 	vector<float> y1, y2;
//     //double test_input[1000][numInputs];
//     int numTestSets = numTrainingSets;
// 	for (float i = 0; i < numTestSets; i=i+0.25)
// 	{
//         double p = (1.0*(double)i/numTestSets);
// 		x.push_back(p);
// 		// y1.push_back(p*cos(p));
//     auto powS = pow(p,3.5);
//     auto pow1mS = pow(1.0-p,1.5);
//     auto kr = 1.0*powS/(powS+pow1mS*3);
//     y1.push_back(kr);
//
// 		double test_input[1];
// 		test_input[0] = p/MAXX;
//         predict(test_input);
// 		y2.push_back(outputLayer[0]*MAXX);
// 	}
//   //
// 	// FILE * gp = popen("gnuplot", "w");
// 	// fprintf(gp, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp, "set grid \n");
// 	// fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
// 	// fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
//   //
// 	// ///Exact f(x) = sin(x) -> Green Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y1[k]);
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
// 	// for (int k = 0; k < x.size(); k++) {
// 	// 	fprintf(gp, "%f %f \n", x[k], y2[k]);
//   //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
//   //
// 	// }
// 	// fprintf(gp, "e\n");
//   //
// 	// fflush(gp);
//   //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
//   //   FILE * gp1 = popen("gnuplot", "w");
// 	// fprintf(gp1, "set terminal wxt size 600,400 \n");
// 	// fprintf(gp1, "set grid \n");
// 	// fprintf(gp1, "set title '%s' \n", "Performance");
// 	// fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
// 	// fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
// 	// fprintf(gp1, "plot '-' w p ls 1 \n");
//   //
//   //   for (int k = 0; k < epo.size(); k++) {
// 	// 	fprintf(gp1, "%f %f \n", epo[k], performance[k]);
// 	// }
// 	// fprintf(gp1, "e\n");
//   //
// 	// fflush(gp1);
//   //
// 	// system("pause");
// 	// //_pclose(gp);
//   // cin.get();
//
//     // return 0;
// }

template< class FunctionSpace, class GridPart >
struct PhysicalParModel
{
  typedef FunctionSpace FunctionSpaceType;
  typedef GridPart GridPartType;

  typedef typename FunctionSpaceType::DomainType DomainType;
  typedef typename FunctionSpaceType::RangeType RangeType;
  typedef typename FunctionSpaceType::JacobianRangeType JacobianRangeType;

  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  typedef ProblemInterface< FunctionSpaceType > ProblemType ;
  typedef  std::vector< double > GravitationVectorType;

  enum { dimRange  = FunctionSpaceType :: dimRange };
  enum { dimDomain = FunctionSpaceType :: dimDomain };

public:

  //! constructor taking problem reference
  PhysicalParModel( const ProblemType& problem, const GridPart &gridPart )
    : problem_( problem ),
      gridPart_(gridPart),
      gravity_( dimDomain, 0 ),
      eps_(Dune::Fem::Parameter::getValue< double >( "phaseflow.eps", 0.001 ) ),
      srn_(Dune::Fem::Parameter::getValue< double >( "phaseflow.residualnonwetting", 0 ) ),
      srw_(Dune::Fem::Parameter::getValue< double >( "phaseflow.residualwetting", 0.15 ) ),
      srwlens_(Dune::Fem::Parameter::getValue< double >( "phaseflow.lensresidualwetting", 0.15 ) ),
      visc_n_(Dune::Fem::Parameter::getValue< double >( "phaseflow.vicositynonwetting", 0.0005 ) ),
      visc_w_(Dune::Fem::Parameter::getValue< double >( "phaseflow.vicositywetting", 0.002 ) ),
      inhom_theta_(Dune::Fem::Parameter::getValue< double >( "phaseflow.inhomogeneitytheta", 2 ) ),
      lens_inhom_theta_(Dune::Fem::Parameter::getValue< double >( "phaseflow.lensinhomogeneitytheta", 2 ) ),
      gravity_value_(Dune::Fem::Parameter::getValue< double >( "phaseflow.gravity_value", -9.81 ) ),
      entry_press_(Dune::Fem::Parameter::getValue< double >( "phaseflow.entrypressure", 5e4 ) ),
      lens_entry_press_(Dune::Fem::Parameter::getValue< double >( "phaseflow.lensentrypressure", 5e4 ) ),
      abs_perm1(Dune::Fem::Parameter::getValue< double >( "phaseflow.domain1absolutepermeability", 6.64e-11 ) ),
      lens_abs_perm(Dune::Fem::Parameter::getValue< double >( "phaseflow.lensabsolutepermeability", 6.64e-11 ) ),
      porosity(Dune::Fem::Parameter::getValue< double >( "phaseflow.porosity", 1 ) ),
      lens_porosity(Dune::Fem::Parameter::getValue< double >( "phaseflow.lensporosity", 1 ) ),
      density_w_(Dune::Fem::Parameter::getValue< double >( "phaseflow.densitywetting", 1 ) ),
      density_n_(Dune::Fem::Parameter::getValue< double >( "phaseflow.densitynonwetting", 1 ) ),
      secondlens_entry_press_(Dune::Fem::Parameter::getValue< double >( "phaseflow.secondlensentrypressure", 5e4 ) ),
      secondlens_abs_perm(Dune::Fem::Parameter::getValue< double >( "phaseflow.secondlensabsolutepermeability", 6.64e-11 ) ),
      secondlens_porosity(Dune::Fem::Parameter::getValue< double >( "phaseflow.secondlensporosity", 1 ) ),
      srw_secondlens_(Dune::Fem::Parameter::getValue< double >( "phaseflow.secondlensresidualwetting", 0.15 ) ),
      secondlens_inhom_theta_(Dune::Fem::Parameter::getValue< double >( "phaseflow.secondlensinhomogeneitytheta", 2 ) ),
      penalty_press_( Dune::Fem::Parameter::getValue< double >( "phaseflow.penaltypress", 1 ) ),
      penalty_sat_(Dune::Fem::Parameter::getValue< double >( "phaseflow.penaltysat", 1 ) ),
      /* coordinate first lens*/
      coord_x_lens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_x_lens0", 1 ) ),
      coord_x_lens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_x_lens1", 1 ) ),
      coord_y_lens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_y_lens0", 1 ) ),
      coord_y_lens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_y_lens1", 1 ) ),
      coord_z_lens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_z_lens0", 0 ) ),
      coord_z_lens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_z_lens1", 0 ) ),
      /* coordinate second lens*/
      coord_x_secondlens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_x_secondlens0", 0 ) ),
      coord_x_secondlens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_x_secondlens1", 0 ) ),
      coord_y_secondlens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_y_secondlens0", 0 ) ),
      coord_y_secondlens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_y_secondlens1", 0 ) ),
      coord_z_secondlens0(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_z_secondlens0", 0 ) ),
      coord_z_secondlens1(Dune::Fem::Parameter::getValue< double >( "phaseflow.coord_z_secondlens1", 0 ) ),
      inputset(10),
         epo(10),
         perf(10),
         epon(10),
         perfn(10),
         resultdnn(10)
  {
    launchdnnKrn(epon, perfn);
    launchdnnKrw(epo, perf);

    // prepdnn();
    if ( dimDomain == 3 )
    {
      gravity_[dimDomain-3] = 0;
      gravity_[dimDomain-2] = 0;
      gravity_[dimDomain-1] = gravity_value_;
    }
    else if ( dimDomain == 2 )
    {
      gravity_[dimDomain-2] = 0;
      gravity_[dimDomain-1] = gravity_value_;
    }
  }



  void launchdnnKrn( vector<double> epch, vector<double>perf ) const
  {
      ///TRAINING DATA GENERATION
      // cout<< "Initial Data: ";
      for (int i = 0; i < numTrainingSets; i++)
      {
      double p = (1.0*(double)i/numTrainingSets);
      training_inputs[i][0] = (p);

      double tetha_in( 0 );
      double effsat;
      double Krn;
      //
      // inhomegeneity_tetha( entity, x, tetha_in );
      // effective_sat( entity, x, p, effsat );
      //
      // if (1>p)
      //   Krw = (std::pow((1-p),(2.+3*2.0)/2.0));
      // else
      //   Krw =0;

      if (p>=0 && (1>p))
        Krn = std::pow(p,2.)*(1-std::pow((1-p),(2.+2)/2));
      else
        Krn=0;

  // if (1>effsat)
  //   Krw = (std::pow((1-effsat),(2.+3*tetha_in)/tetha_in));
  // else
  //   Krw =0;

       auto testwmob = Krn/9.0e-4;




      // // training_outputs[i][0] = p*cos(p);
      // auto powS = pow(p,3.5);
      // auto pow1mS = pow(1.0-p,1.5);
      // auto kr = 1.0*powS/(powS+pow1mS*3);
      training_outputs[i][0] = testwmob;
      /***************************Try Avoiding Edits In This part*******************************/
          ///FINDING NORMALIZING FACTOR
          for(int m=0; m<numInputs; ++m)
              if(MAXX < training_inputs[i][m])
                  MAXX = training_inputs[i][m];
          for(int m=0; m<numOutputs; ++m)
              if(MAXX < training_outputs[i][m])
                  MAXX = training_outputs[i][m];

    }
      ///NORMALIZING
      for (int i = 0; i < numTrainingSets; i++)
      {
            for(int m=0; m<numInputs; ++m)
                training_inputs[i][m] /= 1.0f*MAXX;

            for(int m=0; m<numOutputs; ++m)
                training_outputs[i][m] /= 1.0f*MAXX;
            //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
            //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
      }
      // cout<<endl;
      ///WEIGHT & BIAS INITIALIZATION
      for (int i=0; i<numInputs; i++) {
          for (int j=0; j<numHiddenNodes; j++) {
              hiddenWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numHiddenNodes; i++) {
          hiddenLayerBias[i] = init_weight();
          for (int j=0; j<numOutputs; j++) {
              outputWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numOutputs; i++) {
          //outputLayerBias[i] = init_weight();
          outputLayerBias[i] = 0;
      }

      ///FOR INDEX SHUFFLING
      int trainingSetOrder[numTrainingSets];
      for(int j=0; j<numTrainingSets; ++j)
          trainingSetOrder[j] = j;


      ///TRAINING
      //std::cout<<"start train\n";
      vector<double> performance, epo; ///STORE MSE, EPOCH
      // vector<double> performance; ///STORE MSE, EPOCH

      for (int n=0; n < epoch; n++)
      {
          double MSE = 0;
          shuffle(trainingSetOrder,numTrainingSets);
          // std::cout<<"\nepoch :"<<n;
          for (int x=0; x<numTrainingSets; x++)
          {
              int i = trainingSetOrder[x];
              //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
              //int x=i;
              //std::cout<<"Training Set :"<<x<<"\n";
              /// Forward pass
              for (int j=0; j<numHiddenNodes; j++)
              {
                  double activation=hiddenLayerBias[j];
                  //std::cout<<"Training Set :"<<x<<"\n";
                   for (int k=0; k<numInputs; k++) {
                      activation+=training_inputs[i][k]*hiddenWeights[k][j];
                  }
                  hiddenLayer[j] = tanh(activation);
              }

              for (int j=0; j<numOutputs; j++) {
                  double activation=outputLayerBias[j];
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      activation+=hiddenLayer[k]*outputWeights[k][j];
                  }
                  outputLayer[j] = lin(activation);
              }

              //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
              for(int k=0; k<numOutputs; ++k)
                  MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);

             /// Backprop
             ///   For V
              double deltaOutput[numOutputs];
              for (int j=0; j<numOutputs; j++) {
                  double errorOutput = (training_outputs[i][j]-outputLayer[j]);
                  deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
              }

              ///   For W
              double deltaHidden[numHiddenNodes];
              for (int j=0; j<numHiddenNodes; j++) {
                  double errorHidden = 0.0f;
                  for(int k=0; k<numOutputs; k++) {
                      errorHidden+=deltaOutput[k]*outputWeights[j][k];
                  }
                  deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
              }

              ///Updation
              ///   For V and b
              for (int j=0; j<numOutputs; j++) {
                  //b
                  outputLayerBias[j] += deltaOutput[j]*lr;
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
                  }
              }

              ///   For W and c
              for (int j=0; j<numHiddenNodes; j++) {
                  //c
                  hiddenLayerBias[j] += deltaHidden[j]*lr;
                  //W
                  for(int k=0; k<numInputs; k++) {
                    hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
                  }
              }
          }
          //Averaging the MSE
          MSE /= 1.0f*numTrainingSets;
          //cout<< "  MSE: "<< MSE<<endl;
          ///Steps to PLOT PERFORMANCE PER EPOCH
          performance.push_back(MSE*100);
          epo.push_back(n);
      }

      epch =epo;
      perf = performance;
      // return epo;
      //
      // //  // Print weights
      //  // std::cout << "Final Hidden Weights\n[ ";
      //  for (int j=0; j<numHiddenNodes; j++) {
      //      // std::cout << "[ ";
      //      for(int k=0; k<numInputs; k++) {
      //          // std::cout << hiddenWeights[k][j] << " ";
      //      }
      //      // std::cout << "] ";
      //  }
      //  // std::cout << "]\n";
      //
      //  // std::cout << "Final Hidden Biases\n[ ";
      //  for (int j=0; j<numHiddenNodes; j++) {
      //      // std::cout << hiddenLayerBias[j] << " ";
      //  }
       // std::cout << "]\n";
       // std::cout << "Final Output Weights";
       // for (int j=0; j<numOutputs; j++) {
       //     // std::cout << "[ ";
       //     for (int k=0; k<numHiddenNodes; k++) {
       //         std::cout << outputWeights[k][j] << " ";
       //     }
       //     std::cout << "]\n";
       // }
       // std::cout << "Final Output Biases\n[ ";
       // for (int j=0; j<numOutputs; j++) {
       //     std::cout << outputLayerBias[j] << " ";
       // }
       // std::cout << "]\n";

         //Plot the results
        vector<float> x;
        vector<float> y1, y2;
         double test_input[1000][numInputs];
         int numTestSets = numTrainingSets;
        for (float i = 0; i < numTestSets; i=i+0.25)
        {
             double p = (1.0*(double)i/numTestSets);
         x.push_back(p);
         // // y1.push_back(p*cos(p));
         // auto powS = pow(p,3.5);
         // auto pow1mS = pow(1.0-p,1.5);
         // auto kr = 1.0*powS/(powS+pow1mS*3);
         double Kr;
        
         if (1>p)
           Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
         else
           Kr =0;
        
         // auto testwmob = Krw/1.0e-3;
         y1.push_back(Kr);
        
         double test_input[1];
         test_input[0] = p/MAXX;
             predict(test_input);
         y2.push_back(outputLayer[0]*MAXX);
        
        }
        std::cout <<"yyyyy Output "<< y2.size()<< " ";
        //
        FILE * gp = popen("gnuplot", "w");
        fprintf(gp, "set terminal \n");
        fprintf(gp, "set grid \n");
        fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
        fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
        fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
        fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
        
        ///Exact f(x) = sin(x) -> Green Graph
        for (int k = 0; k < x.size(); k++) {
          fprintf(gp, "%f %f \n", x[k], y1[k]);
        }
        fprintf(gp, "e\n");
        //
        // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
        for (int k = 0; k < x.size(); k++) {
          fprintf(gp, "%f %f \n", x[k], y2[k]);
          std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
        
        }
        fprintf(gp, "e\n");
        
        fflush(gp);
          ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
          FILE * gp1 = popen("gnuplot", "w");
        fprintf(gp1, "set terminal \n");
        fprintf(gp1, "set grid \n");
        fprintf(gp1, "set title '%s' \n", "Performance");
        fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
        fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
        fprintf(gp1, "plot '-' w p ls 1 \n");
        
          for (int k = 0; k < epo.size(); k++) {
          fprintf(gp1, "%f %f \n", epo[k], performance[k]);
        }
        fprintf(gp1, "e\n");
        
        fflush(gp1);
        //
        system("pause");
        //_pclose(gp);
        cin.get();
        //  resultdnn = y2;
        //  inputset = x;
        //    // return 0;
        //  // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
        //  // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
       }



  void launchdnnKrw( vector<double> epch, vector<double>perf ) const
  {
      ///TRAINING DATA GENERATION
      // cout<< "Initial Data: ";
      for (int i = 0; i < numTrainingSets; i++)
      {
      double p = (1.0*(double)i/numTrainingSets);
      training_inputs[i][0] = (p);

      double tetha_in( 0 );
      double effsat;
      double Krw;
      //
      // inhomegeneity_tetha( entity, x, tetha_in );
      // effective_sat( entity, x, p, effsat );
      //
      if (1>p)
        Krw = (std::pow((1-p),(2.+3*2.0)/2.0));
      else
        Krw =0;

       auto testwmob = Krw/1.0e-3;


      // // training_outputs[i][0] = p*cos(p);
      // auto powS = pow(p,3.5);
      // auto pow1mS = pow(1.0-p,1.5);
      // auto kr = 1.0*powS/(powS+pow1mS*3);
      training_outputs[i][0] = testwmob;
      /***************************Try Avoiding Edits In This part*******************************/
          ///FINDING NORMALIZING FACTOR
          for(int m=0; m<numInputs; ++m)
              if(MAXX < training_inputs[i][m])
                  MAXX = training_inputs[i][m];
          for(int m=0; m<numOutputs; ++m)
              if(MAXX < training_outputs[i][m])
                  MAXX = training_outputs[i][m];

    }
      ///NORMALIZING
      for (int i = 0; i < numTrainingSets; i++)
      {
            for(int m=0; m<numInputs; ++m)
                training_inputs[i][m] /= 1.0f*MAXX;

            for(int m=0; m<numOutputs; ++m)
                training_outputs[i][m] /= 1.0f*MAXX;
            //cout<<" "<<"("<<training_inputs[i][0]<<","<<training_outputs[i][0] <<")";
            //cout<<"In: "<<training_inputs[i][0]<<"  out: "<<training_outputs[i][0]<<endl;
      }
      // cout<<endl;
      ///WEIGHT & BIAS INITIALIZATION
      for (int i=0; i<numInputs; i++) {
          for (int j=0; j<numHiddenNodes; j++) {
              hiddenWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numHiddenNodes; i++) {
          hiddenLayerBias[i] = init_weight();
          for (int j=0; j<numOutputs; j++) {
              outputWeights[i][j] = init_weight();
          }
      }
      for (int i=0; i<numOutputs; i++) {
          //outputLayerBias[i] = init_weight();
          outputLayerBias[i] = 0;
      }

      ///FOR INDEX SHUFFLING
      int trainingSetOrder[numTrainingSets];
      for(int j=0; j<numTrainingSets; ++j)
          trainingSetOrder[j] = j;


      ///TRAINING
      //std::cout<<"start train\n";
      vector<double> performance, epo; ///STORE MSE, EPOCH
      // vector<double> performance; ///STORE MSE, EPOCH

      for (int n=0; n < epoch; n++)
      {
          double MSE = 0;
          shuffle(trainingSetOrder,numTrainingSets);
          // std::cout<<"\nepoch :"<<n;
          for (int x=0; x<numTrainingSets; x++)
          {
              int i = trainingSetOrder[x];
              //cout<<" "<<"("<<training_inputs[x][0]<<","<<training_outputs[x][0] <<")";
              //int x=i;
              //std::cout<<"Training Set :"<<x<<"\n";
              /// Forward pass
              for (int j=0; j<numHiddenNodes; j++)
              {
                  double activation=hiddenLayerBias[j];
                  //std::cout<<"Training Set :"<<x<<"\n";
                   for (int k=0; k<numInputs; k++) {
                      activation+=training_inputs[i][k]*hiddenWeights[k][j];
                  }
                  hiddenLayer[j] = tanh(activation);
              }

              for (int j=0; j<numOutputs; j++) {
                  double activation=outputLayerBias[j];
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      activation+=hiddenLayer[k]*outputWeights[k][j];
                  }
                  outputLayer[j] = lin(activation);
              }

              //std::cout << "Input:" << training_inputs[x][0] << " " << "    Output:" << outputLayer[0] << "    Expected Output: " << training_outputs[x][0] << "\n";
              for(int k=0; k<numOutputs; ++k)
                  MSE += (1.0f/numOutputs)*pow( training_outputs[i][k] - outputLayer[k], 2);

             /// Backprop
             ///   For V
              double deltaOutput[numOutputs];
              for (int j=0; j<numOutputs; j++) {
                  double errorOutput = (training_outputs[i][j]-outputLayer[j]);
                  deltaOutput[j] = errorOutput*dlin(outputLayer[j]);
              }

              ///   For W
              double deltaHidden[numHiddenNodes];
              for (int j=0; j<numHiddenNodes; j++) {
                  double errorHidden = 0.0f;
                  for(int k=0; k<numOutputs; k++) {
                      errorHidden+=deltaOutput[k]*outputWeights[j][k];
                  }
                  deltaHidden[j] = errorHidden*dtanh(hiddenLayer[j]);
              }

              ///Updation
              ///   For V and b
              for (int j=0; j<numOutputs; j++) {
                  //b
                  outputLayerBias[j] += deltaOutput[j]*lr;
                  for (int k=0; k<numHiddenNodes; k++)
                  {
                      outputWeights[k][j]+= hiddenLayer[k]*deltaOutput[j]*lr;
                  }
              }

              ///   For W and c
              for (int j=0; j<numHiddenNodes; j++) {
                  //c
                  hiddenLayerBias[j] += deltaHidden[j]*lr;
                  //W
                  for(int k=0; k<numInputs; k++) {
                    hiddenWeights[k][j]+=training_inputs[i][k]*deltaHidden[j]*lr;
                  }
              }
          }
          //Averaging the MSE
          MSE /= 1.0f*numTrainingSets;
          //cout<< "  MSE: "<< MSE<<endl;
          ///Steps to PLOT PERFORMANCE PER EPOCH
          performance.push_back(MSE*100);
          epo.push_back(n);
      }

      epch =epo;
      perf = performance;
       }








void dnnprediction(vector<double> epo, vector<double>performance,std::vector<float> inputset, std::vector<float> resultdnn )const
{

             vector<float> x;
             vector<float> y1, y2;
              double test_input[1000][numInputs];
              int numTestSets = numTrainingSets;
             for (float i = 0; i < numTestSets; i=i+0.25)
             {
                  double p = (1.0*(double)i/numTestSets);
              x.push_back(p);
              // // y1.push_back(p*cos(p));
              // auto powS = pow(p,3.5);
              // auto pow1mS = pow(1.0-p,1.5);
              // auto kr = 1.0*powS/(powS+pow1mS*3);
              double Kr;

              if (1>p)
                Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
              else
                Kr =0;

              // auto testwmob = Krw/1.0e-3;
              y1.push_back(Kr);

              double test_input[1];
              test_input[0] = p/MAXX;
                  predict(test_input);
              y2.push_back(outputLayer[0]*MAXX);

             }
             // std::cout <<"yyyyy Output "<< y2.size()<< " ";
             // //
             // FILE * gp = popen("gnuplot", "w");
             // fprintf(gp, "set terminal wxt size 600,400 \n");
             // fprintf(gp, "set grid \n");
             // fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
             // fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
             // fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
             // fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
             //
             // ///Exact f(x) = sin(x) -> Green Graph
             // for (int k = 0; k < x.size(); k++) {
             //   fprintf(gp, "%f %f \n", x[k], y1[k]);
             // }
             // fprintf(gp, "e\n");
             // //
             // // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
             // for (int k = 0; k < x.size(); k++) {
             //   fprintf(gp, "%f %f \n", x[k], y2[k]);
             //   std::cout <<"x coord"<< x[k]<<" Output "<< y2[k] << " ";
             //
             // }
             // fprintf(gp, "e\n");
             //
             // fflush(gp);
             //   ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
             //   FILE * gp1 = popen("gnuplot", "w");
             // fprintf(gp1, "set terminal wxt size 600,400 \n");
             // fprintf(gp1, "set grid \n");
             // fprintf(gp1, "set title '%s' \n", "Performance");
             // fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
             // fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
             // fprintf(gp1, "plot '-' w p ls 1 \n");
             //
             //   for (int k = 0; k < epo.size(); k++) {
             //   fprintf(gp1, "%f %f \n", epo[k], performance[k]);
             // }
             // fprintf(gp1, "e\n");
             //
             // fflush(gp1);
             // //
             // system("pause");
             // //_pclose(gp);
             // cin.get();
              resultdnn = y2;
              inputset = x;
              // std::cout << "resultdnn10";
              // //
                // std::cout << y2[10] << " ";
                // std::cout << "]\n";
                // return 0;
              // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
              // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";
            }

  const GravitationVectorType &gravity() const
  {
    return gravity_;
  }

  const double &density_w() const
  {
    return density_w_;
  }

  const double &density_n() const
  {
    return density_n_;
  }

  //Effective saturation s_e= 1-\frac{1-s_w-s_rw}{1-s_rw -s_rn}
template< class Entity, class Point >
void effective_sat( const Entity &entity,
                    const Point &x,
                    const RangeType &value,
                    double &effsat ) const
{
  double tetha_in( 0 );
  double irred_water_sat( 0 );
  //
  inhomegeneity_tetha( entity, x, tetha_in );
  irreduct_water_satu( entity, x, irred_water_sat );
  //
  effsat = value[1]-srn_;
  effsat /= (1-irred_water_sat-srn_);
}

//Capillary pressure function p_c=p_d (1-s_n)^{-\frac{1}{inhom_theta_}}
template< class Entity, class Point >
void cap_Press ( const Entity &entity,
     const Point &x,
     const RangeType &value,
     RangeType &cap_press ) const
{
  double tetha_in( 0 );
  double entry_press( 0 );
  double effsat( 0 );
  //
  inhomegeneity_tetha( entity, x, tetha_in );
  entry_Pressure( entity, x, entry_press );
  effective_sat( entity, x, value, effsat );
  //
  if (effsat>=0 && (1>effsat))
    cap_press[1] = entry_press*(std::pow((1-effsat),-1/tetha_in));
  else
    cap_press[1] = 0;
}

//First derivative of the cappilary pressure
template< class Entity, class Point >
void first_der_cap_Press( const Entity &entity,
                          const Point &x,
                          const RangeType &value,
                          RangeType &first_der_cap_press ) const
{
  double tetha_in( 0 );
  double irred_water_sat( 0 );
  double entry_press( 0 );
  double effsat( 0 );
  RangeType cappress;
  //
  irreduct_water_satu( entity, x, irred_water_sat );
  inhomegeneity_tetha( entity, x, tetha_in );
  entry_Pressure( entity, x, entry_press );
  effective_sat( entity, x, value, effsat );
  cap_Press( entity, x, value, cappress );
  //
  first_der_cap_press[0] = 0;
  first_der_cap_press[1] = (1/tetha_in)*std::pow(entry_press,-1*tetha_in)*(std::pow(cappress[1],(1+tetha_in)));
  first_der_cap_press[1] /= (1-irred_water_sat-srn_);
}

//Second derivative of the cappilary pressure
template< class Entity, class Point >
void second_der_cap_Press( const Entity &entity,
                           const Point &x,
                           const RangeType &value,
                           RangeType &sec_der_cap_press) const
{
  double tetha_in( 0 );
  double entry_press( 0 );
  double irred_water_sat( 0 );
  double effsat( 0 );
  RangeType first_der_cap_pressure,cappress;
  //
  irreduct_water_satu( entity, x, irred_water_sat );
  entry_Pressure( entity, x, entry_press );
  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat( entity, x, value, effsat );
  first_der_cap_Press( entity, x, value, first_der_cap_pressure );
  cap_Press( entity, x, value, cappress );
  //
  sec_der_cap_press[0] = 0;
  sec_der_cap_press[1] = (1/tetha_in)*std::pow(entry_press,-1*tetha_in)*(1+tetha_in)*(std::pow(cappress[1],tetha_in))*first_der_cap_pressure[1];
  sec_der_cap_press[1] /= (1-irred_water_sat-srn_);
}

//gradient of the cappilary pressure
template< class Entity, class Point >
void grad_cap_Press( const Entity &entity,
                     const Point &x,
                     const RangeType &value,
                     const JacobianRangeType & gradient,
                     JacobianRangeType &grad_cap_press ) const
{
  double tetha_in( 0 );
  double effsat( 0 );
  RangeType firs_der_cap( 0 );
  //
  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat( entity, x, value, effsat );
  first_der_cap_Press( entity, x, value, firs_der_cap );
  //
  grad_cap_press[0] = 0;
  grad_cap_press[1] = gradient[1];
  grad_cap_press[1] *= 1*firs_der_cap[1];
}

//Non wetting mobility
template< class Entity, class Point >
void nonWet_mobility( const Entity &entity,
                      const Point &x,
                      const RangeType &value,
                      double &nwmob ) const
{
  double tetha_in( 0 );
  double effsat( 0 );
  double Krn;
  //
  // launchdnn();
    launchdnnKrn(epon, perfn);



                       vector<float> xx;
                       vector<float> y1, y2;
                        double test_input[1000][numInputs];
                        int numTestSets = numTrainingSets;
                       for (float i = 0; i < numTestSets; i=i+0.25)
                       {
                            double p = (1.0*(double)i/numTestSets);
                        xx.push_back(p);
                        // // y1.push_back(p*cos(p));
                        // auto powS = pow(p,3.5);
                        // auto pow1mS = pow(1.0-p,1.5);
                        // auto kr = 1.0*powS/(powS+pow1mS*3);
                        double Krn;

                        // if (1>p)
                        //   Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
                        // else
                        //   Kr =0;

                          if (p>=0 && (1>p))
                            Krn = std::pow(p,2.)*(1-std::pow((1-p),(2.+2)/2));
                          else
                            Krn=0;

                      // if (1>effsat)
                      //   Krw = (std::pow((1-effsat),(2.+3*tetha_in)/tetha_in));
                      // else
                      //   Krw =0;

                          auto testwmob = Krn/9.0e-4;


                        // auto testwmob = Krw/1.0e-3;
                        y1.push_back(testwmob);

                        double test_input[1];
                        test_input[0] = p/MAXX;
                            predict(test_input);
                        y2.push_back(outputLayer[0]*MAXX);

                       }
                       // std::cout <<"yyyyy Output "<< y2.size()<< " ";
                       //
                       FILE * gp = popen("gnuplot", "w");
                       fprintf(gp, "set terminal   \n");
                       fprintf(gp, "set grid \n");
                       fprintf(gp, "set title '%s' \n", "f(x) = x sin (x)");
                       fprintf(gp, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
                       fprintf(gp, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
                       fprintf(gp, "plot '-' w p ls 1, '-' w p ls 2 \n");
                       
                       ///Exact f(x) = sin(x) -> Green Graph
                       for (int k = 0; k < xx.size(); k++) {
                         fprintf(gp, "%f %f \n", xx[k], y1[k]);
                       }
                       fprintf(gp, "e\n");
                       //
                       // ///Neural Network Approximate f(x) = xsin(x) -> Red Graph
                       for (int k = 0; k < xx.size(); k++) {
                         fprintf(gp, "%f %f \n", xx[k], y2[k]);
                         std::cout <<"xx coord"<< xx[k]<<" Output "<< y2[k] << " ";
                       
                       }
                       fprintf(gp, "e\n");
                       
                       fflush(gp);
                         ///FILE POINTER FOR SECOND PLOT (PERFORMANCE GRAPH)
                         FILE * gp1 = popen("gnuplot", "w");
                       fprintf(gp1, "set terminal   \n");
                       fprintf(gp1, "set grid \n");
                       fprintf(gp1, "set title '%s' \n", "Performance");
                       fprintf(gp1, "set style line 1 lt 3 pt 7 ps 0.1 lc rgb 'green' lw 1 \n");
                       fprintf(gp1, "set style line 2 lt 3 pt 7 ps 0.1 lc rgb 'red' lw 1 \n");
                       fprintf(gp1, "plot '-' w p ls 1 \n");
                       
                         for (int k = 0; k < epo.size(); k++) {
                         fprintf(gp1, "%f %f \n", epo[k], perf[k]);
                       }
                       fprintf(gp1, "e\n");
                       
                       fflush(gp1);
                       //
                       system("pause");
                       //_pclose(gp);
                       cin.get();
                        // resultdnn = y2;
                        // // inputset = xx;
                        // std::cout << "resultdnn10";
                        // //
                        //   std::cout<<nwmob << y2[119*value[1]] << " ";
                        //   std::cout<<xx.size()<< y2.size() << " ";
                        
                        //   std::cout << "]\n";
                          // return 0;
                        // std::cout<<"\testresultdnn :"<< resultdnn <<std::endl;
                        // std::cout <<"x coord"<< x<<" Output "<< y2 <<" ";

  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat ( entity, x, value, effsat );
  //
  if (effsat>=0 && (1>effsat))
    Krn = std::pow(effsat,2.)*(1-std::pow((1-effsat),(2.+tetha_in)/tetha_in));
  else
    Krn=0;
  nwmob = Krn/visc_n_;


  // std::cout<<nwmob <<"nwmob"<< y2[119*value[1]] << std::endl;

}

//Wetting mobility
template< class Entity, class Point >
void wet_mobility( const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   double &wmob ) const
{
  double tetha_in( 0 );
  double effsat;
  double Krw;
  //
  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat( entity, x, value, effsat );
  //


  vector<float> xx;
  vector<float> y1, y2;
   double test_input[1000][numInputs];
   int numTestSets = numTrainingSets;
  for (float i = 0; i < numTestSets; i=i+0.25)
  {
       double p = (1.0*(double)i/numTestSets);
   xx.push_back(p);
   // // y1.push_back(p*cos(p));
   // auto powS = pow(p,3.5);
   // auto pow1mS = pow(1.0-p,1.5);
   // auto kr = 1.0*powS/(powS+pow1mS*3);
   double Kr;

   if (1>p)
     Kr = (std::pow((1-p),(2.+3*2.0)/2.0));
   else
     Kr =0;

   auto testwmob = Kr/1.0e-3;
   y1.push_back(testwmob);

   double test_input[1];
   test_input[0] = p/MAXX;
       predict(test_input);
   y2.push_back(outputLayer[0]*MAXX);

  }

  
  if (1>effsat)
    Krw = (std::pow((1-effsat),(2.+3*tetha_in)/tetha_in));
  else
    Krw =0;
  wmob = Krw/visc_w_;


    // std::cout<<wmob <<"wmob"<< y2[119*value[1]] << std::endl;

}

//Total mobility
template< class Entity, class Point >
void tot_mobility( const Entity &entity,
                   const Point &x,
                   const RangeType &value,
                   double &totmob ) const
{
  double nwmob( 0 ),wmob( 0 );
  //
  wet_mobility ( entity, x, value, wmob );
  nonWet_mobility ( entity, x, value, nwmob );
  //
  totmob = nwmob;
  totmob+=wmob;
}

template< class EntityInside, class EntityOutside, class PointInside, class PointOutside , class DomainTypenormal>
void upwindingMobility( const EntityInside &entityIn,
                        const PointInside &xIn,
                        const EntityOutside &entityOut,
                        const PointOutside &xOut,
                        const DomainTypenormal normal,
                        const RangeType& u0En,
                        const RangeType& u0OutEn,
                        const JacobianRangeType& u0EnJac1,
                        const JacobianRangeType& u0OutJac1,
                        double &upwMob,
                        double &upwgradnonwetmob) const
{
  double permeabilityIn ( 0 ), permeabilityOut( 0 );
  double tem( 0 );
  double nwmobIn( 0 ), nwmobOut( 0 ), gradnonwetmobIn( 0 ), gradnonwetmobOut( 0 );
  JacobianRangeType upw;
  RangeType first_der_cap_pressure;
  //
  permeability_K( entityIn,xIn, permeabilityIn );
  permeability_K( entityOut,xOut, permeabilityOut );
  first_der_cap_Press( entityIn, xIn, u0En, first_der_cap_pressure );
  //
  upw = u0EnJac1;
  upw[1] *= first_der_cap_pressure[1];
  upw[0] += upw[1];

  for (int d=0; d<dimDomain; d++)
  {
    upw[0][d] += 9.81*density_n_;
    tem += normal[d] * upw[0][d];
  }

  nonWet_mobility( entityIn, xIn, u0En, nwmobIn );
  nonWet_mobility( entityOut, xOut, u0OutEn, nwmobOut );
  grad_nonWet_mobility( entityIn,xIn,u0En,gradnonwetmobIn );
  grad_nonWet_mobility( entityOut,xOut,u0OutEn,gradnonwetmobOut );

  if ( tem <= 0 && nwmobOut > 0 )
  {
    upwMob = nwmobOut;
    upwgradnonwetmob = gradnonwetmobOut;
  }
  else if ( tem >= 0 && nwmobOut > 0 )
  {
    upwMob = nwmobIn;
    upwgradnonwetmob = gradnonwetmobIn;
  }
}

//grad of the Non wetting mobility
template< class Entity, class Point >
void grad_nonWet_mobility( const Entity &entity,
                           const Point &x,
                           const RangeType &value,
                           double &gradnonwetmob ) const
{
  double tetha_in( 0 );
  double irred_water_sat( 0 );
  double effsat;
  //
  irreduct_water_satu( entity, x, irred_water_sat );
  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat( entity, x, value, effsat );
  //
  if (effsat>=0 && (1>effsat))
    gradnonwetmob = 1*(2*(effsat)*(1-std::pow(1-effsat,(2.+tetha_in)/tetha_in)))+(1*(2.+tetha_in)/tetha_in)*std::pow(effsat,2.)*(std::pow((1-effsat),(2.)/tetha_in));
  else
    gradnonwetmob =0;

  gradnonwetmob /= visc_n_;
  gradnonwetmob /= (1-irred_water_sat-srn_);
}

//gradient of the wet_mobility
template< class Entity, class Point >
void grad_wet_mobility( const Entity &entity,
                        const Point &x,
                        const RangeType &value,
                        double &gradwetmob ) const
{
  double tetha_in( 0 );
  double irred_water_sat( 0 );
  double effsat;
  //
  irreduct_water_satu( entity, x, irred_water_sat );
  inhomegeneity_tetha( entity, x, tetha_in );
  effective_sat ( entity, x, value, effsat );
  //
  gradwetmob = 0;

  if (effsat>=0 && (1>effsat))
    gradwetmob = (-1*(2.+3*tetha_in)/tetha_in)*std::pow((1-effsat),((2.+3*tetha_in)/tetha_in)-1);
  else
    gradwetmob =0;
  // gradwetmob = (-1*(2.+3*tetha_in)/tetha_in)*std::pow((1-effsat),((2.+3*tetha_in)/tetha_in)-1);
  gradwetmob /= visc_w_ ;
  gradwetmob /= (1-irred_water_sat-srn_);
}

//gradient of the tot mob
template< class Entity, class Point >
void grad_tot_mobility( const Entity &entity,
                        const Point &x,
                        const RangeType &value,
                        double &gradtotmob ) const
{
  double gradwetmob( 0 ), gradnonwetmob( 0 );
  //
  grad_wet_mobility( entity, x, value, gradwetmob );
  grad_nonWet_mobility( entity, x, value, gradnonwetmob );
  //
  gradtotmob = gradwetmob;
  gradtotmob += gradnonwetmob;
}

template< class Entity, class Point,class Entityneigh, class Pointneigh >
void penality_pressure( const Entity &entity,
                        const Point &x1,
                        const Entityneigh &entityneigh,
                        const Pointneigh &x2,
                        double & betapress) const
{
  betapress = penalty_press_;
}

template< class Entity, class Point,class Entityneigh, class Pointneigh >
void penality_saturation( const Entity &entity,
                          const Point &x1,
                          const Entityneigh &entityneigh,
                          const Pointneigh &x2,
                           double & betasat) const
{
  betasat = penalty_sat_;
}

template< class Entity, class Point >
void porosity_PHI( const Entity &entity,
                   const Point &x,
                   double & phi_poro) const
{
  const DomainType xGlobal = entity.geometry().global( Dune::Fem::coordinate( x ) );

  if ( dimDomain == 3 )
  {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) && ( xGlobal[2] >= coord_z_lens0 && xGlobal[2] <= coord_z_lens1 ) )
    phi_poro = lens_porosity;
    else if( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1 ) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) && ( xGlobal[2] >= coord_z_secondlens0 && xGlobal[2] <= coord_z_secondlens1 ) )
    phi_poro = secondlens_porosity;
    else
    phi_poro = porosity;
  }
  else if ( dimDomain == 2 )
  {
    if( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) )
    phi_poro = lens_porosity;
    else if ( (xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1) && (xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) )
    phi_poro = secondlens_porosity;
    else
    phi_poro = porosity;
  }
}

template< class Entity, class Point >
void entry_Pressure( const Entity &entity,
                     const Point &x,
                     double & entry_press ) const
{
  const DomainType xGlobal = entity.geometry().global( Dune::Fem::coordinate( x ) );

  if ( dimDomain == 3)
  {
    if( (xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) && ( xGlobal[2] >= coord_z_lens0 && xGlobal[2] <= coord_z_lens1 ) )
    entry_press =lens_entry_press_;
    else if((xGlobal[0]>=coord_x_secondlens0 && xGlobal[0]<=coord_x_secondlens1) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) && ( xGlobal[2] >= coord_z_secondlens0 && xGlobal[2] <= coord_z_secondlens1 ) )
    entry_press=secondlens_entry_press_;
    else
    entry_press =entry_press_;
  }
  else if ( dimDomain == 2 )
   {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) )
    entry_press = lens_entry_press_;
    else if ( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1 ) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) )
    entry_press = secondlens_entry_press_;
    else
    entry_press =entry_press_;
  }
}

template< class Entity, class Point >
void inhomegeneity_tetha( const Entity &entity,
                          const Point &x,
                          double & tet_inhom) const
{
  const DomainType xGlobal = entity.geometry().global( Dune::Fem::coordinate( x ) );

  if ( dimDomain == 3 )
  {
    if ( (xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) && ( xGlobal[2] >= coord_z_lens0 && xGlobal[2] <= coord_z_lens1 ) )
    tet_inhom = lens_inhom_theta_;
    else if ( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1 ) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) && ( xGlobal[2] >= coord_z_secondlens0 && xGlobal[2] <= coord_z_secondlens1 ) )
    tet_inhom = secondlens_inhom_theta_;
    else
    tet_inhom = inhom_theta_;
  }
  else if ( dimDomain == 2)
   {
    if ( (xGlobal[0] >= coord_x_lens0 && xGlobal[0]<=coord_x_lens1) && (xGlobal[1]>=coord_y_lens0 && xGlobal[1]<=coord_y_lens1 ) )
    tet_inhom = lens_inhom_theta_;
    else if ( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1) && (xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) )
    tet_inhom = secondlens_inhom_theta_;
    else
    tet_inhom = inhom_theta_;
  }
}

template< class Entity, class Point >
void irreduct_water_satu( const Entity &entity,
                          const Point &x,
                          double & swr) const
{
  const DomainType xGlobal = entity.geometry().global( Dune::Fem::coordinate( x ) );

  if ( dimDomain == 3 )
   {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) && ( xGlobal[2] >= coord_z_lens0 && xGlobal[2] <= coord_z_lens1 ) )
    swr = srwlens_;
    else if((xGlobal[0]>=coord_x_secondlens0 && xGlobal[0]<=coord_x_secondlens1) && (xGlobal[1]>=coord_y_secondlens0 && xGlobal[1]<=coord_y_secondlens1 ) && (xGlobal[2]>=coord_z_secondlens0 && xGlobal[2]<=coord_z_secondlens1 ))
    swr =srw_secondlens_;
    else
    swr =srw_;
  }
  else if ( dimDomain==2)
   {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) )
    swr = srwlens_;
    else if ( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1 ) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) )
    swr = srw_secondlens_;
    else
    swr = srw_;
  }
}

template< class Entity, class Point >
void permeability_K( const Entity &entity,
                     const Point &x,
                     double & K ) const
{
  const DomainType xGlobal = entity.geometry().global( Dune::Fem::coordinate( x ) );

  if ( dimDomain == 3 )
   {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) && ( xGlobal[2] >= coord_z_lens0 && xGlobal[2] <= coord_z_lens1 ) )
    K = lens_abs_perm;
    else if((xGlobal[0]>=coord_x_secondlens0 && xGlobal[0]<=coord_x_secondlens1) && (xGlobal[1]>=coord_y_secondlens0 && xGlobal[1]<=coord_y_secondlens1 ) && (xGlobal[2]>=coord_z_secondlens0 && xGlobal[2]<=coord_z_secondlens1 ))
    K = secondlens_abs_perm;
    else
    K = abs_perm1;
  }
  else if ( dimDomain == 2 )
  {
    if ( ( xGlobal[0] >= coord_x_lens0 && xGlobal[0] <= coord_x_lens1 ) && ( xGlobal[1] >= coord_y_lens0 && xGlobal[1] <= coord_y_lens1 ) )
    K = lens_abs_perm;
    else if ( ( xGlobal[0] >= coord_x_secondlens0 && xGlobal[0] <= coord_x_secondlens1 ) && ( xGlobal[1] >= coord_y_secondlens0 && xGlobal[1] <= coord_y_secondlens1 ) )
    K = secondlens_abs_perm;
    else
    K = abs_perm1;
  }
}

protected:

  const ProblemType& problem_;
  const GridPart &gridPart_;

  GravitationVectorType gravity_;

  const double eps_, srn_, srw_,srwlens_, visc_n_, visc_w_, inhom_theta_,lens_inhom_theta_,gravity_value_;
  const double entry_press_, lens_entry_press_, abs_perm1, lens_abs_perm, porosity, lens_porosity, density_w_, density_n_;
  const double  secondlens_entry_press_, secondlens_abs_perm, secondlens_porosity, srw_secondlens_,secondlens_inhom_theta_;

  const double penalty_press_,penalty_sat_;

  std::vector<float> inputset;
  std::vector<float> resultdnn ;
 std::vector<double> perf;
 std::vector<double>  epo;
  std::vector<double> perfn;
 std::vector<double>  epon;
  const double coord_x_lens0,coord_x_lens1,coord_y_lens0,coord_y_lens1,coord_z_lens0,coord_z_lens1;
  const double coord_x_secondlens0,coord_x_secondlens1,coord_y_secondlens0,coord_y_secondlens1,coord_z_secondlens0,coord_z_secondlens1;
};
#endif // #ifndef PHYSICAL_PAR_MODEL_HH
