#ifndef LENS_INITIAL_DATA_HH
#define LENS_INITIAL_DATA_HH

#include <cassert>
#include <cmath>
#include <iostream>

#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/io/parameter.hh>
#include <dune/fem/function/common/gridfunctionadapter.hh>

#include <dune/twophaseDG/algorithms/temporalprobleminterface.hh>


using namespace Dune::Fem;

template <class FunctionSpace>
class InitialData : public TemporalProblemInterface < FunctionSpace >
{
  typedef TemporalProblemInterface < FunctionSpace >  BaseType;
public:
  typedef typename BaseType :: RangeType            RangeType;
  typedef typename BaseType :: DomainType           DomainType;
  typedef typename BaseType :: JacobianRangeType    JacobianRangeType;

  enum { dimRange  = BaseType :: dimRange };
  enum { dimDomain = BaseType :: dimDomain };

  // get time function from base class
  using BaseType :: time ;

  InitialData( const Dune::Fem::TimeProviderBase &timeProvider )
    : BaseType( timeProvider )
  {}

  //! the right hand side data (default = 0)
  virtual void f( const DomainType& x,
                  RangeType& phi ) const
  {
    phi[0] = 0;
    phi[1] = 0;
  }

  //! the exact solution
  virtual void u( const DomainType& x,
                  RangeType& phi ) const
  {
    if ( dimDomain == 3)
    {
      phi[0] = (1-x[2])*9810;
      phi[1] = 0;
    }
    else if ( dimDomain==2)
    {
      phi[0] = (0.65-x[1])*9810;
      phi[1] = 0;
    }
  }

  //! the jacobian of the exact solution
  virtual void uJacobian( const DomainType& x,
                          JacobianRangeType& ret ) const
  {
    // no exact solution
    ret = JacobianRangeType(0);
  }
};

#endif // #ifndef LENS_INITIAL_DATA_HH
