#!/usr/bin/env bash

# This script installs all necessary packages to build DUNE-FemPy.

#Make sure that script exits on failure, and that all commands are printed
set -e
set -x

# Make sure we have updated URLs to packages etc.
sudo apt-get update -y

# system packages
sudo apt-get install -y gfortran
sudo apt-get install -y libmpich-dev
# there seems to be a problem with OpenMPI
#sudo apt-get install -y libopenmpi-dev
sudo apt-get install -y cmake cmake-curses-gui
sudo apt-get install -y python3-venv python3-dev
sudo apt-get install -y pkg-config
sudo apt-get install -y vim

# Other utilities that are required by tutorials etc.
sudo apt-get install unzip -y
