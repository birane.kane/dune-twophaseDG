#!/usr/bin/bash

# create necessary python virtual environment
if ! test -d $HOME/dune-pip ; then
  python3.5 -m venv $HOME/dune-pip
  source $HOME/dune-pip/bin/activate
  pip install --upgrade pip
  pip install ufl numpy matplotlib scipy jupyter ipython
else
  source $HOME/dune-pip/bin/activate
fi

#change appropriately, i.e. 2.6 or empty which refers to master
DUNEVERSION=

FLAGS="-O3 -DNDEBUG -funroll-loops -finline-functions -Wall -ftree-vectorize -fno-stack-protector -mtune=native"

DUNECOREMODULES="dune-common dune-istl dune-geometry dune-grid"
DUNEEXTMODULES="dune-python dune-alugrid"
DUNEFEMMODULES="dune-fem dune-fempy dune-fem-dg dune-vem"

#PY_CXXFLAGS=`python3-config --includes`
FLAGS="$FLAGS $PY_CXXFLAGS"

#PY_LDFLAGS=`python3-config --ldflags`


# build flags for all DUNE and OPM modules
# change according to your needs
if ! test -f config.opts ; then
echo "\
DUNEPATH=`$PWD`
BUILDDIR=build-cmake
USE_CMAKE=yes
MAKE_FLAGS=-j8
CMAKE_FLAGS=\"-DCMAKE_CXX_FLAGS=\\\"$FLAGS\\\"  \\
 -DDUNE_PYTHON_INSTALL_EDITABLE=TRUE \\
 -DADDITIONAL_PIP_PARAMS="-upgrade" \\
 -DCMAKE_LD_FLAGS=\\\"$PY_LDFLAGS\\\" \\
 -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE \\
 -DPETSC_DIR=/usr/lib/petscdir/3.10\\
 -DDISABLE_DOCUMENTATION=TRUE \\
 -DCMAKE_DISABLE_FIND_PACKAGE_MPI=FALSE \\
 -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE\" " > config.opts
fi

DUNEBRANCH=
if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi



# build all DUNE modules using dune-control
./dune-common/bin/dunecontrol --opts=config.opts all

# install all python modules in the pip environment
./dune-python/bin/setup-dunepy.py --opts=config.opts all
