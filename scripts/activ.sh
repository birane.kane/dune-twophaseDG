#!/usr/bin/bash

# set current main working directory
export DUNE_CONTROL_PATH=/$PWD

# defines CMAKE_FLAGS
source ${DUNE_CONTROL_PATH}/config.opts

# CRITICAL, DEBUG or NOTSET
export DUNE_LOG_LEVEL=CRITICAL
export DUNE_LOG_FORMAT='%(asctime)s - %(name)s - %(levelname)s - %(message)s'

export DUNE_PY_DIR=${DUNE_CONTROL_PATH}/cache/

export DUNE_CMAKE_FLAGS=${CMAKE_FLAGS}
export PYTHONPATH=${DUNE_CONTROL_PATH}/dune-python/${BUILDDIR}/python:${DUNE_CONTROL_PATH}/dune-alugrid/${BUILDDIR}/python:${DUNE_CONTROL_PATH}/dune-fempy/${BUILDDIR}/python:${DUNE_CONTROL_PATH}/dune-fem-dg/${BUILDDIR}/python:${DUNE_CONTROL_PATH}/dune-vem/${BUILDDIR}/python:${DUNE_CONTROL_PATH}/dune-fem/${BUILDDIR}/python

