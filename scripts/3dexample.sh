#!/bin/bash
spinner()
  {
  local pid=$1
  local delay=0.175
  local spinstr='|/-\'
  local infotext=$2
  while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
    local temp=${spinstr#?}
    printf " [%c] %s" "$spinstr" "$infotext"
    local spinstr=$temp${spinstr%"$temp"}
    sleep $delay
    printf "\b\b\b\b\b\b"
    for i in $(seq 1 ${#infotext}); do
      printf "\b"
    done
  done
  printf " \b\b\b\b"
  }


echo "Monomial basis test."
sed -i.bak 's/\(-DUSE_LAG_DG=\).*/\10/g' ../../../../../src/test/lenspb/3dlens/CMakeLists.txt

echo "Piecewise linear."
sed -i.bak 's/\(-DPOLORDER=\).*/\11/g' ../../../../../src/test/lenspb/3dlens/CMakeLists.txt

make clean && make > /dev/null

echo "Computing 3D problem with Monomial Basis and Piecewise Linear."

./3dlens ./../data/parameter3dMonoBasisDeg1 > 3dMonoBasisDeg1.log&
clear

echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Cubic functions .. PENAlty Saturation=1e-3 --- COMPLETE"

spinner $! "Computing 3D problem with Monomial Basis and Piecewise Linear functions."

# echo "Monomial basis test."
# sed -i.bak 's/\(-DUSE_LAG_DG=\).*/\10/g' ../../../../../src/test/lenspb/3dlens/CMakeLists.txt
#
# echo "Piecewise quadratic."
# sed -i.bak 's/\(-DPOLORDER=\).*/\12/g' ../../../../../src/test/lenspb/3dlens/CMakeLists.txt
#
# make clean && make > /dev/null
#
# echo "Computing 3D problem with Monomial Basis and Piecewise Quadratic functions"
#
# ./3dlens ./../data/parameter3dMonoBasisDeg2 > 3dMonoBasisDeg2.log&
# clear
# echo "Computing 3D problem with Monomial Basis and Piecewise Quadratic functions --- COMPLETE"
#
# echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
# echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
# echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
# echo "Computing with Lagrange Basis and Piecewise Cubic functions .. PENAlty Saturation=1e-3 --- COMPLETE"
#
# echo "Computing 3D problem with Monomial Basis and Piecewise Linear functions --- COMPLETE"
#
# spinner $! "Computing 3D problem with Monomial Basis and Piecewise Quadratic functions."
#
#
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Cubic functions .. PENAlty Saturation=1e-3 --- COMPLETE"
#
echo "Computing 3D problem with Monomial Basis and Piecewise Linear functions --- COMPLETE"
# echo "Computing 3D problem with Monomial Basis and Piecewise Quadratic functions --- COMPLETE"
