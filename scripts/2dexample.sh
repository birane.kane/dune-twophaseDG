#!/bin/bash
spinner()
  {
    local pid=$1
    local delay=0.175
    local spinstr='|/-\'
    local infotext=$2
    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
      local temp=${spinstr#?}
      printf " [%c] %s" "$spinstr" "$infotext"
      local spinstr=$temp${spinstr%"$temp"}
      sleep $delay
      printf "\b\b\b\b\b\b"
      for i in $(seq 1 ${#infotext}); do
        printf "\b"
      done
    done
    printf " \b\b\b\b"
  }


echo "Monomial basis test."
sed -i.bak 's/\(-DUSE_LAG_DG=\).*/\10/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

echo "Piecewise linear."
sed -i.bak 's/\(-DPOLORDER=\).*/\11/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

make clean && make > /dev/null

echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3"

./2dlens ./../data/parameter2dMonoBasisDeg1PenSat1e-3 > MonoBasisDeg1PenSat1e-3.log&
clear
spinner $! "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3."

echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2"
 ./2dlens ./../data/parameter2dMonoBasisDeg1PenSat1e-2 > MonoBasisDeg1PenSat1e-2.log &
clear

echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
spinner $! "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2."


echo "Lagrange basis test."
sed -i.bak 's/\(-DUSE_LAG_DG=\).*/\11/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

echo "Piecewise linear."
sed -i.bak 's/\(-DPOLORDER=\).*/\11/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

make clean && make > /dev/null

echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3"
./2dlens ./../data/parameter2dLagBasisDeg1PenSat1e-2 > lagbasisDeg1.log &
clear
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"

spinner $! "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2."
clear


echo "Lagrange basis test."
sed -i.bak 's/\(-DUSE_LAG_DG=\).*/\11/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

echo "Piecewise cubics."
sed -i.bak 's/\(-DPOLORDER=\).*/\13/g' ../../../../../src/test/lenspb/2dlens/CMakeLists.txt

make clean && make > /dev/null

echo "Computing with Lagrange Basis and Piecewise Cubics functions & Adams-Moulton 1st order.. PENAlty Saturation=1e-3"
./2dlens ./../data/parameter2dLagBasisDeg3PenSat1e-3 > lagbasisDeg3AM1.log &
clear
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"

spinner $! "Computing with Lagrange Basis and Piecewise Cubics & Adams-Moulton 1st order.. PENAlty Saturation=1e-3."

make clean && make > /dev/null

echo "Computing with Lagrange Basis and Piecewise Cubics functions & Adams-Moulton 2nd order.. PENAlty Saturation=1e-3"
./2dlens ./../data/parameter2dLagBasisDeg3AM2PenSat1e-3 > lagbasisDeg3AM2.log &
clear
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Cubic functions & Adams-Moulton 1st order .. PENAlty Saturation=1e-3 --- COMPLETE"


spinner $! "Computing with Lagrange Basis and Piecewise Cubics & Adams-Moulton 2nd order .. PENAlty Saturation=1e-3."

clear

echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Monomial Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Linear functions .. PENAlty Saturation=1e-2 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Cubic functions & Adams-Moulton 1st order .. PENAlty Saturation=1e-3 --- COMPLETE"
echo "Computing with Lagrange Basis and Piecewise Cubic functions & Adams-Moulton 2nd order .. PENAlty Saturation=1e-3 --- COMPLETE"
