#!/bin/bash

cd 2dlens

source ../../../../../scripts/2dexample.sh

cd ../3dlens

source ../../../../../scripts/3dexample.sh

echo "ALL NUMERICAL TESTS COMPUTED "
echo "****************************"

echo "2d lens problem output available at /dune-twophaseDG/build-cmake/src/test/Output2D"
echo "3d lens problem output available at /dune-twophaseDG/build-cmake/src/test/Output3D"
echo "****************************"
cd ../
