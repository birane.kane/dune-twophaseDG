# SOLVER CONFIGURATION
#---------------------
fem.io.partitioning: rank
# number of process that prints output (-1 no output)
 fem.verboserank: 0
#verbosity for non-linear solver
 fem.solver.newton.verbose: false
# true for iteration output, false means no output
 fem.solver.verbose: false

fem.solver.newton.linabstol: 2.7e-7
fem.solver.newton.tolerance: 3e-7

#type of time discretisation (Backward Euler ==1)
phaseflow.theta: 1.

# parameters from time dependent problems
fem.timeprovider.starttime: 0
phaseflow.endtime: 2000
phaseflow.timestep: 5e0
fem.timeprovider.factor: 1.
fem.timeprovider.updatestep: 1

fem.preconditioning: false

##################################


# DOMAIN CONFIGURATION #########
#---------------------

# macro grid file (2dgrid.dgf)
fem.io.macroGridFile_2d: ../data/2dgrid.dgf

# Domain boundary conditions
phaseflow.coord_x_Dirich_bnd0: 0
phaseflow.coord_x_Dirich_bnd1: 0.9
phaseflow.coord_y_Dirich_bnd0: 0
phaseflow.coord_y_Dirich_bnd1: 0

phaseflow.coord_x_Neum_bnd0: 0.39
phaseflow.coord_x_Neum_bnd1: 0.48
phaseflow.coord_y_Neum_bnd0: 0.
phaseflow.coord_y_Neum_bnd1: 0.65

# Lens delimitation
phaseflow.coord_x_lens0: 0.33
phaseflow.coord_x_lens1: 0.54

phaseflow.coord_y_lens0: 0.465
phaseflow.coord_y_lens1: 0.52

##################################


# GENERAL #######################
#--------

#### Parameters for output ######
phaseflow.computeEOC: 0
# prefix data files
fem.io.datafileprefix: 2dtwophase
# save every i-th step
fem.io.savestep: 40.0e00

# specify directory for data output (is created if not exists)
fem.prefix: ../Output2D/Deg1/MonoBasis/PenSat1e-2
phaseflow.outputfilepath:../Output2D/Deg1/postpro/MonoBasis/PenSat1e-2/
phaseflow.postpro:Output2D_Deg1_MonoBasis_PenSat1e-2_AdamsMoulton1

fem.dofmanager.memoryfactor: 1.1
# output format: binary | vtk-cell | vtk-vertex (projected in DG case) | none
fem.io.outputformat: vtk-vertex
fem.io.path: ./
fem.io.savecount: 0


# upwinding of advective term
phaseflow.with_upw: false

#slope limiter
phaseflow.with_limiter: false

#local adaptivity
phaseflow.local_adapt: true

# tolerance for estimator
phaseflow.tolerance: 5

#number of global refinements before start of simulation
phaseflow.level: 0

#maximum level of refinement
phaseflow.maxlevel:2


#DG penalty
phaseflow.penaltypress: 1e-2
phaseflow.penaltysat: 1e-2

#DG method NIPG=-1 SIPG=1 IIPG=0
phaseflow.DGeps: 0

##################################


# PHYSICAL PARAMETER #############
#--------------

phaseflow.gravity_value: -9.81

# fluids properties
phaseflow.vicositynonwetting: 0.9e-3
phaseflow.vicositywetting: 1e-3
phaseflow.densitynonwetting: 1.460e3
phaseflow.densitywetting: 1e3

# Domain
phaseflow.porosity: 0.4
phaseflow.entrypressure: 755
phaseflow.domain1absolutepermeability: 6.64e-11
phaseflow.inhomogeneitytheta: 2.7
phaseflow.residualnonwetting: 0
phaseflow.residualwetting: 0.12

# Lens
phaseflow.lensabsolutepermeability: 6.64e-16
phaseflow.lensentrypressure: 5000
phaseflow.lensinhomogeneitytheta: 2.
phaseflow.lensporosity: 0.39
phaseflow.lensresidualwetting: 0.1
##################################
